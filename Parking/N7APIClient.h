//
//  N7APIClient.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

extern NSString * const N7APIErrorDomain;

@interface N7APIClient : NSObject

- (instancetype)initWithBaseURL:(NSURL *)url;

- (RACSignal *)loginWithPhone:(NSString *)phone password:(NSString *)password;
- (RACSignal *)resetPasswordForPhone:(NSString *)phone;
- (RACSignal *)registerWithPhone:(NSString *)phone
                           email:(NSString *)email
                       firstName:(NSString *)firstName
                        lastName:(NSString *)lastName
                            male:(BOOL)male;
- (RACSignal *)loadBalanceWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)loadParkingAreasWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)loadAccountSettingsWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)loadVehiclesWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)addVehicle:(NSString *)vehicle toAccountWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)removeVehicle:(NSString *)vehicle fromAccountWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)loadActivePaymentsWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)loadAccountHistoryWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)loadMessagesWithPhone:(NSString *)phone apiKey:(NSString *)apiKey;
- (RACSignal *)startPaymentWithPhone:(NSString *)phone
                              apiKey:(NSString *)apiKey
                            zoneCode:(NSString *)zoneCode
                             vehicle:(NSString *)vehicle
                             minutes:(NSInteger)minutes
                           unlimited:(BOOL)unlimited;
- (RACSignal *)prolongParkingWithPhone:(NSString *)phone
                                apiKey:(NSString *)apiKey
                               vehicle:(NSString *)vehicle
                               minutes:(NSInteger)minutes;
- (RACSignal *)stopPaymentSignalWithPhone:(NSString *)phone
                                   apiKey:(NSString *)apiKey
                                  vehicle:(NSString *)vehicle;

- (RACSignal *)loadMapObjects;

@end

@interface N7APIClient (Cache)

- (RACSignal *)clearCache;

@end
