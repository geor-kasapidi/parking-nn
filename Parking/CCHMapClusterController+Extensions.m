//
//  CCHMapClusterController+Extensions.m
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "CCHMapClusterController+Extensions.h"

@implementation CCHMapClusterController (Extensions)

- (void)selectAnnotation:(id<MKAnnotation>)annotation zoom:(CLLocationDistance)zoom
{
    [self selectAnnotation:annotation andZoomToRegionWithLatitudinalMeters:zoom longitudinalMeters:zoom];
}

@end
