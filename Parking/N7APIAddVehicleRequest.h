//
//  N7APIAddVehicleRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 17.07.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRequest.h"
#import "N7APIVehicleRequestData.h"

@interface N7APIAddVehicleRequest : N7APIRequest

@property (strong, nonatomic) N7APIVehicleRequestData *vehicleRequestData;

@end
