//
//  N7APIStartPaymentResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIStartPaymentResponse.h"
#import "N7APIStartPaymentResult.h"

@implementation N7APIStartPaymentResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(startPaymentResult): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)startPaymentResultJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIStartPaymentResult class]];
}

- (id)resultObject
{
    return self.startPaymentResult;
}

@end
