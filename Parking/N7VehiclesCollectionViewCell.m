//
//  N7VehiclesCollectionViewCell.m
//  Parking
//
//  Created by Георгий Касапиди on 02.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7VehiclesCollectionViewCell.h"
#import "N7Core.h"

@interface N7VehiclesCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@end

@implementation N7VehiclesCollectionViewCell

- (void)awakeFromNib
{
    self.imageView.image = [[UIImage imageNamed:@"vehicle"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)setVehicle:(N7Vehicle *)vehicle
{
    _vehicle = vehicle;
    
    self.imageView.tintColor = vehicle.color;
    self.numberLabel.text = vehicle.number;
}

@end
