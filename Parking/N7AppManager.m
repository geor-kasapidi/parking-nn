//
//  N7AppManager.m
//  Parking
//
//  Created by Георгий Касапиди on 13.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7AppManager.h"

#import "NSString+Mask.h"

#import "N7NotificationsManager.h"
#import "N7DataManager.h"
#import "N7APIClient.h"
#import "N7AccountManager.h"
#import "N7NavigationManager.h"

#import "N7AppData.h"
#import "N7Account.h"

@interface N7AppManager ()

@property (strong, nonatomic) N7AppData *data;

@property (strong, nonatomic) N7NotificationsManager *notificationsManager;
@property (strong, nonatomic) N7DataManager *dataManager;
@property (strong, nonatomic) N7AccountManager *accountManager;
@property (strong, nonatomic) N7NavigationManager *navigationManager;

@end

@implementation N7AppManager

- (instancetype)initWithWindow:(UIWindow *)window
{
    if (self = [super init]) {
#if DEBUG
        NSString *apiURL = @"https://street-parking.ru/";
#else
        NSString *apiURL = @"https://parkovkinn.ru/";
#endif
        self.notificationsManager = [N7NotificationsManager new];
        self.dataManager = [[N7DataManager alloc] initWithNotificationsManager:self.notificationsManager baseURL:[NSURL URLWithString:apiURL]];
        self.accountManager = [[N7AccountManager alloc] initWithService:[NSBundle mainBundle].bundleIdentifier];
        self.navigationManager = [[N7NavigationManager alloc] initWithWindow:window storyboardName:@"Storyboard"];
    }
    
    return self;
}

- (NSString *)lastSavedPhone
{
    return [N7AccountManager lastSavedPhone];
}

- (void)start
{
    @weakify(self);
    [[[[self.accountManager rac_signalForSelector:@selector(setAccount:)] flattenMap:^RACStream *(id value) {
        @strongify(self);
        
        return [self.dataManager clearData];
    }] deliverOn:[RACScheduler mainThreadScheduler]] subscribeNext:^(id x) {
        @strongify(self);
        
        self.data = nil;
        
        [self.notificationsManager cancelAllLocalNotifications];
    }];
    
    if (self.accountManager.account) {
        [self showLoading];
    } else {
        [self showLogin];
    }
}

- (void)showLogin
{
    if ([self.navigationManager showLogin]) {
        self.accountManager.account = nil;
    }
}

- (void)showRegister
{
    [self.navigationManager showRegister];
}

- (void)showLoading
{
    if (![self.navigationManager showLoading]) {
        return;
    }
    
    @weakify(self);
    [[[self.dataManager loadAppData:self.accountManager.account] deliverOn:[RACScheduler mainThreadScheduler]] subscribeNext:^(N7AppData *appData) {
        @strongify(self);
        
        self.data = appData;
        
        [self startRefreshing];
    } error:^(NSError *error) {
        @strongify(self);
        
        [self handleError:error forceLogout:YES];
    } completed:^{
        @strongify(self);
        
        [self showMenu];
    }];
}

- (void)showUserAgreement
{
    [self.navigationManager openWebBrowserWithURL:[NSURL URLWithString:@"https://parkovkinn.ru/info/terms"]];
}

- (void)toggleMenu
{
    [self.navigationManager toggleMenu];
}

- (void)showMenu
{
    [self.navigationManager showMenu];
    
    if (self.data.account.session.activePayments.count > 0) {
        [self showActivePayments];
    } else {
        [self showParkingAreas];
    }
}

- (void)replenishBalance
{
    NSString *url = [NSString stringWithFormat:@"https://parkovkinn.ru/api/extended?module=add&phone=%@&key=%@", self.accountManager.account.phone, self.accountManager.account.apiKey];
    
    [self.navigationManager openWebBrowserWithURL:[NSURL URLWithString:url]];
}

- (void)showParkingAreas
{
    [self.navigationManager showParkingAreas];
}

- (void)showActivePayments
{
    [self.navigationManager showActivePayments];
}

- (void)showActivePaymentsIfNeeded
{
    if (self.data.account.session.activePayments.count > 0) {
        [self showActivePayments];
    }
}

- (void)showAccountHistory
{
    [self.navigationManager showAccountHistory];
}

- (void)showMessages
{
    [self.navigationManager showMessages];
}

- (void)showVehicles
{
    [self.navigationManager showVehicles];
}

- (void)signInWithPhone:(NSString *)phone password:(NSString *)password
{
    if (self.navigationManager.rootContext != N7NavigationManagerRootContextLogin) {
        return;
    }
    
    @weakify(self);
    [[[[[[[self.dataManager loginWithPhone:phone password:password] doNext:^(N7Account *account) {
        @strongify(self);
        
        self.accountManager.account = account;
    }] flattenMap:^RACStream *(N7Account *account) {
        @strongify(self);
        
        return [self.dataManager loadAppData:account];
    }] deliverOn:[RACScheduler mainThreadScheduler]] initially:^{
        @strongify(self);
        
        [self.navigationManager setLoadingVisible:YES fullScreen:NO];
    }] finally:^{
        @strongify(self);
        
        [self.navigationManager setLoadingVisible:NO fullScreen:NO];
    }] subscribeNext:^(N7AppData *appData) {
        @strongify(self);
        
        self.data = appData;
        
        [self startRefreshing];
    } error:^(NSError *error) {
        @strongify(self);
        
        [self handleError:error];
    } completed:^{
        @strongify(self);
        
        [self showMenu];
    }];
}

- (void)resetPassword
{
    if (self.navigationManager.rootContext != N7NavigationManagerRootContextLogin) {
        return;
    }
    
    @weakify(self);
    [[[self.navigationManager.dialogManager promptWithTitle:NSLocalizedString(@"reset_password_prompt_alert_title", nil) message:NSLocalizedString(@"reset_password_prompt_alert_body", nil) text:@"+7"] flattenMap:^RACStream *(NSString *text) {
        @strongify(self);
        
        NSString *phone = [text unmaskedValueWithMask:@"+7**********" maskCharacter:@"*"];
        
        if (phone.length != 10) {
            return [self.navigationManager.dialogManager alertWithTitle:NSLocalizedString(@"reset_password_error_alert_title", nil) message:NSLocalizedString(@"reset_password_error_alert_body", nil)];
        }
        
        return [[[[[self.dataManager resetPasswordForPhone:phone] deliverOn:[RACScheduler mainThreadScheduler]] initially:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:YES fullScreen:NO];
        }] finally:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:NO fullScreen:NO];
        }] flattenMap:^RACStream *(id value) {
            @strongify(self);
            
            return [self.navigationManager.dialogManager alertWithTitle:NSLocalizedString(@"reset_password_success_alert_title", nil) message:NSLocalizedString(@"reset_password_success_alert_body", nil)];
        }];
    }] subscribeNext:^(id x) {
        
    } error:^(NSError *error) {
        @strongify(self);
        
        [self handleError:error];
    }];
}

- (void)registerWithPhone:(NSString *)phone
                    email:(NSString *)email
                firstName:(NSString *)firstName
                 lastName:(NSString *)lastName
                     male:(BOOL)male
{
    if (self.navigationManager.rootContext != N7NavigationManagerRootContextRegister) {
        return;
    }
    
    @weakify(self);
    [[[[[[self.dataManager registerWithPhone:phone email:email firstName:firstName lastName:lastName male:male] deliverOn:[RACScheduler mainThreadScheduler]] initially:^{
        @strongify(self);
        
        [self.navigationManager setLoadingVisible:YES fullScreen:NO];
    }] finally:^{
        @strongify(self);
        
        [self.navigationManager setLoadingVisible:NO fullScreen:NO];
    }] flattenMap:^RACStream *(id value) {
        @strongify(self);
        
        return [self.navigationManager.dialogManager alertWithTitle:NSLocalizedString(@"registration_success_alert_title", nil) message:NSLocalizedString(@"registration_success_alert_body", nil)];
    }] subscribeError:^(NSError *error) {
        @strongify(self);
        
        [self handleError:error];
    } completed:^{
        @strongify(self);
        
        [self showLogin];
    }];
}

- (void)selectVehicleParkingTime:(N7Vehicle *)vehicle parkingArea:(N7ParkingArea *)parkingArea
{
    [self.navigationManager showTimeSelectionWithVehicle:vehicle parkingArea:parkingArea];
}

- (void)startVehicleParking:(N7Vehicle *)vehicle parkingArea:(N7ParkingArea *)parkingArea minutes:(NSNumber *)minutes
{
    NSDate *now = [NSDate date];
    
    NSNumber *price = minutes ? [parkingArea.days priceForMinutes:[minutes unsignedIntegerValue] fromDate:now] : @(INFINITY);
    
    NSString *alertBody = [NSString stringWithFormat:NSLocalizedString(@"start_parking_confirmation_alert_body", nil), vehicle. number, parkingArea.code, [now timeString], minutes ? [[now dateByAddingMinutes:[minutes integerValue]] timeString] : @"∞", [price currencyString]];
    
    @weakify(self);
    [[self.navigationManager.dialogManager confirmWithTitle:NSLocalizedString(@"start_parking_confirmation_alert_title", nil) message:alertBody] subscribeNext:^(id x) {
        @strongify(self);
        
        [[[[[[self.dataManager startPaymentWithAccount:self.accountManager.account zoneCode:parkingArea.code vehicle:vehicle.number minutes:[minutes unsignedIntegerValue] unlimited:!minutes] flattenMap:^RACStream *(id value) {
            @strongify(self);
            
            return [self.dataManager loadAccountSessionData:self.accountManager.account];
        }] deliverOn:[RACScheduler mainThreadScheduler]] initially:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:YES fullScreen:NO];
        }] finally:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:NO fullScreen:NO];
        }] subscribeNext:^(N7SessionData *session) {
            @strongify(self);
            
            self.data.account.session = session;
        } error:^(NSError *error) {
            @strongify(self);
            
            [self handleError:error];
        } completed:^{
            @strongify(self);
            
            [self.navigationManager showActivePayments];
        }];
    }];
}

- (void)stopPayment:(N7ActivePayment *)activePayment
{
    NSString *dialogMessage = [NSString stringWithFormat:NSLocalizedString(@"stop_parking_confirmation_alert_body", nil), activePayment.vehicleNumber, activePayment.parkingAreaCode];
    
    @weakify(self);
    [[self.navigationManager.dialogManager confirmWithTitle:NSLocalizedString(@"stop_parking_confirmation_alert_title", nil) message:dialogMessage] subscribeNext:^(id x) {
        @strongify(self);
        
        [self.notificationsManager cancelLocalNotificationsByKey:activePayment.vehicleNumber];
        
        [[[[[[self.dataManager stopPaymentWithAccount:self.accountManager.account vehicle:activePayment.vehicleNumber] flattenMap:^RACStream *(id value) {
            @strongify(self);
            
            return [self.dataManager loadAccountSessionData:self.accountManager.account];
        }] deliverOn:[RACScheduler mainThreadScheduler]] initially:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:YES fullScreen:NO];
        }] finally:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:NO fullScreen:NO];
        }] subscribeNext:^(N7SessionData *session) {
            @strongify(self);
            
            self.data.account.session = session;
        } error:^(NSError *error) {
            @strongify(self);
            
            [self handleError:error];
        }];
    }];
}

- (void)prolongPayment:(N7ActivePayment *)activePayment
{
    if (self.navigationManager.rootContext != N7NavigationManagerRootContextMenu) {
        return;
    }
    
    @weakify(self);
    [[self.navigationManager.dialogManager selectionListWithItems:@[NSLocalizedString(@"prolong_parking_selection_dialog_first_item_title", nil), NSLocalizedString(@"prolong_parking_selection_dialog_second_item_title", nil), NSLocalizedString(@"prolong_parking_selection_dialog_third_item_title", nil)] title:NSLocalizedString(@"prolong_parking_selection_dialog_title", nil) closeTitle:NSLocalizedString(@"cancel", nil)] subscribeNext:^(NSNumber *selectedIndex) {
        @strongify(self);
        
        NSUInteger index = [selectedIndex unsignedIntegerValue];
        
        [[[[[[self.dataManager prolongParkingWithAccount:self.accountManager.account vehicle:activePayment.vehicleNumber minutes:index == 0 ? 30 : index == 1 ? 60 : 120] flattenMap:^RACStream *(id value) {
            @strongify(self);
            
            return [self.dataManager loadAccountSessionData:self.accountManager.account];
        }] deliverOn:[RACScheduler mainThreadScheduler]] initially:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:YES fullScreen:NO];
        }] finally:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:NO fullScreen:NO];
        }] subscribeNext:^(N7SessionData *session) {
            @strongify(self);
            
            self.data.account.session = session;
        } error:^(NSError *error) {
            @strongify(self);
            
            [self handleError:error];
        }];
    }];
}

- (void)addVehicle
{
    if (self.navigationManager.rootContext != N7NavigationManagerRootContextMenu) {
        return;
    }
    
    @weakify(self);
    [[[self.navigationManager.dialogManager promptWithTitle:NSLocalizedString(@"add_vehicle_prompt_alert_title", nil) message:@""] flattenMap:^RACStream *(NSString *number) {
        @strongify(self);
        
        if (self.data.account.vehicles[number] != nil) {
            return [self.navigationManager.dialogManager alertWithTitle:NSLocalizedString(@"vehicle_exists_alert_title", nil) message:NSLocalizedString(@"vehicle_exists_alert_body", nil)];
        }
        
        return [[[[self.dataManager addVehicleWithNumber:number toAccount:self.accountManager.account] deliverOnMainThread] initially:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:YES fullScreen:NO];
        }] finally:^{
            @strongify(self);
            
            [self.navigationManager setLoadingVisible:NO fullScreen:NO];
        }];
    }] subscribeNext:^(N7VehiclesCollection *vehicles) {
        @strongify(self);
        
        if ([vehicles isKindOfClass:[N7VehiclesCollection class]]) {
            self.data.account.vehicles = vehicles;
        }
    } error:^(NSError *error) {
        @strongify(self);
        
        [self handleError:error];
    }];
}

- (void)removeVehicle:(N7Vehicle *)vehicle
{
    if (self.navigationManager.rootContext != N7NavigationManagerRootContextMenu) {
        return;
    }
    
    if ([self.data.account.vehicles.selectedVehicleNumber isEqualToString:vehicle.number]) {
        self.data.account.vehicles.selectedVehicleNumber = nil;
    }
    
    @weakify(self);
    [[[[[self.dataManager removeVehicle:vehicle fromAccount:self.accountManager.account] deliverOnMainThread] initially:^{
        @strongify(self);
        
        [self.navigationManager setLoadingVisible:YES fullScreen:NO];
    }] finally:^{
        @strongify(self);
        
        [self.navigationManager setLoadingVisible:NO fullScreen:NO];
    }] subscribeNext:^(N7VehiclesCollection *vehicles) {
        @strongify(self);
        
        if ([vehicles isKindOfClass:[N7VehiclesCollection class]]) {
            self.data.account.vehicles = vehicles;
        }
    } error:^(NSError *error) {
        @strongify(self);
        
        [self handleError:error];
    }];
}

- (void)signOut
{
    @weakify(self);
    [[self.navigationManager.dialogManager confirmWithTitle:NSLocalizedString(@"logout_confirmation_alert_body", nil) message:NSLocalizedString(@"logout_confirmation_alert_body", nil)] subscribeNext:^(id x) {
        @strongify(self);
        
        [self showLogin];
    }];
}

#pragma mark -

- (void)startRefreshing
{
    @weakify(self);
    [[[[self.dataManager reloadAccountSessionData:self.accountManager.account withInterval:60] deliverOn:[RACScheduler mainThreadScheduler]] takeUntil:[self.accountManager rac_signalForSelector:@selector(setAccount:)]] subscribeNext:^(N7SessionData *session) {
        @strongify(self);
        
        if (session) {
            self.data.account.session = session;
        }
    } error:^(NSError *error) {
        @strongify(self);
        
        [self handleError:error];
    }];
}

#pragma mark -

- (void)handleError:(NSError *)error
{
    [self handleError:error forceLogout:NO];
}

- (void)handleError:(NSError *)error forceLogout:(BOOL)forceLogout
{
    if ([error.domain isEqualToString:N7APIErrorDomain] ||
        [error.domain isEqualToString:NSURLErrorDomain]) {
        
        @weakify(self);
        [[self.navigationManager.dialogManager alertWithTitle:NSLocalizedString(@"error_alert_title", nil) message:error.localizedDescription] subscribeNext:^(id x) {
            @strongify(self);
            
            if (forceLogout || ([error.domain isEqualToString:N7APIErrorDomain] && error.code > 15 && error.code < 22)) {
                [self showLogin];
            }
        }];
    }
}

@end
