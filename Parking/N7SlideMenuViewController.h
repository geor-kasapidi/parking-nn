//
//  N7SlideMenuViewController.h
//  Parking
//
//  Created by Георгий Касапиди on 06.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@interface N7SlideMenuViewController : UIViewController

@property (assign, nonatomic) CGFloat menuWidth;

@property (strong, nonatomic) UIViewController *menuViewController;
@property (strong, nonatomic) UIViewController *contentViewController;

@property (assign, nonatomic) BOOL panEnabled;

@property (assign, nonatomic) BOOL menuVisible;

@end
