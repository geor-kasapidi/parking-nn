//
//  N7APIActivePaymentsResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIActivePaymentsResponse.h"
#import "N7APIActivePayment.h"

@implementation N7APIActivePaymentsResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(activePayments): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)activePaymentsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[N7APIActivePayment class]];
}

- (id)resultObject
{
    return self.activePayments;
}

@end
