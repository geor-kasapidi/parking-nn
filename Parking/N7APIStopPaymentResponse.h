//
//  N7APIStopPaymentResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIStopPaymentResult;

@interface N7APIStopPaymentResponse : N7APIResponse

@property (strong, nonatomic) N7APIStopPaymentResult *stopPaymentResult;

@end
