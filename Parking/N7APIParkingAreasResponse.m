//
//  N7APIParkingAreasResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIParkingAreasResponse.h"
#import "N7APIParkingArea.h"

@implementation N7APIParkingAreasResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(parkingAreas): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)parkingAreasJSONTransformer
{    
    return [MTLJSONAdapter arrayTransformerWithModelClass:[N7APIParkingArea class]];
}

- (id)resultObject
{
    return self.parkingAreas;
}

@end
