//
//  AFHTTPSessionManager+RAC.h
//  Parking
//
//  Created by Георгий Касапиди on 02.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface AFHTTPSessionManager (RAC)

- (RACSignal *)GET:(NSString *)url parameters:(id)parameters;
- (RACSignal *)POST:(NSString *)url parameters:(id)parameters;

@end
