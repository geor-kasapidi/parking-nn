//
//  N7Vehicle.h
//  Parking
//
//  Created by Георгий Касапиди on 13.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@interface N7Vehicle : NSObject

@property (assign, nonatomic) BOOL remote;

@property (copy, nonatomic) NSString *number;
@property (copy, nonatomic) UIColor *color;

@end
