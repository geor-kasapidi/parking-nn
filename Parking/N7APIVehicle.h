//
//  N7APIVehicle.h
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIVehicle : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *registrationNumber;
@property (copy, nonatomic) NSNumber *useCounter;

@end
