//
//  N7ActivePaymentsViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "UIScrollView+EmptyDataSet.h"

#import "N7Core.h"

#import "N7ActivePaymentsViewController.h"
#import "N7ActivePaymentsViewModel.h"
#import "N7Button.h"
#import "N7ActivePaymentsCollectionViewCell.h"

@interface N7ActivePaymentsViewController () <N7ViewPreferredActions, UICollectionViewDataSource, UICollectionViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@property (weak, nonatomic) IBOutlet UICollectionView *activePaymensCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet N7Button *stopButton;
@property (weak, nonatomic) IBOutlet N7Button *prolongButton;
@property (strong, nonatomic) IBOutlet N7ActivePaymentsViewModel *viewModel;

@end

@implementation N7ActivePaymentsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
    [self localizeUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.view.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:self.view.bounds andColors:@[[UIColor whiteColor], FlatWhite]];
}

#pragma mark - N7ViewPreferredActions

- (void)setupUI
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)bindUI
{
    RAC(self, title) = [RACObserve(self.viewModel, balance) map:^id(NSNumber *balance) {
        return [balance currencyString];
    }];
    
    self.activePaymensCollectionView.delegate = self;
    self.activePaymensCollectionView.dataSource = self;
    self.activePaymensCollectionView.emptyDataSetDelegate = self;
    self.activePaymensCollectionView.emptyDataSetSource = self;
    
    @weakify(self);
    [RACObserve(self.viewModel, activePayments) subscribeNext:^(NSArray<N7ActivePayment *> *activePayments) {
        @strongify(self);
        
        self.pageControl.numberOfPages = activePayments.count;
        
        self.stopButton.hidden =
        self.prolongButton.hidden = activePayments.count == 0;
        
        [self.activePaymensCollectionView reloadData];
    }];
    
    self.navigationItem.leftBarButtonItem.rac_command = self.viewModel.menuCommand;
    
    [[self.stopButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        
        NSIndexPath *indexPath = [[self.activePaymensCollectionView indexPathsForVisibleItems] linq_firstOrNil];
        
        if (indexPath) {
            [self.viewModel.stopCommand execute:self.viewModel.activePayments[indexPath.row]];
        }
    }];
    
    [[self.prolongButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        
        NSIndexPath *indexPath = [[self.activePaymensCollectionView indexPathsForVisibleItems] linq_firstOrNil];
        
        if (indexPath) {
            [self.viewModel.prolongCommand execute:self.viewModel.activePayments[indexPath.row]];
        }
    }];
}

- (void)localizeUI
{
    [self.stopButton setTitle:NSLocalizedString(@"active_payments_screen_stop_button_title", nil) forState:UIControlStateNormal];
    [self.prolongButton setTitle:NSLocalizedString(@"active_payments_screen_prolong_button_title", nil) forState:UIControlStateNormal];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.pageControl.currentPage = self.activePaymensCollectionView.contentOffset.x / CGRectGetWidth(self.activePaymensCollectionView.frame);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.viewModel.activePayments.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionView dequeueReusableCellWithReuseIdentifier:CLASS_NAME(N7ActivePaymentsCollectionViewCell) forIndexPath:indexPath];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(N7ActivePaymentsCollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    N7ActivePayment *activePayment = self.viewModel.activePayments[indexPath.row];
    N7ParkingArea *parkingArea = self.viewModel.parkingAreas[activePayment.parkingAreaCode];
    
    [cell setActivePayment:activePayment parkingArea:parkingArea];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.bounds.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"active_payments_screen_empty_title", nil)];
}

@end
