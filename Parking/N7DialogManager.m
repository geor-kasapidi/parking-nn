//
//  N7DialogManager.m
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7DialogManager.h"
#import "N7BaseNavigationManager.h"

@interface N7DialogManager ()

@property (weak, nonatomic) N7BaseNavigationManager *navigationManager;

@end

@implementation N7DialogManager

- (instancetype)initWithNavigationManager:(N7BaseNavigationManager *)navigationManager
{
    if (self = [super init]) {
        self.navigationManager = navigationManager;
    }
    
    return self;
}

- (RACSignal *)alertWithTitle:(NSString *)title message:(NSString *)message
{
    return [self alertWithTitle:title message:message closeTitle:NSLocalizedString(@"close", nil)];
}

- (RACSignal *)alertWithTitle:(NSString *)title
                      message:(NSString *)message
                   closeTitle:(NSString *)closeTitle
{
    @weakify(self);
    return [RACSignal startLazilyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        @strongify(self);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:closeTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [subscriber sendNext:nil];
            [subscriber sendCompleted];
        }]];
        
        [self.navigationManager.topViewController presentViewController:alertController animated:YES completion:nil];
    }];
}

- (RACSignal *)confirmWithTitle:(NSString *)title message:(NSString *)message
{
    return [self confirmWithTitle:title
                          message:message
                          okTitle:NSLocalizedString(@"ok", nil)
                       closeTitle:NSLocalizedString(@"cancel", nil)];
}

- (RACSignal *)confirmWithTitle:(NSString *)title
                        message:(NSString *)message
                        okTitle:(NSString *)okTitle
                     closeTitle:(NSString *)closeTitle
{
    @weakify(self);
    return [RACSignal startLazilyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        @strongify(self);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:closeTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [subscriber sendError:nil];
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:okTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [subscriber sendNext:nil];
            [subscriber sendCompleted];
        }]];
        
        [self.navigationManager.topViewController presentViewController:alertController animated:YES completion:nil];
    }];
}

- (RACSignal *)promptWithTitle:(NSString *)title message:(NSString *)message
{
    return [self promptWithTitle:title
                         message:message
                            text:@""];
}

- (RACSignal *)promptWithTitle:(NSString *)title
                       message:(NSString *)message
                          text:(NSString *)text
{
    return [self promptWithTitle:title
                         message:message
                            text:text
                         okTitle:NSLocalizedString(@"ok", nil)
                      closeTitle:NSLocalizedString(@"close", nil)];
}

- (RACSignal *)promptWithTitle:(NSString *)title
                       message:(NSString *)message
                          text:(NSString *)text
                       okTitle:(NSString *)okTitle
                    closeTitle:(NSString *)closeTitle
{
    @weakify(self);
    return [RACSignal startLazilyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        @strongify(self);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.textAlignment = NSTextAlignmentCenter;
            textField.text = text;
        }];
        
        [alertController addAction:[UIAlertAction actionWithTitle:closeTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [subscriber sendError:nil];
        }]];
        
        @weakify(alertController);
        [alertController addAction:[UIAlertAction actionWithTitle:okTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(alertController);
            
            [subscriber sendNext:alertController.textFields[0].text];
            [subscriber sendCompleted];
        }]];
        
        [self.navigationManager.topViewController presentViewController:alertController animated:YES completion:nil];
    }];
}

- (RACSignal *)selectionListWithItems:(NSArray<NSString *> *)items
                                title:(NSString *)title
                           closeTitle:(NSString *)closeTitle
{
    @weakify(self);
    return [RACSignal startLazilyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        @strongify(self);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (NSUInteger i = 0; i < items.count; i++) {
            NSUInteger index = i;
            
            [alertController addAction:[UIAlertAction actionWithTitle:items[index] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [subscriber sendNext:@(index)];
                [subscriber sendCompleted];
            }]];
        }
        
        [alertController addAction:[UIAlertAction actionWithTitle:closeTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [subscriber sendError:nil];
        }]];
        
        [self.navigationManager.topViewController presentViewController:alertController animated:YES completion:nil];
    }];
}

@end
