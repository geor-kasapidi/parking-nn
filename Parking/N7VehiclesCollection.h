//
//  N7VehiclesCollection.h
//  Parking
//
//  Created by Георгий Касапиди on 22.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7Vehicle.h"

@interface N7VehiclesCollection : NSObject

- (instancetype)initWithItems:(NSArray<N7Vehicle *> *)items;

@property (assign, nonatomic, readonly) NSUInteger count;

@property (copy, nonatomic) NSString *selectedVehicleNumber;

- (NSInteger)indexOf:(N7Vehicle *)vehicle;

- (N7Vehicle *)objectAtIndexedSubscript:(NSUInteger)idx;
- (N7Vehicle *)objectForKeyedSubscript:(NSString *)key;

@end
