//
//  N7APIRequestModel.h
//  Parking
//
//  Created by Георгий Касапиди on 14.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIRequestModel : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *language;

@end
