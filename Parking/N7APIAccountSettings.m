//
//  N7APIAccountSettings.m
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIAccountSettings.h"

@implementation N7APIAccountSettings

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(accountType): @"account_type",
             SEL_NAME(firstName): @"firstname",
             SEL_NAME(lastName): @"lastname",
             SEL_NAME(parkedReminderIntervalOptions): @"parked_reminder_interval_options",
             SEL_NAME(parkedReminderInterval): @"parked_reminder_interval",
             SEL_NAME(isReplyEnabled): @"is_reply_enabled",
             SEL_NAME(lowNotificationThresholdOptions): @"low_notification_threshold_options",
             SEL_NAME(lowNotificationThreshold): @"low_notification_threshold",
             SEL_NAME(preferredLanguage): @"preferred_language"};
}

@end
