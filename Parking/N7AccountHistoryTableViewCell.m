//
//  N7AccountHistoryTableViewCell.m
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7AccountHistoryTableViewCell.h"

@interface N7AccountHistoryTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end

@implementation N7AccountHistoryTableViewCell

- (void)setEntry:(N7AccountHistoryEntry *)entry
{
    _entry = entry;
    
    self.dateLabel.text = [entry.date timeAgoSinceNow];
    self.descriptionLabel.text = entry.text;
    
    if (entry.entryType == N7AccountHistoryEntryTypeParking) {
        self.amountLabel.text = [NSString stringWithFormat:@"-%@", [entry.amount currencyString]];
        self.amountLabel.textColor = FlatRedDark;
    }
    
    if (entry.entryType == N7AccountHistoryEntryTypeRecharge) {
        self.amountLabel.text = [NSString stringWithFormat:@"+%@", [entry.amount currencyString]];
        self.amountLabel.textColor = FlatGreenDark;
    }
}

@end
