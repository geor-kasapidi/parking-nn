//
//  N7LoginViewModel.m
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7LoginViewModel.h"

@interface N7LoginViewModel ()

@property (strong, nonatomic) RACCommand *loginCommand;
@property (strong, nonatomic) RACCommand *registerCommand;
@property (strong, nonatomic) RACCommand *forgotPasswordCommand;

@end

@implementation N7LoginViewModel

- (void)awakeFromNib
{
    self.phone = CORE.lastSavedPhone;
}

#pragma mark -

- (RACCommand *)loginCommand
{
    if (!_loginCommand) {
        RACSignal *enabled = [[RACSignal combineLatest:@[[RACObserve(self, phone) map:^id(NSString *phone) {
            return @(phone.length == 10);
        }], [RACObserve(self, password) map:^id(NSString *password) {
            return @(password.length > 3 && password.length < 25);
        }]]] and];
        
        @weakify(self);
        _loginCommand = [[RACCommand alloc] initWithEnabled:enabled signalBlock:^RACSignal *(id input) {
            @strongify(self);
            
            [CORE signInWithPhone:self.phone password:self.password];
            
            return [RACSignal empty];
        }];
    }
    
    return _loginCommand;
}

- (RACCommand *)registerCommand
{
    if (!_registerCommand) {
        _registerCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE showRegister];
            
            return [RACSignal empty];
        }];
    }
    
    return _registerCommand;
}

- (RACCommand *)forgotPasswordCommand
{
    if (!_forgotPasswordCommand) {
        _forgotPasswordCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE resetPassword];
            
            return [RACSignal empty];
        }];
    }
    
    return _forgotPasswordCommand;
}

@end
