//
//  N7AccountHistoryViewModel.h
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class N7AccountHistoryEntry;

@interface N7AccountHistoryViewModel : N7ViewModel

@property (strong, nonatomic, readonly) NSArray<N7AccountHistoryEntry *> *entries;

@property (strong, nonatomic, readonly) NSNumber *balance;

@property (strong, nonatomic, readonly) RACCommand *menuCommand;

@end
