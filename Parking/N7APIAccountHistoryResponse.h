//
//  N7APIAccountHistoryResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 17.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIAccountHistoryEntry;

@interface N7APIAccountHistoryResponse : N7APIResponse

@property (strong, nonatomic) NSArray<N7APIAccountHistoryEntry *> *entries;

@end
