//
//  N7APIProlongParkingResponse.h
//  Parking
//
//  Created by Geor Kasapidi on 09.02.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIProlongParkingResult;

@interface N7APIProlongParkingResponse : N7APIResponse

@property (strong, nonatomic) N7APIProlongParkingResult *prolongParkingResult;

@end
