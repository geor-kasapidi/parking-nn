//
//  N7APIStopPaymentRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APIStopPayment;

@interface N7APIStopPaymentRequest : N7APIRequest

@property (strong, nonatomic) N7APIStopPayment *stopPayment;

@end
