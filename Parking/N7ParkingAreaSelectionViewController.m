//
//  N7ParkingAreaSelectionViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 02.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <CCHMapClusterAnnotation.h>
#import <CCHMapClusterController.h>
#import <CCHMapClusterControllerDelegate.h>

#import "UIScrollView+EmptyDataSet.h"

#import "CLLocation+Extensions.h"
#import "CCHMapClusterController+Extensions.h"

#import "N7Core.h"

#import "N7ParkingAreaSelectionViewController.h"

#import "N7ParkingAreaSelectionViewModel.h"
#import "N7SlideMenuViewController.h"
#import "N7VehiclesCollectionViewCell.h"
#import "N7ParkingAreaMapAnnotationView.h"
#import "N7Button.h"

static CLLocationDistance kParkingSelectionZoomMeters = 200;

@interface N7ParkingAreaSelectionViewController ()
<
N7ViewPreferredActions,
UIGestureRecognizerDelegate,
MKMapViewDelegate,
CLLocationManagerDelegate,
CCHMapClusterControllerDelegate,
UISearchResultsUpdating,
UISearchControllerDelegate,
UICollectionViewDelegateFlowLayout,
UICollectionViewDataSource,
UITableViewDelegate,
UITableViewDataSource,
DZNEmptyDataSetDelegate,
DZNEmptyDataSetSource
>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurView;
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *worktimeLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *vehiclesCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *prevImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nextImageView;
@property (weak, nonatomic) IBOutlet N7Button *continueButton;
@property (weak, nonatomic) IBOutlet N7Button *addVehicleButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *blurViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *panelViewHeight;

@property (strong, nonatomic) IBOutlet CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet N7ParkingAreaSelectionViewModel *viewModel;

@property (strong, nonatomic) CCHMapClusterController *mapClusterController;
@property (strong, nonatomic) UITableViewController *searchResultsController;
@property (strong, nonatomic) UISearchController *searchController;

@end

@implementation N7ParkingAreaSelectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
    [self localizeUI];
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

#pragma mark - N7ViewPreferredActions

- (void)setupUI
{
    [self setPanelViewVisible:NO animated:NO];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    
    self.mapClusterController = [[CCHMapClusterController alloc] initWithMapView:self.mapView];
    self.mapClusterController.maxZoomLevelForClustering = 16;
    
    UITableViewController *searchResultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    
    searchResultsController.tableView.rowHeight = 64;
    
    self.searchResultsController = searchResultsController;
    
    UISearchController *searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsController];
    
    searchController.delegate = self;
    searchController.hidesNavigationBarDuringPresentation = NO;
    searchController.dimsBackgroundDuringPresentation = YES;
    searchController.searchBar.barTintColor = [self.navigationController.navigationBar barTintColor];
    
    self.navigationItem.titleView = searchController.searchBar;
    self.searchController = searchController;
    self.definesPresentationContext = YES;
}

- (void)bindUI
{
    @weakify(self);
    
    self.mapView.delegate = self;
    self.locationManager.delegate = self;
    self.mapClusterController.delegate = self;
    self.searchController.searchResultsUpdater = self;
    self.searchResultsController.tableView.delegate = self;
    self.searchResultsController.tableView.dataSource = self;
    self.vehiclesCollectionView.delegate = self;
    self.vehiclesCollectionView.dataSource = self;
    self.vehiclesCollectionView.emptyDataSetDelegate = self;
    self.vehiclesCollectionView.emptyDataSetSource = self;
    
    [self.mapView addOverlays:self.viewModel.mapData.polylines];
    [self.mapView addOverlays:self.viewModel.mapData.polygons];
    
    MKMapRect mapRect = MKMapRectNull;
    
    for (id<MKOverlay> overlay in self.mapView.overlays) {
        if ([overlay respondsToSelector:@selector(boundingMapRect)]) {
            mapRect = MKMapRectUnion(mapRect, overlay.boundingMapRect);
        }
    }
    
    if (!MKMapRectIsNull(mapRect)) {
        [self.mapView setVisibleMapRect:mapRect animated:NO];
    }
    
    [self.mapClusterController addAnnotations:self.viewModel.mapData.points withCompletionHandler:^{
        @strongify(self);
        
        [self.locationManager startUpdatingLocation];
    }];
    
    RAC(self.searchController, active) = RACObserve(self.viewModel, searchActive);
    
    [self rac_liftSelector:@selector(setPanelViewVisible:animated:) withSignals:[[RACObserve(self.viewModel, panelVisible) throttle:.2] deliverOn:[RACScheduler mainThreadScheduler]], [RACSignal return:@YES], nil];
    
    [RACObserve(self.viewModel, filteredMapPoints) subscribeNext:^(id x) {
        @strongify(self);
        
        [self.searchResultsController.tableView reloadData];
    }];
    
    [RACObserve(self.viewModel, vehicles) subscribeNext:^(N7VehiclesCollection *vehicles) {
        @strongify(self);
        
        [self.vehiclesCollectionView reloadData];
        
        NSInteger index = [vehicles indexOf:vehicles[vehicles.selectedVehicleNumber]];
        
        if (index > 0) {
            [self.vehiclesCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        }
        
        self.addVehicleButton.hidden = vehicles.count > 0;
        
        self.prevImageView.hidden =
        self.nextImageView.hidden =
        self.continueButton.hidden = vehicles.count == 0;
    }];
    
    self.navigationItem.leftBarButtonItem.rac_command = self.viewModel.menuCommand;
    self.addVehicleButton.rac_command = self.viewModel.addVehicleCommand;
    
    [[self.continueButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        
        NSIndexPath *indexPath = [[self.vehiclesCollectionView indexPathsForVisibleItems] linq_firstOrNil];
        
        if (indexPath) {
            [self.viewModel.selectTimeCommand execute:self.viewModel.vehicles[indexPath.row]];
        }
    }];
}

- (void)localizeUI
{
    [self.addVehicleButton setTitle:NSLocalizedString(@"map_screen_add_vehicle_button_title", nil) forState:UIControlStateNormal];
    [self.continueButton setTitle:NSLocalizedString(@"map_screen_select_time_button_title", nil) forState:UIControlStateNormal];
}

#pragma mark -

- (void)setPanelViewVisible:(BOOL)visible animated:(BOOL)animated
{
    if ((!self.panelView.hidden && visible) || (self.panelView.hidden && !visible)) {
        return;
    }
    
    CGFloat offset = CGRectGetHeight(self.view.bounds);
    
    if (visible) {
        offset -= self.panelViewHeight.constant;
    }
    
    void (^animations)(void) = ^void(void) {
        self.blurViewTop.constant = offset;
        
        [self.view layoutIfNeeded];
    };
    
    if (self.panelView.hidden && visible) {
        self.panelView.hidden = NO;
        
        if (!animated) {
            animations();
            
            return;
        }
        
        [UIView animateWithDuration:.3
                              delay:.05
             usingSpringWithDamping:.65
              initialSpringVelocity:0
                            options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut
                         animations:animations
                         completion:nil];
    }
    
    if (!self.panelView.hidden && !visible) {
        if (!animated) {
            animations();
            
            self.panelView.hidden = YES;
            
            return;
        }
        
        [UIView animateWithDuration:.3
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut
                         animations:animations
                         completion:^(BOOL finished) {
                             self.panelView.hidden = YES;
                         }];
    }
}

#pragma mark -

- (IBAction)handleSwipe:(id)sender
{
    [self deselectAnnotations];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return [gestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *location = [locations lastObject];
    
    if (!location) {
        return;
    }
    
    [manager stopUpdatingLocation];
    
    CLLocationDistance distance = CLLocationDistanceMax;
    
    id<MKAnnotation> closestAnnotation = nil;
    
    for (id<MKAnnotation> annotation in self.mapClusterController.annotations) {
        CLLocationDistance distanceFrom = [location distanceFrom:annotation.coordinate];
        
        if (distanceFrom < distance) {
            distance = distanceFrom;
            closestAnnotation = annotation;
        }
    }
    
    if (closestAnnotation) {
        [self.mapClusterController selectAnnotation:closestAnnotation zoom:kParkingSelectionZoomMeters];
    }
}

#pragma mark - MKMapViewDelegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        
        renderer.strokeColor = FlatRedDark;
        renderer.lineWidth = 5;
        
        return renderer;
    }
    
    if ([overlay isKindOfClass:[MKPolygon class]]) {
        MKPolygonRenderer *renderer = [[MKPolygonRenderer alloc] initWithPolygon:overlay];
        
        renderer.strokeColor = FlatRedDark;
        renderer.fillColor = [FlatRedDark colorWithAlphaComponent:.2];
        renderer.lineWidth = 5;
        
        return renderer;
    }
    
    return nil;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if (![annotation isKindOfClass:[CCHMapClusterAnnotation class]]) {
        return nil;
    }
    
    MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:CLASS_NAME(N7ParkingAreaMapAnnotationView)];
    
    if (!annotationView) {
        annotationView = [[N7ParkingAreaMapAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:CLASS_NAME(N7ParkingAreaMapAnnotationView)];
    }
    
    annotationView.canShowCallout = NO;
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray<MKAnnotationView *> *)views
{
    for (MKAnnotationView *view in views) {
        if ([view isKindOfClass:[N7ParkingAreaMapAnnotationView class]]) {
            N7ParkingAreaMapAnnotationView *annotationView = (N7ParkingAreaMapAnnotationView *)view;
            
            if ([annotationView.annotation isKindOfClass:[CCHMapClusterAnnotation class]]) {
                annotationView.count = [(CCHMapClusterAnnotation *)annotationView.annotation annotations].count;
            }
        }
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (![view.annotation isKindOfClass:[CCHMapClusterAnnotation class]]) {
        return;
    }
    
    CCHMapClusterAnnotation *clusterAnnotation = (CCHMapClusterAnnotation *)view.annotation;
    
    if (clusterAnnotation.annotations.count > 1) {
        UIEdgeInsets edgePadding = UIEdgeInsetsMake(20, 20, 20, 20);
        
        [mapView setVisibleMapRect:[clusterAnnotation mapRect] edgePadding:edgePadding animated:YES];
        
        return;
    }
    
    id<MKAnnotation> annotation = [clusterAnnotation.annotations anyObject];
    
    if (!annotation) {
        return;
    }
    
    if ([CLLocation distanceFrom:annotation.coordinate to:self.mapView.centerCoordinate] > 1) {
        [self.mapClusterController selectAnnotation:annotation zoom:kParkingSelectionZoomMeters];
    }
    
    N7ParkingArea *parkingArea = self.viewModel.parkingAreas[annotation.title];
    
    if (!parkingArea) {
        return;
    }
    
    self.viewModel.selectedParkingArea = parkingArea;
    
    self.addressLabel.text = annotation.subtitle;
    self.codeLabel.text = parkingArea.code;
    self.worktimeLabel.text = [parkingArea.days.currentDay worktimeString];
    self.priceLabel.text = [parkingArea.days.currentDay pricePerHourString];
    
    self.viewModel.panelVisible = YES;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    self.viewModel.panelVisible = NO;
}

#pragma mark - CCHMapClusterControllerDelegate

- (void)mapClusterController:(CCHMapClusterController *)mapClusterController willReuseMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    MKAnnotationView *annotationView = [mapClusterController.mapView viewForAnnotation:mapClusterAnnotation];
    
    if ([annotationView isKindOfClass:[N7ParkingAreaMapAnnotationView class]]) {
        [(N7ParkingAreaMapAnnotationView *)annotationView setCount:mapClusterAnnotation.annotations.count];
    }
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self.viewModel.filterMapPointsCommand execute:searchController.searchBar.text];
}

#pragma mark - UISearchControllerDelegate

- (void)willPresentSearchController:(UISearchController *)searchController
{
    [self deselectAnnotations];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.filteredMapPoints.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLASS_NAME(UITableViewCell)];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CLASS_NAME(UITableViewCell)];
        
        cell.textLabel.textColor = FlatBlackDark;
        cell.detailTextLabel.textColor = FlatRedDark;
        
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:20];
    }
    
    return cell;
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    MKPointAnnotation *mapPoint = self.viewModel.filteredMapPoints[indexPath.row];
    
    cell.textLabel.text = mapPoint.subtitle;
    cell.detailTextLabel.text = mapPoint.title;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    MKPointAnnotation *mapPoint = self.viewModel.filteredMapPoints[indexPath.row];
    
    self.viewModel.searchActive = NO;
    
    [self.mapClusterController selectAnnotation:mapPoint zoom:kParkingSelectionZoomMeters];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.viewModel.vehicles.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionView dequeueReusableCellWithReuseIdentifier:CLASS_NAME(N7VehiclesCollectionViewCell) forIndexPath:indexPath];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(N7VehiclesCollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.vehicle = self.viewModel.vehicles[indexPath.row];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.bounds.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"map_screen_vehicles_empty_title", nil) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:20]}];
}

#pragma mark -

- (void)deselectAnnotations
{
    for (id<MKAnnotation> annotation in self.mapView.selectedAnnotations) {
        [self.mapView deselectAnnotation:annotation animated:NO];
    }
}

@end
