//
//  N7APIStartPaymentResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIStartPaymentResult;

@interface N7APIStartPaymentResponse : N7APIResponse

@property (strong, nonatomic) N7APIStartPaymentResult *startPaymentResult;

@end
