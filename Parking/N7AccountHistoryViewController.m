//
//  N7AccountHistoryViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 10.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "UIScrollView+EmptyDataSet.h"

#import "N7AccountHistoryViewController.h"
#import "N7AccountHistoryViewModel.h"
#import "N7AccountHistoryTableViewCell.h"

@interface N7AccountHistoryViewController () <N7ViewPreferredActions, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet N7AccountHistoryViewModel *viewModel;

@end

@implementation N7AccountHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
}

#pragma mark - N7ViewPreferredActions

- (void)setupUI
{
    self.tableView.estimatedRowHeight = 80;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [UIView new];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)bindUI
{
    RAC(self, title) = [RACObserve(self.viewModel, balance) map:^id(NSNumber *balance) {
        return [balance currencyString];
    }];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    
    @weakify(self);
    [RACObserve(self.viewModel, entries) subscribeNext:^(id x) {
        @strongify(self);
        
        [self.tableView reloadData];
    }];
    
    self.navigationItem.leftBarButtonItem.rac_command = self.viewModel.menuCommand;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.entries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    N7AccountHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLASS_NAME(N7AccountHistoryTableViewCell)];
    
    cell.entry = self.viewModel.entries[indexPath.row];
    
    return cell;
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"account_history_screen_empty_title", nil)];
}

@end
