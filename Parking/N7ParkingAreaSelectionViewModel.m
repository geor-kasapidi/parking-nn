//
//  N7ParkingAreaSelectionViewModel.m
//  Parking
//
//  Created by Георгий Касапиди on 02.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7ParkingAreaSelectionViewModel.h"

@interface N7ParkingAreaSelectionViewModel ()

@property (strong, nonatomic) N7MapData *mapData;
@property (strong, nonatomic) N7ParkingAreasCollection *parkingAreas;
@property (strong, nonatomic) N7VehiclesCollection *vehicles;

@property (strong, nonatomic) NSArray<MKPointAnnotation *> *filteredMapPoints;

@property (strong, nonatomic) RACCommand *menuCommand;
@property (strong, nonatomic) RACCommand *addVehicleCommand;
@property (strong, nonatomic) RACCommand *filterMapPointsCommand;
@property (strong, nonatomic) RACCommand *selectTimeCommand;

@end

@implementation N7ParkingAreaSelectionViewModel

- (void)awakeFromNib
{
    self.mapData = CORE.data.map;
    
    RAC(self, parkingAreas) = RACObserve(CORE, data.account.parkingAreas);
    RAC(self, vehicles) = RACObserve(CORE, data.account.vehicles);
}

#pragma mark -

- (RACCommand *)menuCommand
{
    if (!_menuCommand) {
        @weakify(self);
        _menuCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            @strongify(self);
            
            self.searchActive = NO;
            
            [CORE toggleMenu];
            
            return [RACSignal empty];
        }];
    }
    
    return _menuCommand;
}

- (RACCommand *)addVehicleCommand
{
    if (!_addVehicleCommand) {
        _addVehicleCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE addVehicle];
            
            return [RACSignal empty];
        }];
    }
    
    return _addVehicleCommand;
}

- (RACCommand *)filterMapPointsCommand
{
    if (!_filterMapPointsCommand) {
        @weakify(self);
        _filterMapPointsCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(NSString *text) {
            @strongify(self);
            
            if ([text isKindOfClass:[NSString class]]) {
                self.filteredMapPoints = [[self.mapData.points linq_where:^BOOL(id<MKAnnotation> point) {
                    return [point.title containsString:text] || [point.subtitle containsString:text];
                }] copy];
            }
            
            return [RACSignal empty];
        }];
    }
    
    return _filterMapPointsCommand;
}

- (RACCommand *)selectTimeCommand
{
    if (!_selectTimeCommand) {
        @weakify(self);
        _selectTimeCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(N7Vehicle *vehicle) {
            @strongify(self);
            
            if ([vehicle isKindOfClass:[N7Vehicle class]]) {
                [CORE selectVehicleParkingTime:vehicle parkingArea:self.selectedParkingArea];
            }
            
            return [RACSignal empty];
        }];
    }
    
    return _selectTimeCommand;
}

@end
