//
//  N7AccountData.h
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7SessionData.h"
#import "N7AccountSettings.h"
#import "N7ParkingAreasCollection.h"
#import "N7VehiclesCollection.h"

@interface N7AccountData : NSObject

@property (strong, nonatomic) N7SessionData *session;
@property (strong, nonatomic) N7AccountSettings *accountSettings;
@property (strong, nonatomic) N7ParkingAreasCollection *parkingAreas;
@property (strong, nonatomic) N7VehiclesCollection *vehicles;

@end
