//
//  N7APIVehiclesResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIVehiclesResponse.h"
#import "N7APIVehicle.h"

@implementation N7APIVehiclesResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(vehicles): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)vehiclesJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[N7APIVehicle class]];
}

- (id)resultObject
{
    return self.vehicles;
}

@end
