//
//  N7ParkingDaysCollection.h
//  Parking
//
//  Created by Георгий Касапиди on 27.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7ParkingDay.h"

@interface N7ParkingDaysCollection : NSObject

+ (instancetype)collectionWithWorkday:(N7ParkingDay *)workday
                             saturday:(N7ParkingDay *)saturday
                               sunday:(N7ParkingDay *)sunday;

- (instancetype)initWithWeekdays:(NSArray<N7ParkingDay *> *)weekdays;

- (NSNumber *)priceForMinutesFromNow:(NSUInteger)minutes;
- (NSNumber *)priceForMinutes:(NSUInteger)minutes fromDate:(NSDate *)date;
- (NSNumber *)priceForParkingPeriod:(DTTimePeriod *)parkingPeriod;

@property (strong, nonatomic, readonly) N7ParkingDay *currentDay;

@end
