//
//  N7ParkingAreaSelectionViewModel.h
//  Parking
//
//  Created by Георгий Касапиди on 02.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class N7MapData;
@class N7ParkingAreasCollection;
@class N7VehiclesCollection;
@class N7ParkingArea;
@class N7Vehicle;

@interface N7ParkingAreaSelectionViewModel : N7ViewModel

@property (strong, nonatomic, readonly) N7MapData *mapData;
@property (strong, nonatomic, readonly) N7ParkingAreasCollection *parkingAreas;
@property (strong, nonatomic, readonly) N7VehiclesCollection *vehicles;

@property (strong, nonatomic, readonly) NSArray<MKPointAnnotation *> *filteredMapPoints;

@property (assign, nonatomic) BOOL searchActive;
@property (assign, nonatomic) BOOL panelVisible;

@property (strong, nonatomic) N7ParkingArea *selectedParkingArea;

@property (strong, nonatomic, readonly) RACCommand *menuCommand;
@property (strong, nonatomic, readonly) RACCommand *addVehicleCommand;
@property (strong, nonatomic, readonly) RACCommand *filterMapPointsCommand;
@property (strong, nonatomic, readonly) RACCommand *selectTimeCommand;

@end
