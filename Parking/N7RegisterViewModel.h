//
//  N7RegisterViewModel.h
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

typedef NS_ENUM(NSInteger, N7ContactType) {
    N7ContactTypeMr,
    N7ContactTypeMrs,
    N7ContactTypeNotSpecified = -1
};

#import "N7ViewModel.h"

@interface N7RegisterViewModel : N7ViewModel

@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (assign, nonatomic) N7ContactType contactType;
@property (assign, nonatomic) BOOL termsAgree;

@property (strong, nonatomic, readonly) RACCommand *termsAgreeCommand;
@property (strong, nonatomic, readonly) RACCommand *registerCommand;
@property (strong, nonatomic, readonly) RACCommand *loginCommand;

@end
