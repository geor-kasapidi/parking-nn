//
//  N7APIRegisterRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APIRegister;

@interface N7APIRegisterRequest : N7APIRequest

@property (strong, nonatomic) N7APIRegister *registration;

@end
