//
//  N7APIStartPayment.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APISession.h"

@interface N7APIStartPayment : N7APISession

@property (copy, nonatomic) NSString *zone;
@property (copy, nonatomic) NSString *vehicle;
@property (copy, nonatomic) NSString *type;
@property (assign, nonatomic) NSInteger minutes;
@property (assign, nonatomic) BOOL timed;

@end
