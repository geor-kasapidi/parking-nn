//
//  N7AppDelegate.h
//  Parking
//
//  Created by Георгий Касапиди on 21.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

@class N7AppManager;

@interface N7AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic, readonly) N7AppManager *core;

+ (instancetype)appDelegate;

@end
