//
//  N7APIAccountHistoryRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 17.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIAccountHistoryRequest.h"
#import "N7APISession.h"
#import "N7APIAccountHistoryResponse.h"

@implementation N7APIAccountHistoryRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(session): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)sessionJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APISession class]];
}

- (NSString *)methodName
{
    return @"account_history";
}

- (Class)responseClass
{
    return [N7APIAccountHistoryResponse class];
}

@end
