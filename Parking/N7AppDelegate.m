//
//  N7AppDelegate.m
//  Parking
//
//  Created by Георгий Касапиди on 21.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <KeyboardManager.h>

#import "N7AppDelegate.h"
#import "N7AppManager.h"

@interface N7AppDelegate ()

@property (strong, nonatomic) N7AppManager *core;

@end

@implementation N7AppDelegate

+ (instancetype)appDelegate
{
    return [UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[[Crashlytics class]]];
    
    [IQKeyboardManager sharedManager].enable = YES;
    
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeSound categories:nil]];
    
    self.window = IOS_VERSION > 8 ? [UIWindow new] : [[UIWindow alloc] initWithFrame:MAIN_SCREEN.bounds];
    
    self.core = [[N7AppManager alloc] initWithWindow:self.window];
    
    [self.core start];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self.core showActivePaymentsIfNeeded];
}

@end
