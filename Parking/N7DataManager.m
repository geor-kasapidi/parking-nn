//
//  N7DataManager.m
//  Parking
//
//  Created by Георгий Касапиди on 18.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7DataManager.h"

#import "N7NotificationsManager.h"

#import "N7APIClient.h"

#import "N7APIAccount.h"
#import "N7APIAccountSettings.h"
#import "N7APIParkingArea.h"
#import "N7APIVehicle.h"
#import "N7APIActivePayment.h"
#import "N7APIAccountHistoryEntry.h"
#import "N7APIMessage.h"
#import "N7APIStartPaymentResult.h"
#import "N7APIProlongParkingResult.h"
#import "N7APIStopPaymentResult.h"

#import "N7Account.h"
#import "N7AppData.h"
#import "N7VehiclesCollection.h"

@interface N7DataManager ()

@property (strong, nonatomic) N7NotificationsManager *notificationsManager;

@property (strong, nonatomic) N7APIClient *apiClient;

@end

@implementation N7DataManager

- (instancetype)initWithNotificationsManager:(N7NotificationsManager *)notificationsManager
                                     baseURL:(NSURL *)url
{
    if (self = [super init]) {
        self.notificationsManager = notificationsManager;
        
        self.apiClient = [[N7APIClient alloc] initWithBaseURL:url];
    }
    
    return self;
}

- (RACSignal *)loginWithPhone:(NSString *)phone password:(NSString *)password
{
    return [[self.apiClient loginWithPhone:phone password:password] map:^id(N7APIAccount *result) {
        N7Account *account = [N7Account new];
        
        account.phone = phone;
        account.apiKey = result.apiKey;
        
        return account;
    }];
}

- (RACSignal *)resetPasswordForPhone:(NSString *)phone
{
    return [self.apiClient resetPasswordForPhone:phone];
}

- (RACSignal *)registerWithPhone:(NSString *)phone
                           email:(NSString *)email
                       firstName:(NSString *)firstName
                        lastName:(NSString *)lastName
                            male:(BOOL)male
{
    return [self.apiClient registerWithPhone:phone email:email firstName:firstName lastName:lastName male:male];
}

- (RACSignal *)loadAccountBalance:(N7Account *)account
{
    return [[self.apiClient loadBalanceWithPhone:account.phone apiKey:account.apiKey] map:^id(NSString *value) {
        return @([value doubleValue]);
    }];
}

- (RACSignal *)loadAccountParkingAreas:(N7Account *)account
{
    return [[self.apiClient loadParkingAreasWithPhone:account.phone apiKey:account.apiKey] map:^id(NSArray<N7APIParkingArea *> *items) {
        return [[N7ParkingAreasCollection alloc] initWithItems:[[items linq_select:^id(N7APIParkingArea *item) {
            N7ParkingArea *parkingArea = [N7ParkingArea new];
            
            parkingArea.code = item.zoneCode;
            parkingArea.freeSpots = item.freeSpots;
            parkingArea.totalSpots = item.totalSpots;
            
            N7ParkingDay *workday = [N7ParkingDay parkingDayFromStartTimeString:item.workdayStartTime
                                                                  endTimeString:item.workdayEndTime
                                                                     rateString:item.workdayRateNextHour
                                                                minAmountString:item.workdayMinAmount];
            
            N7ParkingDay *saturday = [N7ParkingDay parkingDayFromStartTimeString:item.saturdayStartTime
                                                                   endTimeString:item.saturdayEndTime
                                                                      rateString:item.saturdayRateNextHour
                                                                 minAmountString:item.saturdayMinAmount];
            
            N7ParkingDay *sunday = [N7ParkingDay parkingDayFromStartTimeString:item.sundayStartTime
                                                                 endTimeString:item.sundayEndTime
                                                                    rateString:item.sundayRateNextHour
                                                               minAmountString:item.sundayMinAmount];
            
            parkingArea.days = [N7ParkingDaysCollection collectionWithWorkday:workday saturday:saturday sunday:sunday];
            
            return parkingArea;
        }] copy]];
    }];
}

- (RACSignal *)loadAccountSettings:(N7Account *)account
{
    return [[[self.apiClient loadAccountSettingsWithPhone:account.phone apiKey:account.apiKey] catch:^RACSignal *(NSError *error) {
        return [RACSignal return:nil];
    }] map:^id(N7APIAccountSettings *result) {
        N7AccountSettings *settings = [N7AccountSettings new];
        
        settings.accountType = result.accountType;
        settings.firstName = result.firstName;
        settings.lastName = result.lastName;
        
        return settings;
    }];
}

- (RACSignal *)loadAccountActivePayments:(N7Account *)account
{
    @weakify(self);
    return [[[[[self.apiClient loadActivePaymentsWithPhone:account.phone apiKey:account.apiKey] catch:^RACSignal *(NSError *error) {
        return [RACSignal return:nil];
    }] map:^id(NSArray<N7APIActivePayment *> *items) {
        return [[[items linq_select:^id(N7APIActivePayment *item) {
            N7ActivePayment *activePayment = [N7ActivePayment new];
            
            activePayment.parkingAreaCode = item.zone;
            activePayment.vehicleNumber = item.vehicle;
            
            activePayment.startDate = [NSDate dateWithTimeIntervalSince1970:item.startTime];
            
            if (item.endTime > 0) {
                activePayment.endDate = [NSDate dateWithTimeIntervalSince1970:item.endTime];
            }

            activePayment.delta = item.currentTime - [[NSDate date] timeIntervalSince1970];
            
            return activePayment;
        }] linq_reverse] copy];
    }] deliverOn:[RACScheduler mainThreadScheduler]] doNext:^(NSArray<N7ActivePayment *> *activePayments) {
        @strongify(self);
        
        for (N7ActivePayment *activePayment in activePayments) {
            [self.notificationsManager cancelLocalNotificationsByKey:activePayment.vehicleNumber];
            
            NSDate *fireDate = [activePayment completedSoonNotificationFireDate];
            
            if (fireDate) {
                [self.notificationsManager scheduleLocalNotificationWithKey:activePayment.vehicleNumber fireDate:fireDate body:[activePayment completedSoonNotificationText]];
            }
        }
    }];
}

- (RACSignal *)loadAccountHistory:(N7Account *)account
{
    return [[self.apiClient loadAccountHistoryWithPhone:account.phone apiKey:account.apiKey] map:^id(NSArray<N7APIAccountHistoryEntry *> *items) {
        return [[[[items linq_where:^BOOL(N7APIAccountHistoryEntry *item) {
            return [item.contentType integerValue] == 1 || [item.contentType integerValue] == 2;
        }] linq_select:^id(N7APIAccountHistoryEntry *item) {
            N7AccountHistoryEntry *entry = [N7AccountHistoryEntry new];
            
            entry.date = [NSDate dateWithTimeIntervalSince1970:[item.dateString doubleValue]];
            entry.entryType = [item.contentType integerValue] == 2 ? N7AccountHistoryEntryTypeParking : N7AccountHistoryEntryTypeRecharge;
            entry.amount = @([item.amount doubleValue]);
            
            if ([item.contentType integerValue] == 1 && item.message.length > 0) {
                entry.text = item.message;
            }
            
            if ([item.contentType integerValue] == 2 && item.vehicle.length > 0 && item.zoneCode.length > 0) {
                entry.text = [NSString stringWithFormat:@"%@ → %@", item.vehicle, item.zoneCode];
            }
            
            return entry;
        }] linq_reverse] copy];
    }];
}

- (RACSignal *)loadAccountMessages:(N7Account *)account
{
    return [[[self.apiClient loadMessagesWithPhone:account.phone apiKey:account.apiKey] catch:^RACSignal *(NSError *error) {
        return [RACSignal return:@[]];
    }] map:^id(NSArray<N7APIMessage *> *items) {
        return [items linq_select:^id(N7APIMessage *item) {
            N7Message *message = [N7Message new];
            
            message.title = item.title;
            message.message = item.message;
            message.date = [NSDate dateWithTimeIntervalSince1970:[item.date doubleValue]];
            
            return message;
        }];
    }];
}

- (RACSignal *)startPaymentWithAccount:(N7Account *)account
                              zoneCode:(NSString *)zoneCode
                               vehicle:(NSString *)vehicle
                               minutes:(NSInteger)minutes
                             unlimited:(BOOL)unlimited
{
    return [[self.apiClient startPaymentWithPhone:account.phone apiKey:account.apiKey zoneCode:zoneCode vehicle:vehicle minutes:minutes unlimited:unlimited] map:^id(N7APIStartPaymentResult *result) {
        return @(result.parked);
    }];
}

- (RACSignal *)prolongParkingWithAccount:(N7Account *)account vehicle:(NSString *)vehicle minutes:(NSInteger)minutes
{
    return [[self.apiClient prolongParkingWithPhone:account.phone apiKey:account.apiKey vehicle:vehicle minutes:minutes] map:^id(N7APIProlongParkingResult *result) {
        return [NSDate dateWithTimeIntervalSince1970:result.endTime];
    }];
}

- (RACSignal *)stopPaymentWithAccount:(N7Account *)account vehicle:(NSString *)vehicle
{
    return [[[self.apiClient stopPaymentSignalWithPhone:account.phone apiKey:account.apiKey vehicle:vehicle] catchTo:[RACSignal return:nil]] map:^id(N7APIStopPaymentResult *result) {
        return @(YES);
    }];
}

- (RACSignal *)loadMapObjects
{
    return [[self.apiClient loadMapObjects] map:^id(NSArray *shapes) {
        N7MapData *data = [N7MapData new];
        
        data.points = [shapes linq_where:^BOOL(id item) {
            return [item isKindOfClass:[MKPointAnnotation class]];
        }];
        
        data.polylines = [shapes linq_where:^BOOL(id item) {
            return [item isKindOfClass:[MKPolyline class]];
        }];
        
        data.polygons = [shapes linq_where:^BOOL(id item) {
            return [item isKindOfClass:[MKPolygon class]];
        }];
        
        return data;
    }];
}

- (RACSignal *)clearData
{
    return [self.apiClient clearCache];
}

@end

@implementation N7DataManager (Vehicles)

- (RACSignal *)loadAccountVehicles:(N7Account *)account
{
    return [[[self.apiClient loadVehiclesWithPhone:account.phone apiKey:account.apiKey] catch:^RACSignal *(NSError *error) {
        return [RACSignal return:@[]];
    }] map:^id(NSArray<N7APIVehicle *> *items) {
        NSArray<N7Vehicle *> *vehicles = [[items linq_select:^id(N7APIVehicle *item) {
            N7Vehicle *vehicle = [N7Vehicle new];
            
            vehicle.number = item.registrationNumber;
            vehicle.color = FlatSkyBlueDark;
            vehicle.remote = YES;
            
            return vehicle;
        }] copy];
        
        return vehicles.count > 0 ? [[N7VehiclesCollection alloc] initWithItems:vehicles] : nil;
    }];
}

- (RACSignal *)addVehicleWithNumber:(NSString *)number toAccount:(N7Account *)account
{
    @weakify(self);
    return [[[self.apiClient addVehicle:number toAccountWithPhone:account.phone apiKey:account.apiKey] catchTo:[RACSignal return:nil]] flattenMap:^RACStream *(id value) {
        @strongify(self);
        
        return [self loadAccountVehicles:account];
    }];
}

- (RACSignal *)removeVehicle:(N7Vehicle *)vehicle fromAccount:(N7Account *)account
{
    @weakify(self);
    return [[[self.apiClient removeVehicle:vehicle.number fromAccountWithPhone:account.phone apiKey:account.apiKey] catchTo:[RACSignal return:nil]] flattenMap:^RACStream *(id value) {
        @strongify(self);
        
        return [self loadAccountVehicles:account];
    }];
}

@end

@implementation N7DataManager (Extensions)

- (RACSignal *)loadAccountSessionData:(N7Account *)account
{
    return [RACSignal combineLatest:@[[self loadAccountBalance:account],
                                      [self loadAccountActivePayments:account],
                                      [self loadAccountHistory:account],
                                      [self loadAccountMessages:account]]
                             reduce:^id(NSNumber *balance, NSArray<N7ActivePayment *> *activePayments, NSArray<N7AccountHistoryEntry *> *accountHistory, NSArray<N7Message *> *messages) {
                                 N7SessionData *data = [N7SessionData new];
                                 
                                 data.balance = balance;
                                 data.activePayments = activePayments;
                                 data.accountHistory = accountHistory;
                                 data.messages = messages;
                                 
                                 return data;
                             }];
}

- (RACSignal *)loadAccountData:(N7Account *)account
{
    return [RACSignal combineLatest:@[[self loadAccountSessionData:account],
                                      [self loadAccountSettings:account],
                                      [self loadAccountParkingAreas:account],
                                      [self loadAccountVehicles:account]]
                             reduce:^id(N7SessionData *session,
                                        N7AccountSettings *accountSettings,
                                        N7ParkingAreasCollection *parkingAreas,
                                        N7VehiclesCollection *vehicles) {
                                 N7AccountData *data = [N7AccountData new];
                                 
                                 data.session = session;
                                 data.accountSettings = accountSettings;
                                 data.parkingAreas = parkingAreas;
                                 data.vehicles = vehicles;
                                 
                                 return data;
                             }];
}

- (RACSignal *)loadAppData:(N7Account *)account
{
    return [RACSignal combineLatest:@[[self loadMapObjects],
                                      [self loadAccountData:account]]
                             reduce:^id(N7MapData *mapData, N7AccountData *accountData) {
                                 N7AppData *data = [N7AppData new];
                                 
                                 data.map = mapData;
                                 data.account = accountData;
                                 
                                 return data;
                             }];
}

- (RACSignal *)reloadAccountSessionData:(N7Account *)account withInterval:(NSTimeInterval)interval
{
    @weakify(self);
    return [[RACSignal interval:interval onScheduler:[RACScheduler mainThreadScheduler]] flattenMap:^RACStream *(id value) {
        @strongify(self);
        
        return [[self loadAccountSessionData:account] catch:^RACSignal *(NSError *error) {
            if ([error.domain isEqualToString:N7APIErrorDomain]) {
                return [RACSignal error:error];
            } else {
                return [RACSignal return:nil];
            }
        }];
    }];
}

@end
