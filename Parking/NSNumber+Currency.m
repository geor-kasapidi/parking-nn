//
//  NSNumber+Currency.m
//  Parking
//
//  Created by Георгий Касапиди on 07.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "NSNumber+Currency.h"

@implementation NSNumber (Currency)

- (BOOL)hasFractionDigits
{
    double value = [self doubleValue];
    double roundValue = round(value);
    
    return value > roundValue || value < roundValue;
}

- (NSString *)currencyStringWithMaximumFractionDigits:(NSUInteger)maximumFractionDigits locale:(NSLocale *)locale
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    
    formatter.locale = locale;
    
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    formatter.maximumFractionDigits = [self hasFractionDigits] ? maximumFractionDigits : 0;
    
    NSString *result = [formatter stringFromNumber:self];
    
    return result;
}

- (NSString *)currencyStringWithMaximumFractionDigits:(NSUInteger)maximumFractionDigits
{
    return [self currencyStringWithMaximumFractionDigits:maximumFractionDigits locale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
}

- (NSString *)currencyString
{
    return [self currencyStringWithMaximumFractionDigits:1];
}

@end
