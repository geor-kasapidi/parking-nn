//
//  N7APIActivePaymentsRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APISession;

@interface N7APIActivePaymentsRequest : N7APIRequest

@property (strong, nonatomic) N7APISession *session;

@end
