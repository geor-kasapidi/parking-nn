//
//  N7AccountSettings.m
//  Parking
//
//  Created by Георгий Касапиди on 13.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7AccountSettings.h"

@implementation N7AccountSettings

- (NSString *)fullName
{
    return [NSString stringWithFormat:@"%@\n%@",
            self.firstName.length > 0 ? self.firstName : @"",
            self.lastName.length > 0 ? self.lastName : @""];
}

@end
