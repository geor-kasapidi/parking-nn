//
//  N7APIAccountSettingsRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIAccountSettingsRequest.h"
#import "N7APISession.h"
#import "N7APIAccountSettingsResponse.h"

@implementation N7APIAccountSettingsRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(session): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)sessionJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APISession class]];
}

- (NSString *)methodName
{
    return @"account_settings";
}

- (Class)responseClass
{
    return [N7APIAccountSettingsResponse class];
}

@end
