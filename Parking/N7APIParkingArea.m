//
//  N7APIParkingArea.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIParkingArea.h"

@implementation N7APIParkingArea

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(zoneCode): @"zone_code",
             SEL_NAME(freeSpots): @"free_spots",
             SEL_NAME(totalSpots): @"total_spots",
             SEL_NAME(workdayStartTime): @"workday_start_time",
             SEL_NAME(workdayEndTime): @"workday_end_time",
             SEL_NAME(workdayRateFirstHour): @"workday_rate_first_hour",
             SEL_NAME(workdayRateNextHour): @"workday_rate_next_hour",
             SEL_NAME(workdayMinAmount): @"workday_min_amount",
             SEL_NAME(saturdayStartTime): @"saturday_start_time",
             SEL_NAME(saturdayEndTime): @"saturday_end_time",
             SEL_NAME(saturdayRateFirstHour): @"saturday_rate_first_hour",
             SEL_NAME(saturdayRateNextHour): @"saturday_rate_next_hour",
             SEL_NAME(saturdayMinAmount): @"saturday_min_amount",
             SEL_NAME(sundayStartTime): @"sunday_start_time",
             SEL_NAME(sundayEndTime): @"sunday_end_time",
             SEL_NAME(sundayRateFirstHour): @"sunday_rate_first_hour",
             SEL_NAME(sundayRateNextHour): @"sunday_rate_next_hour",
             SEL_NAME(sundayMinAmount): @"sunday_min_amount"};
}

@end
