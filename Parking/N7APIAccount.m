//
//  N7APIAccount.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIAccount.h"

@implementation N7APIAccount

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(apiKey): @"apikey",
             SEL_NAME(accountType): @"account_type"};
}

@end
