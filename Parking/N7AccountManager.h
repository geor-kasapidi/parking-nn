//
//  N7AccountManager.h
//  Parking
//
//  Created by Георгий Касапиди on 21.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

@class N7Account;

@interface N7AccountManager : NSObject

@property (strong, nonatomic) N7Account *account;

+ (NSString *)lastSavedPhone;

- (instancetype)initWithService:(NSString *)service;

@end
