//
//  NSString+Mask.h
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@interface NSString (Mask)

- (instancetype)maskedValueWithMask:(NSString *)mask maskCharacter:(NSString *)maskCharacter;
- (instancetype)unmaskedValueWithMask:(NSString *)mask maskCharacter:(NSString *)maskCharacter;

@end
