//
//  N7APIError.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIError.h"

@implementation N7APIError

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(message): @"message",
             SEL_NAME(code): @"code"};
}

@end
