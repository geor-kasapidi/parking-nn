//
//  NSString+Extensions.m
//  Parking
//
//  Created by Георгий Касапиди on 02.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "NSString+Extensions.h"

@implementation NSString (Extensions)

- (instancetype)trim
{
    return [self stringByTrimmingCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
}

@end
