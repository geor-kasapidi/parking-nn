//
//  N7ViewPreferredActions.h
//  Parking
//
//  Created by Георгий Касапиди on 09.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@protocol N7ViewPreferredActions <NSObject>

@optional
- (void)setupUI;
- (void)bindUI;
- (void)localizeUI;

@end
