//
//  N7APIRegisterResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIResponse.h"

@interface N7APIRegisterResponse : N7APIResponse

@end
