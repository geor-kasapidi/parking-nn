//
//  N7APIParkingAreasRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIParkingAreasRequest.h"
#import "N7APISession.h"
#import "N7APIParkingAreasResponse.h"

@implementation N7APIParkingAreasRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(session): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)sessionJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APISession class]];
}

- (NSString *)methodName
{
    return @"zone_info";
}

- (Class)responseClass
{
    return [N7APIParkingAreasResponse class];
}

@end
