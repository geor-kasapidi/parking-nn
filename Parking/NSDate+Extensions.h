//
//  NSDate+Extensions.h
//  Parking
//
//  Created by Георгий Касапиди on 16.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@interface NSDate (Extensions)

- (NSString *)timeString;
- (instancetype)noSec;

@end
