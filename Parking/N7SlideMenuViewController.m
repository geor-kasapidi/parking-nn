//
//  N7SlideMenuViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 06.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7SlideMenuViewController.h"

@interface N7SlideMenuViewController () <UIGestureRecognizerDelegate>

@property (assign, nonatomic) CGFloat panTouchBeganX;

@property (strong, nonatomic) UIView *contentContainerView;

@end

@implementation N7SlideMenuViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark -

- (instancetype)init
{
    if (self = [super init]) {
        UIView *contentContainerView = [[UIView alloc] initWithFrame:self.view.bounds];
        
        contentContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.view addSubview:contentContainerView];
        
        self.contentContainerView = contentContainerView;
    }
    
    return self;
}

#pragma mark -

- (void)replaceViewController:(UIViewController *)oldVC
                 byAnotherOne:(UIViewController *)newVC
           viewInsertingBlock:(void (^)(UIView *view))viewInsertingBlock
{
    if (!newVC || newVC == oldVC || !viewInsertingBlock) {
        return;
    }
    
    [self addChildViewController:newVC];
    
    viewInsertingBlock(newVC.view);
    
    [newVC didMoveToParentViewController:self];
    
    if (!oldVC || ![self.childViewControllers containsObject:oldVC]) {
        return;
    }
    
    [oldVC willMoveToParentViewController:nil];
    [oldVC.view removeFromSuperview];
    [oldVC removeFromParentViewController];
}

- (void)setMenuViewController:(UIViewController *)menuViewController
{
    [self replaceViewController:_menuViewController byAnotherOne:menuViewController viewInsertingBlock:^(UIView *view) {
        view.frame = CGRectMake(0, 0, _menuWidth, CGRectGetHeight(self.view.bounds));
        view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        [self.view insertSubview:view atIndex:0];
    }];
    
    _menuViewController = menuViewController;
}

- (void)setContentViewController:(UIViewController *)contentViewController
{
    [self replaceViewController:_contentViewController byAnotherOne:contentViewController viewInsertingBlock:^(UIView *view) {
        view.frame = self.contentContainerView.bounds;
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        for (UIGestureRecognizer *gestureRecognizer in self.contentContainerView.gestureRecognizers) {
            if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
                [self.contentContainerView removeGestureRecognizer:gestureRecognizer];
            }
        }
        
        [self.contentContainerView addSubview:view];
        
        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        
        panGestureRecognizer.delegate = self;
        
        [self.contentContainerView addGestureRecognizer:panGestureRecognizer];
    }];
    
    _contentViewController = contentViewController;
}

#pragma mark -

- (void)handlePan:(UIPanGestureRecognizer *)panRecognizer
{
    CGPoint touchPoint = [panRecognizer translationInView:panRecognizer.view];
    
    switch (panRecognizer.state) {
        case UIGestureRecognizerStateBegan: {
            _panTouchBeganX = CGRectGetMinX(panRecognizer.view.frame);
            
            break;
        }
        case UIGestureRecognizerStateChanged: {
            self.view.userInteractionEnabled = NO;
            
            CGFloat x = MAX(0, MIN(_menuWidth, _panTouchBeganX + touchPoint.x));
            
            panRecognizer.view.frame = CGRectMake(x, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
            
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled: {
            if (_panTouchBeganX + touchPoint.x > _menuWidth) {
                self.view.userInteractionEnabled = YES;
                
                break;
            }
            
            CGFloat velocity = [panRecognizer velocityInView:panRecognizer.view].x;
            CGFloat animationDuration = MIN(.5, _menuWidth / velocity);
            
            CGFloat x = _panTouchBeganX < touchPoint.x ? _menuWidth : 0;
            
            [UIView animateWithDuration:animationDuration
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 panRecognizer.view.frame = CGRectMake(x, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
                             } completion:^(BOOL finished) {
                                 for (UIView *subview in panRecognizer.view.subviews) {
                                     subview.userInteractionEnabled = x < _menuWidth;
                                 }
                                 
                                 self.view.userInteractionEnabled = YES;
                             }];
            break;
        }
        default:
            break;
    }
}

- (BOOL)panEnabled
{
    for (UIGestureRecognizer *gestureRecognizer in self.contentContainerView.gestureRecognizers) {
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            return gestureRecognizer.enabled;
        }
    }
    
    return NO;
}

- (void)setPanEnabled:(BOOL)panEnabled
{
    for (UIGestureRecognizer *gestureRecognizer in self.contentContainerView.gestureRecognizers) {
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            gestureRecognizer.enabled = panEnabled;
        }
    }
}

#pragma mark -

- (BOOL)menuVisible
{
    return CGRectGetMinX(self.contentContainerView.frame) > 0;
}

- (void)setMenuVisible:(BOOL)menuVisible
{
    if (self.menuVisible == menuVisible) {
        return;
    }
    
    self.view.userInteractionEnabled = NO;
    
    CGFloat x = menuVisible ? _menuWidth : 0;
    
    [UIView animateWithDuration:.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.contentContainerView.frame = CGRectMake(x, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
                     } completion:^(BOOL finished) {
                         for (UIView *subview in self.contentContainerView.subviews) {
                             subview.userInteractionEnabled = x < _menuWidth;
                         }
                         
                         self.view.userInteractionEnabled = YES;
                     }];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([CLASS_NAME([touch.view class]) rangeOfString:CLASS_NAME(UITableViewCell)].location != NSNotFound) {
        return NO;
    }
    
    return YES;
}

@end
