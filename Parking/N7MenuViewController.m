//
//  N7MenuViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7MenuViewController.h"
#import "N7Label.h"

@interface N7MenuViewController () <N7ViewPreferredActions, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet N7Label *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray<NSString *> *images;
@property (strong, nonatomic) NSArray<NSString *> *items;

@end

@implementation N7MenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
    [self localizeUI];
}

#pragma mark - N7ViewPreferredActions

- (void)setupUI
{
    self.images = @[@"selection",
                    @"hourglass",
                    @"payment",
                    @"payments",
                    @"messages",
                    @"vehicles",
                    @"logout"];
}

- (void)bindUI
{
    self.titleLabel.text = [CORE.data.account.accountSettings fullName];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)localizeUI
{
    self.items = @[NSLocalizedString(@"menu_screen_parking_area_selection_row_text", nil),
                   NSLocalizedString(@"menu_screen_active_payments_row_text", nil),
                   NSLocalizedString(@"menu_screen_replenish_balance_row_text", nil),
                   NSLocalizedString(@"menu_screen_payments_history_row_text", nil),
                   NSLocalizedString(@"menu_screen_messages_row_text", nil),
                   NSLocalizedString(@"menu_screen_vehicles_row_text", nil),
                   NSLocalizedString(@"menu_screen_logout_row_text", nil)];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLASS_NAME(UITableViewCell)];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CLASS_NAME(UITableViewCell)];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont systemFontOfSize:18];
        cell.selectedBackgroundView = [UIView new];
        cell.selectedBackgroundView.backgroundColor = FlatRedDark;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.imageView.image = [UIImage imageNamed:self.images[indexPath.row]];
    cell.textLabel.text = self.items[indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [CORE showParkingAreas];
            [CORE toggleMenu];
            
            break;
        case 1:
            [CORE showActivePayments];
            [CORE toggleMenu];
            
            break;
        case 2:
            [CORE replenishBalance];
            
            self.selectedItemIndex = self.selectedItemIndex;
            
            break;
        case 3:
            [CORE showAccountHistory];
            [CORE toggleMenu];
            
            break;
        case 4:
            [CORE showMessages];
            [CORE toggleMenu];
            
            break;
        case 5:
            [CORE showVehicles];
            [CORE toggleMenu];
            
            break;
        case 6:
            [CORE signOut];
            
            self.selectedItemIndex = self.selectedItemIndex;
            
            break;
        default:
            break;
    }
}

#pragma mark -

- (void)setSelectedItemIndex:(NSUInteger)selectedItemIndex
{
    _selectedItemIndex = selectedItemIndex;
    
    if (selectedItemIndex < self.items.count) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedItemIndex inSection:0]
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionNone];
    }
}

@end
