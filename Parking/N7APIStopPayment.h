//
//  N7APIStopPayment.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APISession.h"

@interface N7APIStopPayment : N7APISession

@property (copy, nonatomic) NSString *vehicle;

@end
