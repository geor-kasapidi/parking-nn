//
//  CLLocation+Extensions.h
//  Parking
//
//  Created by Георгий Касапиди on 13.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocation (Extensions)

+ (CLLocationDistance)distanceFrom:(CLLocationCoordinate2D)fromCoordinate to:(CLLocationCoordinate2D)toCoordinate;

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

- (CLLocationDistance)distanceFrom:(CLLocationCoordinate2D)coordinate;

@end
