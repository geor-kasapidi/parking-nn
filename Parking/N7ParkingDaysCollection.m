//
//  N7ParkingDaysCollection.m
//  Parking
//
//  Created by Георгий Касапиди on 27.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7ParkingDaysCollection.h"

@interface N7ParkingDaysCollection ()

@property (copy, nonatomic) NSArray<N7ParkingDay *> *weekdays;

@end

@implementation N7ParkingDaysCollection

+ (instancetype)collectionWithWorkday:(N7ParkingDay *)workday
                             saturday:(N7ParkingDay *)saturday
                               sunday:(N7ParkingDay *)sunday
{
    NSMutableArray<N7ParkingDay *> *weekdays = [NSMutableArray new];
    
    for (NSUInteger i = 0; i < 5; i++) {
        [weekdays addObject:[workday copy]];
    }
    
    [weekdays addObject:[saturday copy]];
    [weekdays addObject:[sunday copy]];
    
    return [[self alloc] initWithWeekdays:weekdays];
}

- (instancetype)initWithWeekdays:(NSArray<N7ParkingDay *> *)weekdays
{
    NSParameterAssert(weekdays.count == 7);
    
    if (self = [super init]) {
        self.weekdays = [weekdays copy];
    }
    
    return self;
}

- (NSNumber *)priceForMinutesFromNow:(NSUInteger)minutes
{
    return [self priceForMinutes:minutes fromDate:[NSDate date]];
}

- (NSNumber *)priceForMinutes:(NSUInteger)minutes fromDate:(NSDate *)date
{
    DTTimePeriod *parkingPeriod = [DTTimePeriod timePeriodWithSize:DTTimePeriodSizeMinute amount:minutes startingAt:date];
    
    return [self priceForParkingPeriod:parkingPeriod];
}

- (NSNumber *)priceForParkingPeriod:(DTTimePeriod *)parkingPeriod
{
    NSInteger weekday = [parkingPeriod.StartDate weekday];
    NSInteger weekdayIndex = [self indexOfWeekday:weekday];
    
    NSDate *dateStart = [NSDate dateWithYear:[parkingPeriod.StartDate year]
                                       month:[parkingPeriod.StartDate month]
                                         day:[parkingPeriod.StartDate day]];
    
    double sum = 0;
    
    for (NSInteger i = 0; i < 3; i++) {
        N7ParkingDay *day = self.weekdays[(i + weekdayIndex) % 7];
        
        if (!day.pricePerMinute) {
            continue;
        }
        
        NSDate *dayStart = [dateStart dateByAddingDays:i];
        
        DTTimePeriod *dayPeriod = [DTTimePeriod timePeriodWithStartDate:[dayStart dateByAddingMinutes:day.startTime]
                                                                endDate:[dayStart dateByAddingMinutes:day.endTime]];
        
        if (![dayPeriod intersects:parkingPeriod]) {
            continue;
        }
        
        NSDate *startDate = [dayPeriod.StartDate isLaterThan:parkingPeriod.StartDate] ? dayPeriod.StartDate : parkingPeriod.StartDate;
        NSDate *endDate = [dayPeriod.EndDate isEarlierThan:parkingPeriod.EndDate] ? dayPeriod.EndDate : parkingPeriod.EndDate;
        
        DTTimePeriod *crossPeriod = [DTTimePeriod timePeriodWithStartDate:startDate endDate:endDate];
        
        double price = [crossPeriod durationInMinutes] * [day.pricePerMinute doubleValue];
        
        sum += price;
    }
    
    if (sum > 0) {
        N7ParkingDay *day = self.weekdays[weekdayIndex % 7];
        
        sum = MAX([day.minPrice doubleValue], sum);
    }
    
    return @(sum);
}

- (N7ParkingDay *)currentDay
{
    return self.weekdays[[self indexOfWeekday:[[NSDate date] weekday]]];
}

#pragma mark -

- (NSInteger)indexOfWeekday:(NSInteger)weekday
{
    return weekday == 1 ? 6 : weekday - 2;
}

@end
