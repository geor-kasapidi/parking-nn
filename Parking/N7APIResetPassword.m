//
//  N7APIResetPassword.m
//  Parking
//
//  Created by Георгий Касапиди on 14.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIResetPassword.h"

@implementation N7APIResetPassword

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(phone): @"phone"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

@end
