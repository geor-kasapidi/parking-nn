//
//  N7ActivePaymentsCollectionViewCell.m
//  Parking
//
//  Created by Георгий Касапиди on 07.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7ActivePaymentsCollectionViewCell.h"
#import "N7Label.h"

@interface N7ActivePaymentsCollectionViewCell ()

@property (strong, nonatomic) N7ActivePayment *activePayment;
@property (strong, nonatomic) N7ParkingArea *parkingArea;

@property (weak, nonatomic) IBOutlet N7Label *timerLabel;
@property (weak, nonatomic) IBOutlet N7Label *vehicleLabel;
@property (weak, nonatomic) IBOutlet N7Label *priceLabel;
@property (weak, nonatomic) IBOutlet N7Label *timeLabel;

@end

@implementation N7ActivePaymentsCollectionViewCell

- (void)setActivePayment:(N7ActivePayment *)activePayment parkingArea:(N7ParkingArea *)parkingArea
{
    self.activePayment = activePayment;
    self.parkingArea = parkingArea;
    
    [self refreshWithDate:[NSDate date]];
    
    @weakify(self);
    [[[RACSignal interval:1 onScheduler:[RACScheduler mainThreadScheduler]] takeUntil:self.rac_prepareForReuseSignal] subscribeNext:^(NSDate *date) {
        @strongify(self);
        
        [self refreshWithDate:date];
    }];
}

#pragma mark -

- (void)refreshWithDate:(NSDate *)date
{
    NSDate *adjustedDate = [date dateByAddingTimeInterval:self.activePayment.delta];
    
    NSUInteger seconds = (NSUInteger)[adjustedDate secondsFrom:self.activePayment.startDate];
    
    self.timerLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", (int)(seconds / 60 / 60), (int)(seconds / 60 % 60), (int)(seconds % 60)];
    self.vehicleLabel.text = self.activePayment.vehicleNumber;
    self.timeLabel.text = [NSString stringWithFormat:@"%@ → %@", [self.activePayment.startDate timeString], self.activePayment.endDate ? [self.activePayment.endDate timeString] : @"∞"];
    
    NSDate *endDate = self.activePayment.endDate ? self.activePayment.endDate : adjustedDate;
    
    NSNumber *price = [self.parkingArea.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[self.activePayment.startDate noSec] endDate:[endDate noSec]]];
    
    [self.priceLabel setText:[price currencyString] animated:NO];
}

@end
