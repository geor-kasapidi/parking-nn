//
//  N7APIStopPaymentResult.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIStopPaymentResult : MTLModel <MTLJSONSerializing>

@property (assign, nonatomic) BOOL stopped;

@end
