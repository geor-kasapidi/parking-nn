//
//  N7TimePicker.m
//  Parking
//
//  Created by Георгий Касапиди on 03.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7TimePicker.h"

@interface N7TimePicker () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (assign, nonatomic, readonly) NSInteger numberOfComponents;
@property (assign, nonatomic, readonly) NSInteger numberOfRows;

@property (assign, nonatomic) NSInteger selectedTime;

@end

@implementation N7TimePicker

- (void)awakeFromNib
{
    self.delegate = self;
    self.dataSource = self;
    
    [self selectRow:[self zeroRowForRow:self.numberOfRows / 2 inComponent:0] inComponent:0 animated:NO];
    [self selectRow:[self zeroRowForRow:self.numberOfRows / 2 inComponent:1] inComponent:1 animated:NO];
}

#pragma mark -

- (void)setActive:(BOOL)active
{
    self.userInteractionEnabled = active;
    
    [UIView animateWithDuration:.2 animations:^{
        self.alpha = active ? 1 : .3;
    }];
}

- (UIFont *)font
{
    if (!_font) {
        _font = [UIFont boldSystemFontOfSize:40];
    }
    
    return _font;
}

#pragma mark -

- (void)setTime:(NSInteger)time
{
    [self willChangeValueForKey:SEL_NAME(time)];
    
    _time = MAX(time, self.minTime);
    
    self.selectedTime = _time;
    
    [self didChangeValueForKey:SEL_NAME(time)];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.numberOfComponents;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.numberOfRows;
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 100;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 64;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *textLabel;
    
    if ([view isKindOfClass:[UILabel class]]) {
        textLabel = (UILabel *)view;
    } else {
        textLabel = [UILabel new];
        
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.font = self.font;
    }
    
    textLabel.text = [@([self valueForRow:row inComponent:component]) stringValue];
    
    return textLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.time = self.selectedTime;
}

#pragma mark -

- (NSInteger)selectedTime
{
    return [self selectedValueInComponent:0] * 60 + [self selectedValueInComponent:1];
}

- (void)setSelectedTime:(NSInteger)selectedTime
{
    [self setSelectedTime:selectedTime animated:YES];
}

- (void)setSelectedTime:(NSInteger)selectedTime animated:(BOOL)animated
{
    [self selectRow:[self rowForValue:(selectedTime / 60) % 24 inComponent:0] inComponent:0 animated:animated];
    [self selectRow:[self rowForValue:selectedTime % 60 inComponent:1] inComponent:1 animated:animated];
}

#pragma mark -

- (NSInteger)numberOfComponents
{
    return 2;
}

- (NSInteger)numberOfRows
{
    return SHRT_MAX;
}

- (NSInteger)periodForComponent:(NSInteger)component
{
    return component == 0 ? 24 : 4;
}

- (NSInteger)multiplierForComponent:(NSInteger)component
{
    return component == 0 ? 1 : 15;
}

- (NSInteger)zeroRowForRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger period = [self periodForComponent:component];
    
    return (row / period) * period;
}

- (NSInteger)selectedValueInComponent:(NSInteger)component
{
    return [self valueForRow:[self selectedRowInComponent:component] inComponent:component];
}

- (NSInteger)valueForRow:(NSInteger)row inComponent:(NSInteger)component
{
    return (row % [self periodForComponent:component]) * [self multiplierForComponent:component];
}

- (NSInteger)rowForValue:(NSInteger)value inComponent:(NSInteger)component
{
    NSInteger selectedRow = [self selectedRowInComponent:component];
    NSInteger zeroRow = [self zeroRowForRow:selectedRow inComponent:component];
    
    return zeroRow + (value / [self multiplierForComponent:component]) % [self periodForComponent:component];
}

@end
