//
//  N7APIBalanceRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APISession;

@interface N7APIBalanceRequest : N7APIRequest

@property (strong, nonatomic) N7APISession *session;

@end
