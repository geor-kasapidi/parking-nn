//
//  N7MessagesTableViewCell.h
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@class N7Message;

@interface N7MessagesTableViewCell : UITableViewCell

@property (strong, nonatomic) N7Message *message;

@end
