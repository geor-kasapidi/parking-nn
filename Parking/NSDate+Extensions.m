//
//  NSDate+Extensions.m
//  Parking
//
//  Created by Георгий Касапиди on 16.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "NSDate+Extensions.h"

@implementation NSDate (Extensions)

- (NSString *)timeString
{
    return [self formattedDateWithFormat:[self isToday] ? @"HH:mm" : @"HH:mm (EEE)"];
}

- (instancetype)noSec
{
    return [self dateBySubtractingSeconds:self.second];
}

@end
