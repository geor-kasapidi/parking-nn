//
//  UIWindow+Root.h
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@interface UIWindow (Root)

- (void)changeRootViewControllerTo:(UIViewController *)viewController
               withTransitionStyle:(UIViewAnimationOptions)transitionStyle;

@end
