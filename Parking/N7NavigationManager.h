//
//  N7NavigationManager.h
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7BaseNavigationManager.h"

@class N7Vehicle;
@class N7ParkingArea;

typedef NS_ENUM(NSUInteger, N7NavigationManagerRootContext) {
    N7NavigationManagerRootContextNone,
    N7NavigationManagerRootContextLogin,
    N7NavigationManagerRootContextRegister,
    N7NavigationManagerRootContextLoading,
    N7NavigationManagerRootContextMenu,
    N7NavigationManagerRootContextUnknown
};

typedef NS_ENUM(NSUInteger, N7NavigationManagerCenterContext) {
    N7NavigationManagerCenterContextParking = 0,
    N7NavigationManagerCenterContextActivePayments = 1,
    N7NavigationManagerCenterContextAccountHistory = 3,
    N7NavigationManagerCenterContextMessages = 4,
    N7NavigationManagerCenterContextVehicles = 5,
    N7NavigationManagerCenterContextUnknown
};

@interface N7NavigationManager : N7BaseNavigationManager

@property (assign, nonatomic, readonly) N7NavigationManagerRootContext rootContext;
@property (assign, nonatomic, readonly) N7NavigationManagerCenterContext centerContext;

- (instancetype)initWithWindow:(UIWindow *)window storyboardName:(NSString *)storyboardName;

- (void)toggleMenu;
- (void)openWebBrowserWithURL:(NSURL *)url;

- (BOOL)showLogin;
- (BOOL)showRegister;
- (BOOL)showLoading;
- (BOOL)showMenu;

- (BOOL)showParkingAreas;
- (BOOL)showActivePayments;
- (BOOL)showAccountHistory;
- (BOOL)showMessages;
- (BOOL)showVehicles;

- (BOOL)showTimeSelectionWithVehicle:(N7Vehicle *)vehicle parkingArea:(N7ParkingArea *)parkingArea;

@end
