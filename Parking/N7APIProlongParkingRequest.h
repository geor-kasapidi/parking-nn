//
//  N7APIProlongParkingRequest.h
//  Parking
//
//  Created by Geor Kasapidi on 09.02.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APIProlongParking;

@interface N7APIProlongParkingRequest : N7APIRequest

@property (strong, nonatomic) N7APIProlongParking *prolongParking;

@end
