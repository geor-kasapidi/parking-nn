//
//  N7ParkingDay.m
//  Parking
//
//  Created by Георгий Касапиди on 27.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7ParkingDay.h"

NSString *timeToString(NSInteger time)
{
    return [NSString stringWithFormat:@"%02d:%02d", (int)(time / 60), (int)(time % 60)];
}

NSInteger stringToTime(NSString *string)
{
    NSArray<NSString *> *s = [string componentsSeparatedByString:@":"];
    
    if (s.count < 2) {
        return 0;
    }
    
    NSInteger h = [s[0] integerValue];
    NSInteger m = [s[1] integerValue];
    
    return h * 60 + m;
}

@implementation N7ParkingDay

+ (instancetype)parkingDayFromStartTimeString:(NSString *)startTimeString
                                endTimeString:(NSString *)endTimeString
                                   rateString:(NSString *)rateString
                              minAmountString:(NSString *)minAmountString
{
    N7ParkingDay *day = [N7ParkingDay new];
    
    if (startTimeString.length > 0) {
        day.startTime = stringToTime(startTimeString);
    }
    
    if (endTimeString.length > 0) {
        day.endTime = stringToTime(endTimeString);
    }
    
    if (rateString.length > 0) {
        day.pricePerMinute = @([rateString doubleValue] / 60);
    }
    
    if (minAmountString.length > 0) {
        day.minPrice = @([minAmountString doubleValue]);
    }
    
    return day;
}

#pragma mark -

- (instancetype)init
{
    if (self = [super init]) {
        self.startTime = 0;
        self.endTime = 24 * 60 - 1;
    }
    
    return self;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone
{
    typeof(self) copy = [[self class] new];
    
    copy.startTime = self.startTime;
    copy.endTime = self.endTime;
    copy.pricePerMinute = [self.pricePerMinute copy];
    copy.minPrice = [self.minPrice copy];
    
    return copy;
}

#pragma mark -

- (NSString *)worktimeString
{
    return [NSString stringWithFormat:@"%@ → %@", timeToString((NSInteger)self.startTime), timeToString((NSInteger)self.endTime)];
}

- (NSString *)pricePerHourString
{
    double price = [self.pricePerMinute doubleValue] * 60;
    
    return [NSString stringWithFormat:NSLocalizedString(@"per_hour", nil), [@(price) currencyString]];
}

@end
