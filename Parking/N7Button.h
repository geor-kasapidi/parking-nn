//
//  N7Button.h
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

IB_DESIGNABLE

@interface N7Button : UIButton

@property (strong, nonatomic) IBInspectable UIColor *buttonColor;
@property (strong, nonatomic) IBInspectable UIColor *textColor;
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;

@end
