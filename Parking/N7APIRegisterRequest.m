//
//  N7APIRegisterRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRegisterRequest.h"
#import "N7APIRegister.h"
#import "N7APIRegisterResponse.h"

@implementation N7APIRegisterRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(registration): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)registrationJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIRegister class]];
}

- (NSString *)methodName
{
    return @"register";
}

- (Class)responseClass
{
    return [N7APIRegisterResponse class];
}

@end
