//
//  NSNumber+Currency.h
//  Parking
//
//  Created by Георгий Касапиди on 07.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@interface NSNumber (Currency)

- (NSString *)currencyStringWithMaximumFractionDigits:(NSUInteger)maximumFractionDigits locale:(NSLocale *)locale;
- (NSString *)currencyStringWithMaximumFractionDigits:(NSUInteger)maximumFractionDigits;
- (NSString *)currencyString;

@end
