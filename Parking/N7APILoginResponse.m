//
//  N7APILoginResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APILoginResponse.h"
#import "N7APIAccount.h"

@implementation N7APILoginResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(account): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)accountJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIAccount class]];
}

- (id)resultObject
{
    return self.account;
}

@end
