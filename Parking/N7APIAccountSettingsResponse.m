//
//  N7APIAccountSettingsResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIAccountSettingsResponse.h"
#import "N7APIAccountSettings.h"

@implementation N7APIAccountSettingsResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(accountSettings): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)accountSettingsJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIAccountSettings class]];
}

- (id)resultObject
{
    return self.accountSettings;
}

@end
