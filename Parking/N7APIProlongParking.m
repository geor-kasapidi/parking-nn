//
//  N7APIProlongParking.m
//  Parking
//
//  Created by Geor Kasapidi on 09.02.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIProlongParking.h"

@implementation N7APIProlongParking

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(vehicle): @"vehicle",
              SEL_NAME(minutes): @"minutes"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

@end
