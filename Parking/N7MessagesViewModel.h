//
//  N7MessagesViewModel.h
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class N7Message;

@interface N7MessagesViewModel : N7ViewModel

@property (strong, nonatomic, readonly) NSArray<N7Message *> *messages;

@property (strong, nonatomic, readonly) NSNumber *balance;

@property (strong, nonatomic, readonly) RACCommand *menuCommand;

@end
