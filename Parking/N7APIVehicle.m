//
//  N7APIVehicle.m
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIVehicle.h"

@implementation N7APIVehicle

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(registrationNumber): @"registration_number",
             SEL_NAME(useCounter): @"use_counter"};
}

@end
