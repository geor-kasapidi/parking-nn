//
//  N7APIParkingAreasResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIParkingArea;

@interface N7APIParkingAreasResponse : N7APIResponse

@property (strong, nonatomic) NSArray<N7APIParkingArea *> *parkingAreas;

@end
