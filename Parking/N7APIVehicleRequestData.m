//
//  N7APIVehicleRequestData.m
//  Parking
//
//  Created by Георгий Касапиди on 17.07.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIVehicleRequestData.h"

@implementation N7APIVehicleRequestData

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(vehicle): @"vehicle"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

@end
