//
//  N7RegisterViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "TTTAttributedLabel.h"

#import "N7RegisterViewController.h"
#import "N7RegisterViewModel.h"
#import "N7Label.h"
#import "N7Button.h"
#import "N7MaskedNumberField.h"

@interface N7RegisterViewController () <N7ViewPreferredActions, TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet N7Label *titleLabel;
@property (weak, nonatomic) IBOutlet N7Label *contactTypeLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *termsAgreeLabel;

@property (weak, nonatomic) IBOutlet N7MaskedNumberField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *contactTypeSegmentControl;
@property (weak, nonatomic) IBOutlet UISwitch *termsAgreeSwitcher;

@property (weak, nonatomic) IBOutlet N7Button *registerButton;
@property (weak, nonatomic) IBOutlet N7Button *loginButton;

@property (strong, nonatomic) IBOutlet N7RegisterViewModel *viewModel;

@end

@implementation N7RegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
    [self localizeUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.view.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:self.view.bounds andColors:@[[UIColor whiteColor], FlatWhite]];
}

#pragma mark - N7ViewPreferredActions

- (void)setupUI
{
//    self.termsAgreeLabel.linkAttributes = @{NSForegroundColorAttributeName: FlatRed};
//    self.termsAgreeLabel.activeLinkAttributes = @{NSForegroundColorAttributeName: FlatRedDark};
}

- (void)bindUI
{
    self.termsAgreeLabel.delegate = self;
    
    RAC(self.viewModel, phone) = self.phoneTextField.rac_textSignal;
    RAC(self.viewModel, email) = self.emailTextField.rac_textSignal;
    RAC(self.viewModel, firstName) = [self.firstNameTextField.rac_textSignal map:^id(NSString *firstName) {
        return [firstName trim];
    }];
    RAC(self.viewModel, lastName) = [self.lastNameTextField.rac_textSignal map:^id(NSString *lastName) {
        return [lastName trim];
    }];
    RAC(self.viewModel, contactType) = [[self.contactTypeSegmentControl rac_newSelectedSegmentIndexChannelWithNilValue:nil] startWith:@(N7ContactTypeNotSpecified)];
    RAC(self.viewModel, termsAgree) = [self.termsAgreeSwitcher rac_newOnChannel];
    
    self.registerButton.rac_command = self.viewModel.registerCommand;
    self.loginButton.rac_command = self.viewModel.loginCommand;
}

- (void)localizeUI
{
    self.titleLabel.text = NSLocalizedString(@"parking_title", nil);
    
    self.phoneTextField.placeholder = NSLocalizedString(@"register_screen_phone_input_placeholder", nil);
    self.emailTextField.placeholder = NSLocalizedString(@"register_screen_email_input_placeholder", nil);
    self.firstNameTextField.placeholder = NSLocalizedString(@"register_screen_firstname_input_placeholder", nil);
    self.lastNameTextField.placeholder = NSLocalizedString(@"register_screen_lastname_input_placeholder", nil);
    
    self.contactTypeLabel.text = NSLocalizedString(@"register_screen_contact_type_label_text", nil);
    [self.contactTypeSegmentControl setTitle:NSLocalizedString(@"register_screen_contact_type_title_male", nil) forSegmentAtIndex:0];
    [self.contactTypeSegmentControl setTitle:NSLocalizedString(@"register_screen_contact_type_title_female", nil) forSegmentAtIndex:1];
    
    NSString *termsText = NSLocalizedString(@"register_screen_terms_label_text", nil);
    NSString *termsLink = NSLocalizedString(@"register_screen_terms_label_link", nil);
    
    self.termsAgreeLabel.text = [NSString stringWithFormat:@"%@ %@", termsText, termsLink];
    
    [self.termsAgreeLabel addLinkToURL:[NSURL URLWithString:@""] withRange:NSMakeRange(termsText.length + 1, termsLink.length)];
    
    [self.registerButton setTitle:NSLocalizedString(@"register_screen_register_button_title", nil) forState:UIControlStateNormal];
    [self.loginButton setTitle:NSLocalizedString(@"register_screen_login_button_title", nil) forState:UIControlStateNormal];
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [self.viewModel.termsAgreeCommand execute:nil];
}

@end
