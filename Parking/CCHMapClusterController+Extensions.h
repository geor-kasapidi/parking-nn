//
//  CCHMapClusterController+Extensions.h
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <CCHMapClusterController.h>

@interface CCHMapClusterController (Extensions)

- (void)selectAnnotation:(id<MKAnnotation>)annotation zoom:(CLLocationDistance)zoom;

@end
