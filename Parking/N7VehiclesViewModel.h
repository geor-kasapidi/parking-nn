//
//  N7VehiclesViewModel.h
//  Parking
//
//  Created by Георгий Касапиди on 10.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class N7VehiclesCollection;

@interface N7VehiclesViewModel : N7ViewModel

@property (strong, nonatomic, readonly) N7VehiclesCollection *vehicles;

@property (strong, nonatomic, readonly) NSNumber *balance;

@property (strong, nonatomic, readonly) RACCommand *menuCommand;
@property (strong, nonatomic, readonly) RACCommand *addVehicleCommand;
@property (strong, nonatomic, readonly) RACCommand *removeVehicleCommand;

@end
