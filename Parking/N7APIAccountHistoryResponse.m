//
//  N7APIAccountHistoryResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 17.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIAccountHistoryResponse.h"
#import "N7APIAccountHistoryEntry.h"

@implementation N7APIAccountHistoryResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(entries): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)entriesJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[N7APIAccountHistoryEntry class]];
}

- (id)resultObject
{
    return self.entries;
}

@end
