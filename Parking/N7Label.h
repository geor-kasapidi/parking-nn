//
//  N7Label.h
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@interface N7Label : UILabel

- (void)setText:(NSString *)text animated:(BOOL)animated;

@end
