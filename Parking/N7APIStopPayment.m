//
//  N7APIStopPayment.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIStopPayment.h"

@implementation N7APIStopPayment

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(vehicle): @"vehicle"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

@end
