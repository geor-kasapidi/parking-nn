//
//  N7APIAccountSettingsResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIAccountSettings;

@interface N7APIAccountSettingsResponse : N7APIResponse

@property (strong, nonatomic) N7APIAccountSettings *accountSettings;

@end
