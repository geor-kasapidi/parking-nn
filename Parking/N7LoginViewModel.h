//
//  N7LoginViewModel.h
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@interface N7LoginViewModel : N7ViewModel

@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *password;

@property (strong, nonatomic, readonly) RACCommand *loginCommand;
@property (strong, nonatomic, readonly) RACCommand *registerCommand;
@property (strong, nonatomic, readonly) RACCommand *forgotPasswordCommand;

@end
