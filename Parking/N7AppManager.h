//
//  N7AppManager.h
//  Parking
//
//  Created by Георгий Касапиди on 13.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@class N7AppData;
@class N7Vehicle;
@class N7ParkingArea;
@class N7ActivePayment;

@interface N7AppManager : NSObject

@property (strong, nonatomic, readonly) N7AppData *data;

@property (strong, nonatomic, readonly) NSString *lastSavedPhone;

- (instancetype)initWithWindow:(UIWindow *)window;

- (void)start;

- (void)showLogin;
- (void)showRegister;
- (void)showUserAgreement;

- (void)toggleMenu;

- (void)replenishBalance;

- (void)showParkingAreas;
- (void)showActivePayments;
- (void)showActivePaymentsIfNeeded;
- (void)showAccountHistory;
- (void)showMessages;
- (void)showVehicles;

- (void)signInWithPhone:(NSString *)phone password:(NSString *)password;
- (void)resetPassword;
- (void)registerWithPhone:(NSString *)phone
                    email:(NSString *)email
                firstName:(NSString *)firstName
                 lastName:(NSString *)lastName
                     male:(BOOL)male;
- (void)selectVehicleParkingTime:(N7Vehicle *)vehicle parkingArea:(N7ParkingArea *)parkingArea;
- (void)startVehicleParking:(N7Vehicle *)vehicle
                parkingArea:(N7ParkingArea *)parkingArea
                    minutes:(NSNumber *)minutes;
- (void)stopPayment:(N7ActivePayment *)activePayment;
- (void)prolongPayment:(N7ActivePayment *)activePayment;
- (void)addVehicle;
- (void)removeVehicle:(N7Vehicle *)vehicle;
- (void)signOut;

@end
