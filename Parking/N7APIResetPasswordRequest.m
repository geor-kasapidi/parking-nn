//
//  N7APIResetPasswordRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 14.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIResetPasswordRequest.h"
#import "N7APIResetPassword.h"
#import "N7APIResetPasswordResponse.h"

@implementation N7APIResetPasswordRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(resetPassword): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)resetPasswordJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIResetPassword class]];
}

- (NSString *)methodName
{
    return @"password_reset";
}

- (Class)responseClass
{
    return [N7APIResetPasswordResponse class];
}

@end
