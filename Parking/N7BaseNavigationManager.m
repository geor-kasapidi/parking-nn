//
//  N7BaseNavigationManager.m
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "MBProgressHUD.h"

#import "N7BaseNavigationManager.h"

@interface N7BaseNavigationManager ()

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) N7DialogManager *dialogManager;

@end

@implementation N7BaseNavigationManager

- (instancetype)initWithWindow:(UIWindow *)window
{
    if (self = [super init]) {
        self.window = window;
        self.dialogManager = [[N7DialogManager alloc] initWithNavigationManager:self];
    }
    
    return self;
}

- (UIViewController *)topViewController
{
    return nil;
}

- (void)setLoadingVisible:(BOOL)visible fullScreen:(BOOL)fullScreen
{
    [self setLoadingVisible:visible fullScreen:fullScreen animated:YES];
}

- (void)setLoadingVisible:(BOOL)visible fullScreen:(BOOL)fullScreen animated:(BOOL)animated
{
    [MBProgressHUD hideAllHUDsForView:self.window animated:animated];
    [MBProgressHUD hideAllHUDsForView:self.topViewController.view animated:animated];
    
    if (visible) {
        [MBProgressHUD showHUDAddedTo:fullScreen ? self.window : self.topViewController.view animated:animated];
    }
}

@end
