//
//  N7VehiclesViewModel.m
//  Parking
//
//  Created by Георгий Касапиди on 10.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7VehiclesViewModel.h"

@interface N7VehiclesViewModel ()

@property (strong, nonatomic) N7VehiclesCollection *vehicles;

@property (strong, nonatomic) NSNumber *balance;

@property (strong, nonatomic) RACCommand *menuCommand;
@property (strong, nonatomic) RACCommand *addVehicleCommand;
@property (strong, nonatomic) RACCommand *removeVehicleCommand;

@end

@implementation N7VehiclesViewModel

- (void)awakeFromNib
{
    RAC(self, balance) = RACObserve(CORE, data.account.session.balance);
    RAC(self, vehicles) = RACObserve(CORE, data.account.vehicles);
}

#pragma mark -

- (RACCommand *)menuCommand
{
    if (!_menuCommand) {
        _menuCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE toggleMenu];
            
            return [RACSignal empty];
        }];
    }
    
    return _menuCommand;
}

- (RACCommand *)addVehicleCommand
{
    if (!_addVehicleCommand) {
        _addVehicleCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE addVehicle];
            
            return [RACSignal empty];
        }];
    }
    
    return _addVehicleCommand;
}

- (RACCommand *)removeVehicleCommand
{
    if (!_removeVehicleCommand) {
        _removeVehicleCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(N7Vehicle *vehicle) {
            if ([vehicle isKindOfClass:[N7Vehicle class]]) {
                [CORE removeVehicle:vehicle];
            }
            
            return [RACSignal empty];
        }];
    }
    
    return _removeVehicleCommand;
}

@end
