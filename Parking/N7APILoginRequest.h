//
//  N7APILoginRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APILogin;

@interface N7APILoginRequest : N7APIRequest

@property (strong, nonatomic) N7APILogin *login;

@end
