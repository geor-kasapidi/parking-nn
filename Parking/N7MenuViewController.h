//
//  N7MenuViewController.h
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewController.h"

@interface N7MenuViewController : N7ViewController

@property (assign, nonatomic) NSUInteger selectedItemIndex;

@end
