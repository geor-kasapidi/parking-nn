//
//  N7VehiclesCollection.m
//  Parking
//
//  Created by Георгий Касапиди on 22.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7VehiclesCollection.h"

@interface N7VehiclesCollection ()

@property (copy, nonatomic) NSArray<N7Vehicle *> *vehicles;

@end

@implementation N7VehiclesCollection

- (instancetype)initWithItems:(NSArray<N7Vehicle *> *)items {
    self = [super init];
    
    if (self) {
        self.vehicles = items;
    }
    
    return self;
}

- (NSUInteger)count {
    return self.vehicles.count;
}

- (NSInteger)indexOf:(N7Vehicle *)vehicle {
    return vehicle ? [self.vehicles indexOfObject:vehicle] : -1;
}

- (NSString *)selectedVehicleNumber {
    return [[NSUserDefaults standardUserDefaults] objectForKey:SEL_NAME(selectedVehicleNumber)];
}

- (void)setSelectedVehicleNumber:(NSString *)selectedVehicleNumber {
    NSString *key = SEL_NAME(selectedVehicleNumber);
    
    [self willChangeValueForKey:key];
    
    if (selectedVehicleNumber.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:selectedVehicleNumber forKey:key];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self didChangeValueForKey:key];
}

- (N7Vehicle *)objectAtIndexedSubscript:(NSUInteger)idx {
    return idx < self.count ? self.vehicles[idx] : nil;
}

- (N7Vehicle *)objectForKeyedSubscript:(NSString *)key {
    if (key.length > 0) {
        return [[self.vehicles linq_where:^BOOL(N7Vehicle *vehicle) {
            return [vehicle.number isEqualToString:key];
        }] linq_firstOrNil];
    }
    
    return nil;
}

@end
