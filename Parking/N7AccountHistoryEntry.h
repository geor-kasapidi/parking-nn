//
//  N7AccountHistoryEntry.h
//  Parking
//
//  Created by Георгий Касапиди on 17.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

typedef NS_ENUM(NSUInteger, N7AccountHistoryEntryType) {
    N7AccountHistoryEntryTypeParking,
    N7AccountHistoryEntryTypeRecharge
};

@interface N7AccountHistoryEntry : NSObject

@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) N7AccountHistoryEntryType entryType;
@property (strong, nonatomic) NSNumber *amount;
@property (copy, nonatomic) NSString *text;

@end
