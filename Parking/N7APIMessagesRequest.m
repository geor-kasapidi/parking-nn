//
//  N7APIMessagesRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIMessagesRequest.h"
#import "N7APISession.h"
#import "N7APIMessagesReponse.h"

@implementation N7APIMessagesRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(session): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)sessionJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APISession class]];
}

- (NSString *)methodName
{
    return @"messages";
}

- (Class)responseClass
{
    return [N7APIMessagesReponse class];
}

@end
