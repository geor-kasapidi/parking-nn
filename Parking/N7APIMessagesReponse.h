//
//  N7APIMessagesReponse.h
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIMessage;

@interface N7APIMessagesReponse : N7APIResponse

@property (strong, nonatomic) NSArray<N7APIMessage *> *messages;

@end
