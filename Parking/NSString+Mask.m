//
//  NSString+Mask.m
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "NSString+Mask.h"

@implementation NSString (Mask)

- (instancetype)maskedValueWithMask:(NSString *)mask maskCharacter:(NSString *)maskCharacter
{
    NSParameterAssert(maskCharacter.length == 1);
    
    NSString *temp = [mask copy];
    
    NSString *result = @"";
    
    for (NSUInteger i = 0, n = self.length; i < n; i++) {
        NSString *value = [self substringWithRange:NSMakeRange(i, 1)];
        
        BOOL isDigit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[value characterAtIndex:0]];
        
        if (!isDigit) {
            continue;
        }
        
        NSRange range = [temp rangeOfString:maskCharacter];
        
        if (range.location == NSNotFound) {
            break;
        }
        
        temp = [temp stringByReplacingCharactersInRange:range withString:value];
        result = [temp substringToIndex:range.location + range.length];
    }
    
    return result;
}

- (instancetype)unmaskedValueWithMask:(NSString *)mask maskCharacter:(NSString *)maskCharacter
{
    NSParameterAssert(maskCharacter.length == 1);
    
    NSMutableString *result = [NSMutableString new];
    
    for (NSUInteger i = 0, n = MIN(self.length, mask.length); i < n; i++) {
        NSString *character = [mask substringWithRange:NSMakeRange(i, 1)];
        NSString *value = [self substringWithRange:NSMakeRange(i, 1)];
        
        BOOL isMaskCharacter = [character isEqualToString:maskCharacter];
        BOOL isDigit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[value characterAtIndex:0]];
        
        if (isMaskCharacter && isDigit) {
            [result appendString:value];
        }
    }
    
    return [result copy];
}

@end
