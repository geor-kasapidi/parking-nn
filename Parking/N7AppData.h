//
//  N7AppData.h
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7MapData.h"
#import "N7AccountData.h"

@interface N7AppData : NSObject

@property (strong, nonatomic) N7MapData *map;
@property (strong, nonatomic) N7AccountData *account;

@end
