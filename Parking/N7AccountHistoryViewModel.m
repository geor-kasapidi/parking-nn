//
//  N7AccountHistoryViewModel.m
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7AccountHistoryViewModel.h"

@interface N7AccountHistoryViewModel ()

@property (strong, nonatomic) NSArray<N7AccountHistoryEntry *> *entries;

@property (strong, nonatomic) NSNumber *balance;

@property (strong, nonatomic) RACCommand *menuCommand;

@end

@implementation N7AccountHistoryViewModel

- (void)awakeFromNib
{
    RAC(self, balance) = RACObserve(CORE, data.account.session.balance);
    RAC(self, entries) = RACObserve(CORE, data.account.session.accountHistory);
}

#pragma mark -

- (RACCommand *)menuCommand
{
    if (!_menuCommand) {
        _menuCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE toggleMenu];
            
            return [RACSignal empty];
        }];
    }
    
    return _menuCommand;
}

@end
