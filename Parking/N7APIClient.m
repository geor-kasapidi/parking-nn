//
//  N7APIClient.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "AFNetworking.h"
#import "PINCache.h"
#import "GeoJSONSerialization.h"

#import "AFHTTPSessionManager+RAC.h"

#import "N7APIClient.h"
#import "N7APIRequest.h"
#import "N7APIResponse.h"
#import "N7APIError.h"
#import "N7APISession.h"

#import "N7APILoginRequest.h"
#import "N7APIResetPasswordRequest.h"
#import "N7APIRegisterRequest.h"
#import "N7APIBalanceRequest.h"
#import "N7APIParkingAreasRequest.h"
#import "N7APIAccountSettingsRequest.h"
#import "N7APIVehiclesRequest.h"
#import "N7APIActivePaymentsRequest.h"
#import "N7APIAccountHistoryRequest.h"
#import "N7APIMessagesRequest.h"
#import "N7APIStartPaymentRequest.h"
#import "N7APIProlongParkingRequest.h"
#import "N7APIStopPaymentRequest.h"
#import "N7APIAddVehicleRequest.h"
#import "N7APIRemoveVehicleRequest.h"

#import "N7APILoginResponse.h"
#import "N7APIResetPasswordResponse.h"
#import "N7APIRegisterResponse.h"
#import "N7APIBalanceResponse.h"
#import "N7APIParkingAreasResponse.h"
#import "N7APIAccountSettingsResponse.h"
#import "N7APIVehiclesResponse.h"
#import "N7APIActivePaymentsResponse.h"
#import "N7APIAccountHistoryResponse.h"
#import "N7APIMessagesReponse.h"
#import "N7APIStartPaymentResponse.h"
#import "N7APIProlongParkingResponse.h"
#import "N7APIStopPaymentResponse.h"

#import "N7APILogin.h"
#import "N7APIResetPassword.h"
#import "N7APIRegister.h"
#import "N7APIStartPayment.h"
#import "N7APIProlongParking.h"
#import "N7APIStopPayment.h"
#import "N7APIVehicleRequestData.h"

NSString * const N7APIErrorDomain = @"N7APIErrorDomain";

@interface N7APIClient ()

@property (strong, nonatomic) AFHTTPSessionManager *sessionManager;
@property (strong, nonatomic) PINDiskCache *diskCache;

@end

@implementation N7APIClient (Cache)

- (RACSignal *)clearCache
{
    @weakify(self);
    return [RACSignal startLazilyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        @strongify(self);
        
        [self.diskCache removeAllObjects:^(PINDiskCache * _Nonnull cache) {
            [subscriber sendNext:cache];
            [subscriber sendCompleted];
        }];
    }];
}

- (RACSignal *)cacheSignalForKey:(NSString *)key
{
    @weakify(self);
    return [RACSignal startLazilyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        @strongify(self);
        
        [self.diskCache objectForKey:key block:^(PINDiskCache * _Nonnull cache, NSString * _Nonnull key, id<NSCoding>  _Nullable object, NSURL * _Nullable fileURL) {
            [subscriber sendNext:object];
            [subscriber sendCompleted];
        }];
    }];
}

- (RACSignal *)cacheSignalForNetworkSignal:(RACSignal *)networkSignal cacheKey:(NSString *)cacheKey
{
    @weakify(self);
    return [[networkSignal doNext:^(id<NSCoding> response) {
        @strongify(self);
        
        [self.diskCache setObject:response forKey:cacheKey];
    }] catch:^RACSignal *(NSError *error) {
        @strongify(self);
        
        if (![error.domain isEqualToString:NSURLErrorDomain]) {
            return [RACSignal error:error];
        }
        
        return [[self cacheSignalForKey:cacheKey] flattenMap:^RACStream *(id value) {
            return value ? [RACSignal return:value] : [RACSignal error:error];
        }];
    }];
}

@end

@implementation N7APIClient

- (instancetype)initWithBaseURL:(NSURL *)url
{
    if (self = [super init]) {
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForResource = 10;
        
        AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url sessionConfiguration:sessionConfig];
        
        sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        sessionManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        NSString *str = [NSString stringWithFormat:@"%@:%@", @"api", @"sandbox"];
        NSData *data = [str dataUsingEncoding:NSASCIIStringEncoding];
        NSString *base64 = [NSString stringWithFormat:@"Basic %@", [data base64EncodedStringWithOptions:0]];
        
        [sessionManager.requestSerializer setValue:base64 forHTTPHeaderField:@"Authorization"];
        
        self.sessionManager = sessionManager;
        
        self.diskCache = [PINDiskCache sharedCache];
    }
    
    return self;
}

#pragma mark -

- (RACSignal *)executeAPIMethodWithRequest:(N7APIRequest *)request cache:(BOOL)cache
{
    NSDictionary *parameters = [MTLJSONAdapter JSONDictionaryFromModel:request error:nil];
    
    RACSignal *networkSignal = [self.sessionManager POST:@"api" parameters:parameters];
    RACSignal *apiSignal = cache ? [self cacheSignalForNetworkSignal:networkSignal cacheKey:request.methodName] : networkSignal;
    
    return [apiSignal flattenMap:^RACStream *(NSDictionary *json) {
        NSError *mtlError;
        
        N7APIResponse *response = [MTLJSONAdapter modelOfClass:[request responseClass] fromJSONDictionary:json error:&mtlError];
        
        if (mtlError) {
            return [RACSignal error:mtlError];
        }
        
        if (response.error) {
            NSError *apiError = [NSError errorWithDomain:N7APIErrorDomain
                                                    code:[response.error.code integerValue]
                                                userInfo:@{NSLocalizedDescriptionKey: response.error.message}];
            
            return [RACSignal error:apiError];
        }
        
        return [RACSignal return:[response resultObject]];
    }];
}

#pragma mark -

- (RACSignal *)loginWithPhone:(NSString *)phone password:(NSString *)password
{
    N7APILoginRequest *request = [N7APILoginRequest new];
    
    request.login = [N7APILogin new];
    
    request.login.phone = phone;
    request.login.password = password;
    
    return [self executeAPIMethodWithRequest:request cache:NO];
}

- (RACSignal *)resetPasswordForPhone:(NSString *)phone
{
    N7APIResetPasswordRequest *request = [N7APIResetPasswordRequest new];
    
    request.resetPassword = [N7APIResetPassword new];
    
    request.resetPassword.phone = phone;
    
    return [self executeAPIMethodWithRequest:request cache:NO];
}

- (RACSignal *)registerWithPhone:(NSString *)phone
                           email:(NSString *)email
                       firstName:(NSString *)firstName
                        lastName:(NSString *)lastName
                            male:(BOOL)male
{
    N7APIRegisterRequest *request = [N7APIRegisterRequest new];
    
    request.registration = [N7APIRegister new];
    
    request.registration.email = email;
    request.registration.phone = phone;
    request.registration.name = firstName;
    request.registration.surname = lastName;
    request.registration.gender = male ? @"male" : @"female";
    
    return [self executeAPIMethodWithRequest:request cache:NO];
}

- (RACSignal *)loadBalanceWithPhone:(NSString *)phone apiKey:(NSString *)apiKey
{
    N7APIBalanceRequest *request = [N7APIBalanceRequest new];
    
    request.session = [N7APISession new];
    
    request.session.phone = phone;
    request.session.apiKey = apiKey;
    
    return [self executeAPIMethodWithRequest:request cache:YES];
}

- (RACSignal *)loadParkingAreasWithPhone:(NSString *)phone apiKey:(NSString *)apiKey
{
    N7APIParkingAreasRequest *request = [N7APIParkingAreasRequest new];
    
    request.session = [N7APISession new];
    
    request.session.phone = phone;
    request.session.apiKey = apiKey;
    
    return [self executeAPIMethodWithRequest:request cache:YES];
}

- (RACSignal *)loadAccountSettingsWithPhone:(NSString *)phone apiKey:(NSString *)apiKey
{
    N7APIAccountSettingsRequest *request = [N7APIAccountSettingsRequest new];
    
    request.session = [N7APISession new];
    
    request.session.phone = phone;
    request.session.apiKey = apiKey;
    
    return [self executeAPIMethodWithRequest:request cache:YES];
}

- (RACSignal *)loadVehiclesWithPhone:(NSString *)phone apiKey:(NSString *)apiKey
{
    N7APIVehiclesRequest *request = [N7APIVehiclesRequest new];
    
    request.session = [N7APISession new];
    
    request.session.phone = phone;
    request.session.apiKey = apiKey;
    
    return [self executeAPIMethodWithRequest:request cache:YES];
}

- (RACSignal *)addVehicle:(NSString *)vehicle toAccountWithPhone:(NSString *)phone apiKey:(NSString *)apiKey {
    N7APIAddVehicleRequest *request = [N7APIAddVehicleRequest new];
    
    request.vehicleRequestData = [N7APIVehicleRequestData new];
    
    request.vehicleRequestData.phone = phone;
    request.vehicleRequestData.apiKey = apiKey;
    request.vehicleRequestData.vehicle = vehicle;
    
    return [self executeAPIMethodWithRequest:request cache:NO];
}

- (RACSignal *)removeVehicle:(NSString *)vehicle fromAccountWithPhone:(NSString *)phone apiKey:(NSString *)apiKey {
    N7APIRemoveVehicleRequest *request = [N7APIRemoveVehicleRequest new];
    
    request.vehicleRequestData = [N7APIVehicleRequestData new];
    
    request.vehicleRequestData.phone = phone;
    request.vehicleRequestData.apiKey = apiKey;
    request.vehicleRequestData.vehicle = vehicle;
    
    return [self executeAPIMethodWithRequest:request cache:NO];
}

- (RACSignal *)loadActivePaymentsWithPhone:(NSString *)phone apiKey:(NSString *)apiKey
{
    N7APIActivePaymentsRequest *request = [N7APIActivePaymentsRequest new];
    
    request.session = [N7APISession new];
    
    request.session.phone = phone;
    request.session.apiKey = apiKey;
    
    return [self executeAPIMethodWithRequest:request cache:YES];
}

- (RACSignal *)loadAccountHistoryWithPhone:(NSString *)phone apiKey:(NSString *)apiKey
{
    N7APIAccountHistoryRequest *request = [N7APIAccountHistoryRequest new];
    
    request.session = [N7APISession new];
    
    request.session.phone = phone;
    request.session.apiKey = apiKey;
    
    return [self executeAPIMethodWithRequest:request cache:YES];
}

- (RACSignal *)loadMessagesWithPhone:(NSString *)phone apiKey:(NSString *)apiKey
{
    N7APIMessagesRequest *request = [N7APIMessagesRequest new];
    
    request.session = [N7APISession new];
    
    request.session.phone = phone;
    request.session.apiKey = apiKey;
    
    return [self executeAPIMethodWithRequest:request cache:YES];
}

- (RACSignal *)startPaymentWithPhone:(NSString *)phone
                              apiKey:(NSString *)apiKey
                            zoneCode:(NSString *)zoneCode
                             vehicle:(NSString *)vehicle
                             minutes:(NSInteger)minutes
                           unlimited:(BOOL)unlimited
{
    N7APIStartPaymentRequest *request = [N7APIStartPaymentRequest new];
    
    request.startPayment = [N7APIStartPayment new];
    
    request.startPayment.phone = phone;
    request.startPayment.apiKey = apiKey;
    request.startPayment.zone = zoneCode;
    request.startPayment.vehicle = vehicle;
    request.startPayment.type = @"P";
    request.startPayment.minutes = minutes;
    request.startPayment.timed = !unlimited;
    
    return [self executeAPIMethodWithRequest:request cache:NO];
}

- (RACSignal *)prolongParkingWithPhone:(NSString *)phone
                                apiKey:(NSString *)apiKey
                               vehicle:(NSString *)vehicle
                               minutes:(NSInteger)minutes
{
    N7APIProlongParkingRequest *request = [N7APIProlongParkingRequest new];
    
    request.prolongParking = [N7APIProlongParking new];
    
    request.prolongParking.phone = phone;
    request.prolongParking.apiKey = apiKey;
    request.prolongParking.vehicle = vehicle;
    request.prolongParking.minutes = minutes;
    
    return [self executeAPIMethodWithRequest:request cache:NO];
}

- (RACSignal *)stopPaymentSignalWithPhone:(NSString *)phone
                                   apiKey:(NSString *)apiKey
                                  vehicle:(NSString *)vehicle
{
    N7APIStopPaymentRequest *request = [N7APIStopPaymentRequest new];
    
    request.stopPayment = [N7APIStopPayment new];
    
    request.stopPayment.phone = phone;
    request.stopPayment.apiKey = apiKey;
    request.stopPayment.vehicle = vehicle;
    
    return [self executeAPIMethodWithRequest:request cache:NO];
}

#pragma mark -

- (RACSignal *)loadMapObjects
{
    RACSignal *networkSignal = [self.sessionManager GET:@"api/map" parameters:@{}];
    RACSignal *cacheSignal = [self cacheSignalForNetworkSignal:networkSignal cacheKey:@"geodata"];
    
    return [cacheSignal flattenMap:^RACStream *(NSDictionary *json) {
        NSError *parseError;
        
        NSArray *shapes = [GeoJSONSerialization shapesFromGeoJSONFeatureCollection:json error:&parseError];
        
        if (parseError) {
            return [RACSignal error:parseError];
        }
        
        return [RACSignal return:shapes];
    }];
}

@end
