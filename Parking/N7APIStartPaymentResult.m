//
//  N7APIStartPaymentResult.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIStartPaymentResult.h"

@implementation N7APIStartPaymentResult

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(parked): @"parked"};
}

@end
