//
//  N7VehiclesViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 10.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "UIScrollView+EmptyDataSet.h"

#import "N7Core.h"

#import "N7VehiclesViewController.h"
#import "N7VehiclesViewModel.h"

@interface N7VehiclesViewController () <N7ViewPreferredActions, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet N7VehiclesViewModel *viewModel;

@end

@implementation N7VehiclesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
}

#pragma mark - N7ViewPreferredActions

- (void)setupUI
{
    self.tableView.rowHeight = 64;
    self.tableView.tableFooterView = [UIView new];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:nil action:nil];
}

- (void)bindUI
{
    RAC(self, title) = [RACObserve(self.viewModel, balance) map:^id(NSNumber *balance) {
        return [balance currencyString];
    }];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    
    @weakify(self);
    [RACObserve(self.viewModel, vehicles.selectedVehicleNumber) subscribeNext:^(id x) {
        @strongify(self);
        
        [self.tableView reloadData];
    }];
    
    self.navigationItem.leftBarButtonItem.rac_command = self.viewModel.menuCommand;
    self.navigationItem.rightBarButtonItem.rac_command = self.viewModel.addVehicleCommand;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.vehicles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLASS_NAME(UITableViewCell)];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CLASS_NAME(UITableViewCell)];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tintColor = FlatRedDark;
        cell.imageView.image = [[UIImage imageNamed:@"vehicle"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.textLabel.font = [UIFont systemFontOfSize:18];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    N7Vehicle *vehicle = self.viewModel.vehicles[indexPath.row];
    
    cell.imageView.tintColor = vehicle.color;
    cell.textLabel.text = vehicle.number;
    
    cell.accessoryType = [vehicle.number isEqualToString:self.viewModel.vehicles.selectedVehicleNumber] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    N7Vehicle *vehicle = self.viewModel.vehicles[indexPath.row];
    
    self.viewModel.vehicles.selectedVehicleNumber = vehicle.number;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        N7Vehicle *vehicle = self.viewModel.vehicles[indexPath.row];
        
        [self.viewModel.removeVehicleCommand execute:vehicle];
    }
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"vehicles_screen_empty_title", nil)];
}

@end
