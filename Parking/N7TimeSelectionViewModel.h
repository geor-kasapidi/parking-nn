//
//  N7TimeSelectionViewModel.h
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class N7Vehicle;
@class N7ParkingArea;

@interface N7TimeSelectionViewModel : N7ViewModel

@property (strong, nonatomic) N7Vehicle *vehicle;
@property (strong, nonatomic) N7ParkingArea *parkingArea;

@property (assign, nonatomic) NSUInteger time;
@property (assign, nonatomic) BOOL unlimited;

@property (strong, nonatomic, readonly) NSDate *endDate;

@property (strong, nonatomic, readonly) NSNumber *price;
@property (strong, nonatomic, readonly) NSNumber *balance;

@property (strong, nonatomic, readonly) RACCommand *startCommand;

@end
