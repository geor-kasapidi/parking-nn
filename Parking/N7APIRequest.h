//
//  N7APIRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIRequest : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *apiId;
@property (copy, nonatomic) NSString *apiVersion;
@property (copy, nonatomic) NSString *methodName;

- (Class)responseClass;

@end
