//
//  N7RegisterViewModel.m
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7RegisterViewModel.h"

@interface N7RegisterViewModel ()

@property (strong, nonatomic) RACCommand *termsAgreeCommand;
@property (strong, nonatomic) RACCommand *registerCommand;
@property (strong, nonatomic) RACCommand *loginCommand;

@end

@implementation N7RegisterViewModel

- (RACCommand *)termsAgreeCommand
{
    if (!_termsAgreeCommand) {
        _termsAgreeCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE showUserAgreement];
            
            return [RACSignal empty];
        }];
    }
    
    return _termsAgreeCommand;
}

- (RACCommand *)registerCommand
{
    if (!_registerCommand) {
        RACSignal *enabled = [[RACSignal combineLatest:@[[RACObserve(self, phone) map:^id(NSString *phone) {
            return @(phone.length == 10);
        }], [RACObserve(self, email) map:^id(NSString *email) {
            NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
            
            return @([[NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex] evaluateWithObject:email]);
        }], [RACObserve(self, firstName) map:^id(NSString *firstName) {
            return @(firstName.length > 1);
        }], [RACObserve(self, lastName) map:^id(NSString *lastName) {
            return @(lastName.length > 1);
        }], [RACObserve(self, contactType) map:^id(NSNumber *contactType) {
            return @([contactType integerValue] != N7ContactTypeNotSpecified);
        }], RACObserve(self, termsAgree)]] and];
        
        _registerCommand = [[RACCommand alloc] initWithEnabled:enabled signalBlock:^RACSignal *(id input) {
            [CORE registerWithPhone:self.phone
                              email:self.email
                          firstName:self.firstName
                           lastName:self.lastName
                               male:self.contactType == N7ContactTypeMr];
            
            return [RACSignal empty];
        }];
    }
    
    return _registerCommand;
}

- (RACCommand *)loginCommand
{
    if (!_loginCommand) {
        _loginCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE showLogin];
            
            return [RACSignal empty];
        }];
    }
    
    return _loginCommand;
}

@end
