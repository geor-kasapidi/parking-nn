//
//  N7APIRemoveVehicleRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 17.07.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRemoveVehicleRequest.h"

@implementation N7APIRemoveVehicleRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return [@{SEL_NAME(vehicleRequestData): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)vehicleRequestDataJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIVehicleRequestData class]];
}

- (NSString *)methodName {
    return @"remove_vehicle";
}

@end
