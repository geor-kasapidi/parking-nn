//
//  N7APIStartPaymentResult.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIStartPaymentResult : MTLModel <MTLJSONSerializing>

@property (assign, nonatomic) BOOL parked;

@end
