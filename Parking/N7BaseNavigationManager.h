//
//  N7BaseNavigationManager.h
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7DialogManager.h"

@interface N7BaseNavigationManager : NSObject

@property (strong, nonatomic, readonly) UIWindow *window;
@property (strong, nonatomic, readonly) N7DialogManager *dialogManager;
@property (strong, nonatomic, readonly) UIViewController *topViewController;

- (instancetype)initWithWindow:(UIWindow *)window;

- (void)setLoadingVisible:(BOOL)visible fullScreen:(BOOL)fullScreen;
- (void)setLoadingVisible:(BOOL)visible fullScreen:(BOOL)fullScreen animated:(BOOL)animated;

@end
