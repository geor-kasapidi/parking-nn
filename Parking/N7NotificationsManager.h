//
//  N7NotificationsManager.h
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@interface N7NotificationsManager : NSObject

- (BOOL)scheduleLocalNotificationWithKey:(NSString *)key fireDate:(NSDate *)fireDate body:(NSString *)body;

- (void)cancelLocalNotificationsByKey:(NSString *)key;
- (void)cancelAllLocalNotifications;

@end
