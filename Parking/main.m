//
//  main.m
//  Parking
//
//  Created by Георгий Касапиди on 21.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, CLASS_NAME(N7AppDelegate));
    }
}
