//
//  N7ActivePaymentsCollectionViewCell.h
//  Parking
//
//  Created by Георгий Касапиди on 07.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@class N7ActivePayment;
@class N7ParkingArea;

@interface N7ActivePaymentsCollectionViewCell : UICollectionViewCell

- (void)setActivePayment:(N7ActivePayment *)activePayment parkingArea:(N7ParkingArea *)parkingArea;

@end
