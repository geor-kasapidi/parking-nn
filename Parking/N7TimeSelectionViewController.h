//
//  N7TimeSelectionViewController.h
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewController.h"

#import "N7TimeSelectionViewModel.h"

@interface N7TimeSelectionViewController : N7ViewController

@property (strong, nonatomic) IBOutlet N7TimeSelectionViewModel *viewModel;

@end
