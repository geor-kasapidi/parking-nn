//
//  N7APIRequestModel.m
//  Parking
//
//  Created by Георгий Касапиди on 14.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRequestModel.h"

@implementation N7APIRequestModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(language): @"language"};
}

- (NSString *)language
{
    NSString *lang = [[[NSBundle mainBundle] preferredLocalizations] firstObject];
    
    return lang ?: @"en";
}

@end
