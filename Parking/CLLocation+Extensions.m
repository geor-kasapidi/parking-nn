//
//  CLLocation+Extensions.m
//  Parking
//
//  Created by Георгий Касапиди on 13.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "CLLocation+Extensions.h"

@implementation CLLocation (Extensions)

+ (CLLocationDistance)distanceFrom:(CLLocationCoordinate2D)fromCoordinate to:(CLLocationCoordinate2D)toCoordinate
{
    return [[[CLLocation alloc] initWithCoordinate:fromCoordinate] distanceFromLocation:[[CLLocation alloc] initWithCoordinate:toCoordinate]];
}

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    return [self initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
}

- (CLLocationDistance)distanceFrom:(CLLocationCoordinate2D)coordinate
{
    return [self distanceFromLocation:[[CLLocation alloc] initWithCoordinate:coordinate]];
}

@end
