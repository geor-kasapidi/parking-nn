//
//  N7TimeSelectionViewModel.m
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7TimeSelectionViewModel.h"

@interface N7TimeSelectionViewModel ()

@property (strong, nonatomic) NSDate *endDate;

@property (strong, nonatomic) NSNumber *price;
@property (strong, nonatomic) NSNumber *balance;

@property (strong, nonatomic) RACCommand *startCommand;

@end

@implementation N7TimeSelectionViewModel

- (void)awakeFromNib
{
    RAC(self, balance) = RACObserve(CORE, data.account.session.balance);
    
    @weakify(self);
    [[RACSignal combineLatest:@[RACObserve(self, parkingArea),
                               RACObserve(self, time),
                               RACObserve(self, unlimited)]
                      reduce:^id(N7ParkingArea *parkingArea, NSNumber *time, NSNumber *unlimited) {
                          NSDate *now = [NSDate date];
                          
                          NSNumber *price = [unlimited boolValue] ? @(INFINITY) : [parkingArea.days priceForMinutes:[time unsignedIntegerValue] fromDate:now];
                          NSDate *endDate = [unlimited boolValue] ? nil : [now dateByAddingMinutes:[time integerValue]];
                          
                          return RACTuplePack(price, endDate);
                      }] subscribeNext:^(RACTuple *values) {
                          @strongify(self);
                          
                          RACTupleUnpack(NSNumber *price, NSDate *endDate) = values;
                          
                          self.price = price;
                          self.endDate = endDate;
                      }];
}

#pragma mark -

- (RACCommand *)startCommand
{
    if (!_startCommand) {
        RACSignal *enabled = [RACSignal combineLatest:@[RACObserve(self, time),
                                                        RACObserve(self, unlimited),
                                                        RACObserve(self, price),
                                                        RACObserve(self, balance)]
                                               reduce:^id(NSNumber *time, NSNumber *unlimited, NSNumber *price, NSNumber *balance) {
                                                   return @([unlimited boolValue] || ([time unsignedIntegerValue] > 14 && [price doubleValue] < [balance doubleValue]));
                                               }];
        
        @weakify(self);
        _startCommand = [[RACCommand alloc] initWithEnabled:enabled signalBlock:^RACSignal *(id input) {
            @strongify(self);
            
            [CORE startVehicleParking:self.vehicle parkingArea:self.parkingArea minutes:self.unlimited ? nil : @(self.time)];
            
            return [RACSignal empty];
        }];
    }
    
    return _startCommand;
}

@end
