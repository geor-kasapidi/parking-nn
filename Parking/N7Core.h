//
//  N7Core.h
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#ifndef N7Core_h
#define N7Core_h

#import "N7AppDelegate.h"
#import "N7AppManager.h"
#import "N7AppData.h"

#define CORE [N7AppDelegate appDelegate].core

#endif
