//
//  N7AccountManager.m
//  Parking
//
//  Created by Георгий Касапиди on 21.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <UICKeyChainStore/UICKeyChainStore.h>

#import "N7AccountManager.h"
#import "N7Account.h"

@interface N7AccountManager ()

@property (strong, nonatomic) UICKeyChainStore *keyChainStore;

@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *apiKey;

@end

@implementation N7AccountManager

- (instancetype)initWithService:(NSString *)service
{
    if (self = [super init]) {
        self.keyChainStore = [UICKeyChainStore keyChainStoreWithService:service];
    }
    
    return self;
}

#pragma mark -

- (N7Account *)account
{
    NSString *phone = self.phone;
    
    if (phone.length > 0) {
        NSString *apiKey = self.apiKey;
        
        if (apiKey.length > 0) {
            N7Account *account = [N7Account new];
            
            account.phone = phone;
            account.apiKey = apiKey;
            
            return account;
        }
    }
    
    return nil;
}

- (void)setAccount:(N7Account *)account
{
    [self willChangeValueForKey:SEL_NAME(account)];
    
    self.phone = account.phone;
    self.apiKey = account.apiKey;
    
    [self didChangeValueForKey:SEL_NAME(account)];
}

#pragma mark -

- (NSString *)phone
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:SEL_NAME(phone)];
}

- (void)setPhone:(NSString *)phone
{
    [[NSUserDefaults standardUserDefaults] setObject:phone forKey:SEL_NAME(phone)];
    
    if (phone.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:phone forKey:SEL_NAME(lastSavedPhone)];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSavedPhone
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:SEL_NAME(lastSavedPhone)];
}

#pragma mark -

- (NSString *)apiKey
{
    return self.keyChainStore[SEL_NAME(apiKey)];
}

- (void)setApiKey:(NSString *)apiKey
{
    self.keyChainStore[SEL_NAME(apiKey)] = apiKey;
}

@end
