//
//  N7MessagesViewModel.m
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7MessagesViewModel.h"

@interface N7MessagesViewModel ()

@property (strong, nonatomic) NSArray<N7Message *> *messages;

@property (strong, nonatomic) NSNumber *balance;

@property (strong, nonatomic) RACCommand *menuCommand;

@end

@implementation N7MessagesViewModel

- (void)awakeFromNib
{
    RAC(self, balance) = RACObserve(CORE, data.account.session.balance);
    RAC(self, messages) = RACObserve(CORE, data.account.session.messages);
}

#pragma mark -

- (RACCommand *)menuCommand
{
    if (!_menuCommand) {
        _menuCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE toggleMenu];
            
            return [RACSignal empty];
        }];
    }
    
    return _menuCommand;
}

@end
