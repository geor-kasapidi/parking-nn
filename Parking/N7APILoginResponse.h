//
//  N7APILoginResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIAccount;

@interface N7APILoginResponse : N7APIResponse

@property (strong, nonatomic) N7APIAccount *account;

@end
