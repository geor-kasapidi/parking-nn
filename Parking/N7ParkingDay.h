//
//  N7ParkingDay.h
//  Parking
//
//  Created by Георгий Касапиди on 27.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@interface N7ParkingDay : NSObject <NSCopying>

+ (instancetype)parkingDayFromStartTimeString:(NSString *)startTimeString // hh:mm
                                endTimeString:(NSString *)endTimeString // hh:mm
                                   rateString:(NSString *)rateString // xx.yy
                              minAmountString:(NSString *)minAmountString; // xx.yy

@property (assign, nonatomic) NSUInteger startTime;
@property (assign, nonatomic) NSUInteger endTime;
@property (strong, nonatomic) NSNumber *pricePerMinute;
@property (strong, nonatomic) NSNumber *minPrice;

- (NSString *)worktimeString;
- (NSString *)pricePerHourString;

@end
