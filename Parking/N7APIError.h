//
//  N7APIError.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIError : MTLModel <MTLJSONSerializing>

@property (nonatomic) id code;
@property (copy, nonatomic) NSString *message;

@end
