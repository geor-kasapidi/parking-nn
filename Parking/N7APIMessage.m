//
//  N7APIMessage.m
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIMessage.h"

@implementation N7APIMessage

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(title): @"title",
             SEL_NAME(message): @"message",
             SEL_NAME(date): @"date"};
}

@end
