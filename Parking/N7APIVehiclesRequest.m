//
//  N7APIVehiclesRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIVehiclesRequest.h"
#import "N7APISession.h"
#import "N7APIVehiclesResponse.h"

@implementation N7APIVehiclesRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(session): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)sessionJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APISession class]];
}

- (NSString *)methodName
{
    return @"vehicles";
}

- (Class)responseClass
{
    return [N7APIVehiclesResponse class];
}

@end
