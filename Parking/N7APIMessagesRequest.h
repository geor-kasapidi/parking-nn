//
//  N7APIMessagesRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APISession;

@interface N7APIMessagesRequest : N7APIRequest

@property (strong, nonatomic) N7APISession *session;

@end
