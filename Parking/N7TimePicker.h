//
//  N7TimePicker.h
//  Parking
//
//  Created by Георгий Касапиди on 03.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@interface N7TimePicker : UIPickerView

@property (assign, nonatomic) NSInteger minTime;
@property (assign, nonatomic) NSInteger time;

@property (assign, nonatomic) BOOL active;

@property (strong, nonatomic) UIFont *font;

@end
