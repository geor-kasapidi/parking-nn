//
//  N7MessagesTableViewCell.m
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7MessagesTableViewCell.h"
#import "N7Label.h"
#import "N7Message.h"

@interface N7MessagesTableViewCell ()

@property (weak, nonatomic) IBOutlet N7Label *dateLabel;
@property (weak, nonatomic) IBOutlet N7Label *titleLabel;
@property (weak, nonatomic) IBOutlet N7Label *messageLabel;

@end

@implementation N7MessagesTableViewCell

- (void)setMessage:(N7Message *)message
{
    _message = message;
    
    self.dateLabel.text = [[NSDate date] timeAgoSinceDate:message.date];
    self.titleLabel.text = message.title;
    self.messageLabel.text = message.message;
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

@end
