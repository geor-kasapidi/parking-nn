//
//  N7AccountSettings.h
//  Parking
//
//  Created by Георгий Касапиди on 13.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@interface N7AccountSettings : NSObject

@property (copy, nonatomic) NSString *accountType;
@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;

- (NSString *)fullName;

@end
