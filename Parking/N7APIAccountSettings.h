//
//  N7APIAccountSettings.h
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIAccountSettings : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *accountType;
@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (copy, nonatomic) NSArray *parkedReminderIntervalOptions;
@property (assign, nonatomic) NSInteger parkedReminderInterval;
@property (assign, nonatomic) BOOL isReplyEnabled;
@property (copy, nonatomic) NSArray *lowNotificationThresholdOptions;
@property (assign, nonatomic) NSInteger lowNotificationThreshold;
@property (copy, nonatomic) NSString *preferredLanguage;

@end
