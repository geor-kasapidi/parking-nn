//
//  N7ActivePaymentsViewModel.h
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class N7ActivePayment;
@class N7ParkingAreasCollection;

@interface N7ActivePaymentsViewModel : N7ViewModel

@property (strong, nonatomic, readonly) NSArray<N7ActivePayment *> *activePayments;
@property (strong, nonatomic, readonly) N7ParkingAreasCollection *parkingAreas;

@property (strong, nonatomic, readonly) NSNumber *balance;

@property (strong, nonatomic, readonly) RACCommand *menuCommand;
@property (strong, nonatomic, readonly) RACCommand *stopCommand;
@property (strong, nonatomic, readonly) RACCommand *prolongCommand;

@end
