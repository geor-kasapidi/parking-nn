//
//  N7APIProlongParkingResult.m
//  Parking
//
//  Created by Geor Kasapidi on 09.02.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIProlongParkingResult.h"

@implementation N7APIProlongParkingResult

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(endTime): @"end_time"};
}

@end
