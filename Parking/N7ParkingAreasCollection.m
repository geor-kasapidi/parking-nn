//
//  N7ParkingAreasCollection.m
//  Parking
//
//  Created by Георгий Касапиди on 22.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7ParkingAreasCollection.h"

@interface N7ParkingAreasCollection ()

@property (copy, nonatomic) NSArray<N7ParkingArea *> *parkingAreas;

@end

@implementation N7ParkingAreasCollection

- (instancetype)initWithItems:(NSArray<N7ParkingArea *> *)items
{
    if (self = [super init]) {
        self.parkingAreas = [items copy];
    }
    
    return self;
}

- (NSUInteger)count
{
    return self.parkingAreas.count;
}

- (N7ParkingArea *)objectAtIndexedSubscript:(NSUInteger)idx
{
    return idx < self.parkingAreas.count ? self.parkingAreas[idx] : nil;
}

- (N7ParkingArea *)objectForKeyedSubscript:(NSString *)key
{
    if (key.length > 0) {
        return [[self.parkingAreas linq_where:^BOOL(N7ParkingArea *parkingArea) {
            return [parkingArea.code isEqualToString:key];
        }] linq_firstOrNil];
    }
    
    return nil;
}

@end
