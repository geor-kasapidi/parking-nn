//
//  N7TimeSelectionViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7TimeSelectionViewController.h"
#import "N7Label.h"
#import "N7Button.h"
#import "N7TimePicker.h"

@interface N7TimeSelectionViewController () <N7ViewPreferredActions>

@property (weak, nonatomic) IBOutlet N7Label *worktimeLabel;
@property (weak, nonatomic) IBOutlet N7Label *pricePerHourLabel;
@property (weak, nonatomic) IBOutlet N7Label *hourLabel;
@property (weak, nonatomic) IBOutlet N7Label *minuteLabel;
@property (weak, nonatomic) IBOutlet N7Label *unlimitedLabel;
@property (weak, nonatomic) IBOutlet N7Label *timeLabel;
@property (weak, nonatomic) IBOutlet N7Label *priceLabel;
@property (weak, nonatomic) IBOutlet N7TimePicker *timePicker;
@property (weak, nonatomic) IBOutlet UISwitch *unlimitedSwitcher;
@property (weak, nonatomic) IBOutlet N7Button *startButton;

@end

@implementation N7TimeSelectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
    [self localizeUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.view.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:self.view.bounds andColors:@[[UIColor whiteColor], FlatWhite]];
}

#pragma mark - N7ViewPreferredActions

- (void)setupUI
{
    self.timePicker.minTime = 30;
    self.timePicker.time = 60;
}

- (void)bindUI
{
    RAC(self, title) = [RACObserve(self.viewModel, balance) map:^id(NSNumber *balance) {
        return [balance currencyString];
    }];
    
    @weakify(self);
    [RACObserve(self.viewModel, parkingArea.days) subscribeNext:^(N7ParkingDaysCollection *days) {
        @strongify(self);
        
        self.worktimeLabel.text = [days.currentDay worktimeString];
        self.pricePerHourLabel.text = [days.currentDay pricePerHourString];
    }];

    RAC(self.viewModel, time) = RACObserve(self.timePicker, time);
    RAC(self.viewModel, unlimited) = self.unlimitedSwitcher.rac_newOnChannel;
    RAC(self.timePicker, active) = [RACObserve(self.viewModel, unlimited) not];
    [self.timeLabel rac_liftSelector:@selector(setText:animated:) withSignals:[RACObserve(self.viewModel, endDate) map:^id(NSDate *endDate) {
        return [NSString stringWithFormat:NSLocalizedString(@"up_to_time", nil), endDate ? [endDate timeString] : @""];
    }], [RACSignal return:@YES], nil];
    [self.priceLabel rac_liftSelector:@selector(setText:animated:) withSignals:[RACObserve(self.viewModel, price) map:^id(NSNumber *price) {
        return [price currencyString];
    }], [RACSignal return:@YES], nil];
    
    self.startButton.rac_command = self.viewModel.startCommand;
}

- (void)localizeUI
{
    self.unlimitedLabel.text = NSLocalizedString(@"time_selection_screen_unlimited_label_text", nil);
    self.hourLabel.text = NSLocalizedString(@"time_selection_screen_hour_label_text", nil);
    self.minuteLabel.text = NSLocalizedString(@"time_selection_screen_minute_label_text", nil);
}

@end
