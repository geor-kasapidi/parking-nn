//
//  N7APIActivePayment.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIActivePayment : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *vehicle;
@property (copy, nonatomic) NSString *zone;
@property (assign, nonatomic) double startTime;
@property (assign, nonatomic) double currentTime;
@property (assign, nonatomic) double endTime;

@end
