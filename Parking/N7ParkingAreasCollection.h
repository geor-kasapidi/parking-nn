//
//  N7ParkingAreasCollection.h
//  Parking
//
//  Created by Георгий Касапиди on 22.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7ParkingArea.h"

@interface N7ParkingAreasCollection : NSObject

@property (assign, nonatomic, readonly) NSUInteger count;

- (instancetype)initWithItems:(NSArray<N7ParkingArea *> *)items;

- (N7ParkingArea *)objectAtIndexedSubscript:(NSUInteger)idx;
- (N7ParkingArea *)objectForKeyedSubscript:(NSString *)key;

@end
