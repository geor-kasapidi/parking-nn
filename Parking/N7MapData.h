//
//  N7MapData.h
//  Parking
//
//  Created by Георгий Касапиди on 06.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@interface N7MapData : NSObject

@property (copy, nonatomic) NSArray<MKPointAnnotation *> *points;
@property (copy, nonatomic) NSArray<MKPolyline *> *polylines;
@property (copy, nonatomic) NSArray<MKPolygon *> *polygons;

@end
