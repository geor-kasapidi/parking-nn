//
//  N7APIActivePaymentsResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIActivePayment;

@interface N7APIActivePaymentsResponse : N7APIResponse

@property (strong, nonatomic) NSArray<N7APIActivePayment *> *activePayments;

@end
