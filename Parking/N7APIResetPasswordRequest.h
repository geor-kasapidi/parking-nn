//
//  N7APIResetPasswordRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 14.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APIResetPassword;

@interface N7APIResetPasswordRequest : N7APIRequest

@property (strong, nonatomic) N7APIResetPassword *resetPassword;

@end
