//
//  N7APIStopPaymentRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIStopPaymentRequest.h"
#import "N7APIStopPayment.h"
#import "N7APIStopPaymentResponse.h"

@implementation N7APIStopPaymentRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(stopPayment): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)stopPaymentJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIStopPayment class]];
}

- (NSString *)methodName
{
    return @"stop_payment";
}

- (Class)responseClass
{
    return [N7APIStopPaymentResponse class];
}

@end
