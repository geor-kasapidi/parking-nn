//
//  N7NavigationManager.m
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@import SafariServices;

#import "TOWebViewController.h"

#import "UIWindow+Root.h"

#import "N7NavigationManager.h"

#import "N7SlideMenuViewController.h"
#import "N7NavigationController.h"
#import "N7ViewController.h"

#import "N7LoginViewController.h"
#import "N7RegisterViewController.h"
#import "N7LoadingViewController.h"
#import "N7MenuViewController.h"
#import "N7ParkingAreaSelectionViewController.h"
#import "N7TimeSelectionViewController.h"
#import "N7ActivePaymentsViewController.h"
#import "N7AccountHistoryViewController.h"
#import "N7MessagesViewController.h"
#import "N7VehiclesViewController.h"

@interface N7NavigationManager ()

@property (strong, nonatomic) UIStoryboard *storyboard;

@property (strong, nonatomic, readonly) N7SlideMenuViewController *slideMenuViewController;
@property (strong, nonatomic) UIViewController *centerViewController;

@end

@implementation N7NavigationManager

- (instancetype)initWithWindow:(UIWindow *)window storyboardName:(NSString *)storyboardName
{
    if (self = [super initWithWindow:window]) {
        self.storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:[NSBundle mainBundle]];
    }
    
    return self;
}

- (UIViewController *)topViewController
{
    return [self topViewControllerWithRootViewController:self.window.rootViewController];
}

- (UIViewController *)topViewControllerWithRootViewController:(UIViewController *)rootViewController
{
    if ([rootViewController isKindOfClass:[N7SlideMenuViewController class]]) {
        return [self topViewControllerWithRootViewController:[(N7SlideMenuViewController *)rootViewController contentViewController]];
    }
    
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        return [self topViewControllerWithRootViewController:[(UITabBarController *)rootViewController selectedViewController]];
    }
    
    if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        return [self topViewControllerWithRootViewController:[(UINavigationController *)rootViewController visibleViewController]];
    }
    
    return rootViewController;
}

- (N7SlideMenuViewController *)slideMenuViewController
{
    if (self.rootContext == N7NavigationManagerRootContextMenu) {
        return (N7SlideMenuViewController *)self.window.rootViewController;
    }
    
    return nil;
}

- (UIViewController *)centerViewController
{
    UIViewController *contentViewController = self.slideMenuViewController.contentViewController;
    
    return [contentViewController isKindOfClass:[UINavigationController class]] ? [(UINavigationController *)contentViewController visibleViewController] : contentViewController;
}

- (void)setCenterViewController:(UIViewController *)centerViewController
{
    self.slideMenuViewController.contentViewController = centerViewController;
}

#pragma mark -

- (N7NavigationManagerRootContext)rootContext
{
    UIViewController *rootViewController = self.window.rootViewController;
    
    if (!rootViewController) {
        return N7NavigationManagerRootContextNone;
    }
    
    if ([rootViewController isKindOfClass:[N7LoginViewController class]]) {
        return N7NavigationManagerRootContextLogin;
    }
    
    if ([rootViewController isKindOfClass:[N7RegisterViewController class]]) {
        return N7NavigationManagerRootContextRegister;
    }
    
    if ([rootViewController isKindOfClass:[N7LoadingViewController class]]) {
        return N7NavigationManagerRootContextLoading;
    }
    
    if ([rootViewController isKindOfClass:[N7SlideMenuViewController class]]) {
        return N7NavigationManagerRootContextMenu;
    }
    
    return N7NavigationManagerRootContextUnknown;
}

- (N7NavigationManagerCenterContext)centerContext
{
    UIViewController *centerViewController = self.centerViewController;
    
    if ([centerViewController isKindOfClass:[N7ParkingAreaSelectionViewController class]] ||
        [centerViewController isKindOfClass:[N7TimeSelectionViewController class]]) {
        return N7NavigationManagerCenterContextParking;
    }
    
    if ([centerViewController isKindOfClass:[N7ActivePaymentsViewController class]]) {
        return N7NavigationManagerCenterContextActivePayments;
    }
    
    if ([centerViewController isKindOfClass:[N7AccountHistoryViewController class]]) {
        return N7NavigationManagerCenterContextAccountHistory;
    }
    
    if ([centerViewController isKindOfClass:[N7MessagesViewController class]]) {
        return N7NavigationManagerCenterContextMessages;
    }
    
    if ([centerViewController isKindOfClass:[N7VehiclesViewController class]]) {
        return N7NavigationManagerCenterContextVehicles;
    }
    
    return N7NavigationManagerCenterContextUnknown;
}

#pragma mark -

- (void)toggleMenu
{
    self.slideMenuViewController.menuVisible = !self.slideMenuViewController.menuVisible;
}

- (void)openWebBrowserWithURL:(NSURL *)url
{
    if (IOS_VERSION < 9) {
        TOWebViewController *webViewController = [[TOWebViewController alloc] initWithURL:url];
        
        [self.topViewController presentViewController:[[UINavigationController alloc] initWithRootViewController:webViewController] animated:YES completion:nil];
    } else {
        SFSafariViewController *safariViewController = [[SFSafariViewController alloc] initWithURL:url];
        
        [self.topViewController presentViewController:[[UINavigationController alloc] initWithRootViewController:safariViewController] animated:YES completion:nil];
        
        safariViewController.navigationController.navigationBarHidden = YES;
    }
}

#pragma mark -

- (BOOL)showLogin
{
    UIViewAnimationOptions transitionStyle = UIViewAnimationOptionTransitionNone;
    
    switch (self.rootContext) {
        case N7NavigationManagerRootContextRegister:
            transitionStyle = UIViewAnimationOptionTransitionFlipFromRight;
            break;
        case N7NavigationManagerRootContextMenu:
            transitionStyle = UIViewAnimationOptionTransitionCurlDown;
            break;
        case N7NavigationManagerRootContextLoading:
            transitionStyle = UIViewAnimationOptionTransitionCrossDissolve;
            break;
        default:
            transitionStyle = UIViewAnimationOptionTransitionNone;
            break;
    }
    
    [self.window changeRootViewControllerTo:[self.storyboard instantiateViewControllerWithIdentifier:CLASS_NAME(N7LoginViewController)] withTransitionStyle:transitionStyle];
    
    return YES;
}

- (BOOL)showRegister
{
    if (self.rootContext == N7NavigationManagerRootContextLogin) {
        [self.window changeRootViewControllerTo:[self.storyboard instantiateViewControllerWithIdentifier:CLASS_NAME(N7RegisterViewController)] withTransitionStyle:UIViewAnimationOptionTransitionFlipFromLeft];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)showLoading
{
    if (self.rootContext == N7NavigationManagerRootContextNone) {
        [self.window changeRootViewControllerTo:[self.storyboard instantiateViewControllerWithIdentifier:CLASS_NAME(N7LoadingViewController)] withTransitionStyle:UIViewAnimationOptionTransitionNone];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)showMenu
{
    if (self.rootContext == N7NavigationManagerRootContextLogin || self.rootContext == N7NavigationManagerRootContextLoading) {
        N7SlideMenuViewController *slideMenuViewController = [N7SlideMenuViewController new];
        
        slideMenuViewController.menuWidth = CGRectGetWidth(MAIN_SCREEN.bounds) * 4 / 5;
        
        slideMenuViewController.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:CLASS_NAME(N7MenuViewController)];
        slideMenuViewController.contentViewController = [UIViewController new];
        
        if (self.rootContext == N7NavigationManagerRootContextLogin) {
            [self.window changeRootViewControllerTo:slideMenuViewController withTransitionStyle:UIViewAnimationOptionTransitionCurlUp];
        } else if (self.rootContext == N7NavigationManagerRootContextLoading) {
            [self.window changeRootViewControllerTo:slideMenuViewController withTransitionStyle:UIViewAnimationOptionTransitionCrossDissolve];
        }
        
        return YES;
    }
    
    return NO;
}

#pragma mark -

- (BOOL)showCenterScreenWithId:(NSString *)screenId itemIndex:(NSUInteger)itemIndex
{
    if (self.rootContext == N7NavigationManagerRootContextMenu) {
        if (![self.centerViewController.restorationIdentifier isEqualToString:screenId]) {
            self.centerViewController = [[N7NavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:screenId]];
        }
        
        if ([self.slideMenuViewController.menuViewController isKindOfClass:[N7MenuViewController class]]) {
            [(N7MenuViewController *)self.slideMenuViewController.menuViewController setSelectedItemIndex:itemIndex];
        }
        
        return YES;
    }
    
    return NO;
}

- (BOOL)showParkingAreas
{
    return [self showCenterScreenWithId:CLASS_NAME(N7ParkingAreaSelectionViewController)
                              itemIndex:N7NavigationManagerCenterContextParking];
}

- (BOOL)showActivePayments
{
    return [self showCenterScreenWithId:CLASS_NAME(N7ActivePaymentsViewController)
                              itemIndex:N7NavigationManagerCenterContextActivePayments];
}

- (BOOL)showAccountHistory
{
    return [self showCenterScreenWithId:CLASS_NAME(N7AccountHistoryViewController)
                              itemIndex:N7NavigationManagerCenterContextAccountHistory];
}

- (BOOL)showMessages
{
    return [self showCenterScreenWithId:CLASS_NAME(N7MessagesViewController)
                              itemIndex:N7NavigationManagerCenterContextMessages];
}

- (BOOL)showVehicles
{
    return [self showCenterScreenWithId:CLASS_NAME(N7VehiclesViewController)
                              itemIndex:N7NavigationManagerCenterContextVehicles];
}

#pragma mark -

- (BOOL)showTimeSelectionWithVehicle:(N7Vehicle *)vehicle parkingArea:(N7ParkingArea *)parkingArea
{
    UIViewController *centerViewController = self.centerViewController;
    
    if (centerViewController.navigationController) {
        N7TimeSelectionViewController *timeSelectionViewController = [self.storyboard instantiateViewControllerWithIdentifier:CLASS_NAME(N7TimeSelectionViewController)];
        
        timeSelectionViewController.viewModel.parkingArea = parkingArea;
        timeSelectionViewController.viewModel.vehicle = vehicle;
        
        [centerViewController.navigationController pushViewController:timeSelectionViewController animated:YES];
        
        return YES;
    }
    
    return NO;
}

@end
