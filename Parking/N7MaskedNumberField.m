//
//  N7MaskedNumberField.m
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "NSString+Mask.h"

#import "N7MaskedNumberField.h"

@interface N7MaskedNumberField () <UITextFieldDelegate>

@property (strong, nonatomic) RACSubject *valueSignal;

@end

@implementation N7MaskedNumberField

- (void)awakeFromNib
{
    self.delegate = self;
    self.keyboardType = UIKeyboardTypePhonePad;
}

#pragma mark -

- (void)setText:(NSString *)text
{
    [super setText:[text maskedValueWithMask:self.mask maskCharacter:self.maskCharacter]];
    
    [self.valueSignal sendNext:self.value];
}

- (NSString *)value
{
    return [self.text unmaskedValueWithMask:self.mask maskCharacter:self.maskCharacter];
}

#pragma mark -

- (RACSubject *)valueSignal
{
    if (!_valueSignal) {
        _valueSignal = [RACSubject subject];
    }
    
    return _valueSignal;
}

- (RACSignal *)rac_textSignal
{
    return self.valueSignal;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *value = self.value;
    
    if (string.length > 0) {
        value = [value stringByAppendingString:string];
    } else {
        value = [value substringToIndex:value.length - 1];
    }
    
    self.text = value;
    
    return NO;
}

@end
