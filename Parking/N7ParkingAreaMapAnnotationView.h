//
//  N7ParkingAreaMapAnnotationView.h
//  Parking
//
//  Created by Георгий Касапиди on 02.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@interface N7ParkingAreaMapAnnotationView : MKAnnotationView

@property (assign, nonatomic) NSUInteger count;

@end
