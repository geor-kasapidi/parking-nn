//
//  N7APIResetPasswordResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 14.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIResponse.h"

@interface N7APIResetPasswordResponse : N7APIResponse

@end
