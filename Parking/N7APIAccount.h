//
//  N7APIAccount.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIAccount : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *apiKey;
@property (copy, nonatomic) NSString *accountType;

@end
