//
//  N7DataManager.h
//  Parking
//
//  Created by Георгий Касапиди on 18.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@class N7NotificationsManager;

@class N7Account;
@class N7Vehicle;

@interface N7DataManager : NSObject

- (instancetype)initWithNotificationsManager:(N7NotificationsManager *)notificationsManager
                                     baseURL:(NSURL *)url;

- (RACSignal *)loginWithPhone:(NSString *)phone password:(NSString *)password;
- (RACSignal *)resetPasswordForPhone:(NSString *)phone;
- (RACSignal *)registerWithPhone:(NSString *)phone
                           email:(NSString *)email
                       firstName:(NSString *)firstName
                        lastName:(NSString *)lastName
                            male:(BOOL)male;

- (RACSignal *)loadAccountBalance:(N7Account *)account;
- (RACSignal *)loadAccountParkingAreas:(N7Account *)account;
- (RACSignal *)loadAccountSettings:(N7Account *)account;
- (RACSignal *)loadAccountActivePayments:(N7Account *)account;
- (RACSignal *)loadAccountHistory:(N7Account *)account;
- (RACSignal *)loadAccountMessages:(N7Account *)account;

- (RACSignal *)startPaymentWithAccount:(N7Account *)account
                              zoneCode:(NSString *)zoneCode
                               vehicle:(NSString *)vehicle
                               minutes:(NSInteger)minutes
                             unlimited:(BOOL)unlimited;
- (RACSignal *)prolongParkingWithAccount:(N7Account *)account
                                 vehicle:(NSString *)vehicle
                                 minutes:(NSInteger)minutes;
- (RACSignal *)stopPaymentWithAccount:(N7Account *)account vehicle:(NSString *)vehicle;

- (RACSignal *)loadMapObjects;

- (RACSignal *)clearData;

@end

@interface N7DataManager (Vehicles)

- (RACSignal *)loadAccountVehicles:(N7Account *)account;
- (RACSignal *)addVehicleWithNumber:(NSString *)number toAccount:(N7Account *)account;
- (RACSignal *)removeVehicle:(N7Vehicle *)vehicle fromAccount:(N7Account *)account;

@end

@interface N7DataManager (Extensions)

- (RACSignal *)loadAccountSessionData:(N7Account *)account;
- (RACSignal *)loadAccountData:(N7Account *)account;
- (RACSignal *)loadAppData:(N7Account *)account;
- (RACSignal *)reloadAccountSessionData:(N7Account *)account withInterval:(NSTimeInterval)interval;

@end
