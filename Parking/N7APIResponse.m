//
//  N7APIResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"
#import "N7APIError.h"

@implementation N7APIResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(error): @"error"};
}

+ (NSValueTransformer *)errorJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIError class]];
}

- (id)resultObject
{
    return nil;
}

@end
