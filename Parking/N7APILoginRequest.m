//
//  N7APILoginRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APILoginRequest.h"
#import "N7APILogin.h"
#import "N7APILoginResponse.h"

@implementation N7APILoginRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(login): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)loginJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APILogin class]];
}

- (NSString *)methodName
{
    return @"login";
}

- (Class)responseClass
{
    return [N7APILoginResponse class];
}

@end
