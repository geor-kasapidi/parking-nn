//
//  N7APIAccountHistoryEntry.h
//  Parking
//
//  Created by Георгий Касапиди on 17.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIAccountHistoryEntry : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *dateString;
@property (copy, nonatomic) NSString *contentType;
@property (copy, nonatomic) NSString *amount;
@property (copy, nonatomic) NSString *vehicle;
@property (copy, nonatomic) NSString *zoneCode;
@property (copy, nonatomic) NSString *message;

@end
