//
//  N7APIAccountSettingsRequest.h
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIRequest.h"

@class N7APISession;

@interface N7APIAccountSettingsRequest : N7APIRequest

@property (strong, nonatomic) N7APISession *session;

@end
