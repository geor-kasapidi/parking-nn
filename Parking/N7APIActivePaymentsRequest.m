//
//  N7APIActivePaymentsRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIActivePaymentsRequest.h"
#import "N7APISession.h"
#import "N7APIActivePaymentsResponse.h"

@implementation N7APIActivePaymentsRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(session): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)sessionJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APISession class]];
}

- (NSString *)methodName
{
    return @"active_payments";
}

- (Class)responseClass
{
    return [N7APIActivePaymentsResponse class];
}

@end
