//
//  N7APIBalanceRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIBalanceRequest.h"
#import "N7APISession.h"
#import "N7APIBalanceResponse.h"

@implementation N7APIBalanceRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(session): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)sessionJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APISession class]];
}

- (NSString *)methodName
{
    return @"account_funds";
}

- (Class)responseClass
{
    return [N7APIBalanceResponse class];
}

@end
