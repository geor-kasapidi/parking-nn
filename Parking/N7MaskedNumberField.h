//
//  N7MaskedNumberField.h
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@interface N7MaskedNumberField : UITextField

@property (copy, nonatomic) IBInspectable NSString *mask;
@property (copy, nonatomic) IBInspectable NSString *maskCharacter;

@property (strong, nonatomic, readonly) NSString *value;

@property (strong, nonatomic, readonly) RACSubject *valueSignal;

@end
