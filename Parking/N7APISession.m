//
//  N7APISession.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APISession.h"

@implementation N7APISession

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(phone): @"phone",
              SEL_NAME(apiKey): @"apikey"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

@end
