//
//  N7LoginViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 05.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7LoginViewController.h"
#import "N7LoginViewModel.h"
#import "N7Label.h"
#import "N7Button.h"
#import "N7MaskedNumberField.h"

@interface N7LoginViewController () <N7ViewPreferredActions>

@property (weak, nonatomic) IBOutlet N7Label *titleLabel;

@property (weak, nonatomic) IBOutlet N7MaskedNumberField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet N7Button *loginButton;
@property (weak, nonatomic) IBOutlet N7Button *registerButton;
@property (weak, nonatomic) IBOutlet N7Button *forgotPasswordButton;

@property (strong, nonatomic) IBOutlet N7LoginViewModel *viewModel;

@end

@implementation N7LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self bindUI];
    [self localizeUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.view.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:self.view.bounds andColors:@[[UIColor whiteColor], FlatWhite]];
}

#pragma mark - N7ViewPreferredActions

- (void)bindUI
{
    self.phoneTextField.text = self.viewModel.phone;
    
    RAC(self.viewModel, phone) = self.phoneTextField.rac_textSignal;
    RAC(self.viewModel, password) = [self.passwordTextField.rac_textSignal map:^id(NSString *password) {
        return [password trim];
    }];
    
    self.loginButton.rac_command = self.viewModel.loginCommand;
    self.registerButton.rac_command = self.viewModel.registerCommand;
    self.forgotPasswordButton.rac_command = self.viewModel.forgotPasswordCommand;
}

- (void)localizeUI
{
    self.titleLabel.text = NSLocalizedString(@"parking_title", nil);
    self.phoneTextField.placeholder = NSLocalizedString(@"login_screen_phone_input_placeholder", nil);
    self.passwordTextField.placeholder = NSLocalizedString(@"login_screen_password_input_placeholder", nil);
    
    [self.loginButton setTitle:NSLocalizedString(@"login_screen_sign_in_button_title", nil) forState:UIControlStateNormal];
    [self.registerButton setTitle:NSLocalizedString(@"login_screen_register_button_title", nil) forState:UIControlStateNormal];
    [self.forgotPasswordButton setTitle:NSLocalizedString(@"login_screen_forgot_password_button_title", nil) forState:UIControlStateNormal];
}

@end
