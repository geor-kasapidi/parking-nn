//
//  N7APILogin.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APILogin.h"

@implementation N7APILogin

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(phone): @"phone",
              SEL_NAME(password): @"password"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

@end
