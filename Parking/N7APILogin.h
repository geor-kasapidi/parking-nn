//
//  N7APILogin.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIRequestModel.h"

@interface N7APILogin : N7APIRequestModel

@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *password;

@end
