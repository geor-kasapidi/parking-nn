//
//  N7Account.h
//  Parking
//
//  Created by Георгий Касапиди on 06.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@interface N7Account : NSObject

@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *apiKey;

@end
