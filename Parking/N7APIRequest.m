//
//  N7APIRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIRequest.h"
#import "N7APIResponse.h"

@implementation N7APIRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(apiId): @"id",
             SEL_NAME(apiVersion): @"jsonrpc",
             SEL_NAME(methodName): @"method"};
}

- (NSString *)apiVersion
{
    return @"2.0";
}

- (NSString *)apiId
{
    return @"1";
}

- (Class)responseClass
{
    return [N7APIResponse class];
}

@end
