//
//  N7APIProlongParking.h
//  Parking
//
//  Created by Geor Kasapidi on 09.02.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APISession.h"

@interface N7APIProlongParking : N7APISession

@property (copy, nonatomic) NSString *vehicle;
@property (assign, nonatomic) NSInteger minutes;

@end
