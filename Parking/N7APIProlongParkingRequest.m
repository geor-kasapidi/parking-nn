//
//  N7APIProlongParkingRequest.m
//  Parking
//
//  Created by Geor Kasapidi on 09.02.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIProlongParkingRequest.h"
#import "N7APIProlongParking.h"
#import "N7APIProlongParkingResponse.h"

@implementation N7APIProlongParkingRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(prolongParking): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)prolongParkingJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIProlongParking class]];
}

- (NSString *)methodName
{
    return @"prolong_parking";
}

- (Class)responseClass
{
    return [N7APIProlongParkingResponse class];
}

@end
