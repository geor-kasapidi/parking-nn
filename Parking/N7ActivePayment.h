//
//  N7ActivePayment.h
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@interface N7ActivePayment : NSObject

@property (copy, nonatomic) NSString *parkingAreaCode;
@property (copy, nonatomic) NSString *vehicleNumber;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (assign, nonatomic) NSTimeInterval delta;

- (NSDate *)completedSoonNotificationFireDate;
- (NSString *)completedSoonNotificationText;

@end
