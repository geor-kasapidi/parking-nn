//
//  N7APIResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@class N7APIError;

@interface N7APIResponse : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) N7APIError *error;

- (id)resultObject;

@end
