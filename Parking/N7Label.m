//
//  N7Label.m
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Label.h"

@implementation N7Label

- (void)layoutSubviews
{
    if (self.numberOfLines == 0) {
        self.preferredMaxLayoutWidth = CGRectGetWidth(self.bounds);
    }
    
    [super layoutSubviews];
}

- (void)setText:(NSString *)text
{
    [self setText:text animated:NO];
}

- (void)setText:(NSString *)text animated:(BOOL)animated
{
    if ([text isEqualToString:self.text]) {
        return;
    }
    
    if (!animated) {
        [super setText:text];
        
        return;
    }
    
    [UIView transitionWithView:self
                      duration:.2
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [super setText:text];
                    }
                    completion:nil];
}

@end
