//
//  N7APIMessagesReponse.m
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIMessagesReponse.h"
#import "N7APIMessage.h"

@implementation N7APIMessagesReponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(messages): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)messagesJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[N7APIMessage class]];
}

- (id)resultObject
{
    return self.messages;
}

@end
