//
//  N7APIProlongParkingResult.h
//  Parking
//
//  Created by Geor Kasapidi on 09.02.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIProlongParkingResult : MTLModel <MTLJSONSerializing>

@property (assign, nonatomic) double endTime;

@end
