//
//  N7APIParkingArea.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface N7APIParkingArea : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *zoneCode;

@property (assign, nonatomic) NSInteger freeSpots;
@property (assign, nonatomic) NSInteger totalSpots;

@property (copy, nonatomic) NSString *workdayStartTime;
@property (copy, nonatomic) NSString *workdayEndTime;
@property (copy, nonatomic) NSString *workdayRateFirstHour;
@property (copy, nonatomic) NSString *workdayRateNextHour;
@property (copy, nonatomic) NSString *workdayMinAmount;

@property (copy, nonatomic) NSString *saturdayStartTime;
@property (copy, nonatomic) NSString *saturdayEndTime;
@property (copy, nonatomic) NSString *saturdayRateFirstHour;
@property (copy, nonatomic) NSString *saturdayRateNextHour;
@property (copy, nonatomic) NSString *saturdayMinAmount;

@property (copy, nonatomic) NSString *sundayStartTime;
@property (copy, nonatomic) NSString *sundayEndTime;
@property (copy, nonatomic) NSString *sundayRateFirstHour;
@property (copy, nonatomic) NSString *sundayRateNextHour;
@property (copy, nonatomic) NSString *sundayMinAmount;

@end
