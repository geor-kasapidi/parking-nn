//
//  N7APIAccountHistoryEntry.m
//  Parking
//
//  Created by Георгий Касапиди on 17.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIAccountHistoryEntry.h"

@implementation N7APIAccountHistoryEntry

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(dateString): @"date",
             SEL_NAME(contentType): @"contentType",
             SEL_NAME(amount): @"amount",
             SEL_NAME(vehicle): @"vehicle",
             SEL_NAME(zoneCode): @"zone_code",
             SEL_NAME(message): @"message"};
}

@end
