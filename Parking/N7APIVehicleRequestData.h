//
//  N7APIVehicleRequestData.h
//  Parking
//
//  Created by Георгий Касапиди on 17.07.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APISession.h"

@interface N7APIVehicleRequestData : N7APISession

@property (copy, nonatomic) NSString *vehicle;

@end
