//
//  N7APIStartPayment.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIStartPayment.h"

@implementation N7APIStartPayment

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(zone): @"zone",
              SEL_NAME(vehicle): @"vehicle",
              SEL_NAME(type): @"type",
              SEL_NAME(minutes): @"minutes",
              SEL_NAME(timed): @"timed"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

@end
