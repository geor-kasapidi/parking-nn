//
//  N7APIStopPaymentResult.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIStopPaymentResult.h"

@implementation N7APIStopPaymentResult

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(stopped): @"stopped"};
}

@end
