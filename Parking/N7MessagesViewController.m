//
//  N7MessagesViewController.m
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

#import "N7MessagesViewController.h"
#import "N7MessagesViewModel.h"
#import "N7MessagesTableViewCell.h"

@interface N7MessagesViewController () <N7ViewPreferredActions, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet N7MessagesViewModel *viewModel;

@end

@implementation N7MessagesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
}

#pragma mark - N7ViewPreferredActions

- (void)setupUI
{
    self.tableView.estimatedRowHeight = 80;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [UIView new];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)bindUI
{
    RAC(self, title) = [RACObserve(self.viewModel, balance) map:^id(NSNumber *balance) {
        return [balance currencyString];
    }];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    
    @weakify(self);
    [RACObserve(self.viewModel, messages) subscribeNext:^(id x) {
        @strongify(self);
        
        [self.tableView reloadData];
    }];
    
    self.navigationItem.leftBarButtonItem.rac_command = self.viewModel.menuCommand;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    N7MessagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLASS_NAME(N7MessagesTableViewCell)];
    
    cell.message = self.viewModel.messages[indexPath.row];
    
    return cell;
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"messages_screen_empty_title", nil)];
}

@end
