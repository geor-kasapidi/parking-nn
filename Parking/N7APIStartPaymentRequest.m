//
//  N7APIStartPaymentRequest.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIStartPaymentRequest.h"
#import "N7APIStartPayment.h"
#import "N7APIStartPaymentResponse.h"

@implementation N7APIStartPaymentRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(startPayment): @"params"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)startPaymentJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIStartPayment class]];
}

- (NSString *)methodName
{
    return @"start_payment";
}

- (Class)responseClass
{
    return [N7APIStartPaymentResponse class];
}

@end
