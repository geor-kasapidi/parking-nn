//
//  N7SessionData.h
//  Parking
//
//  Created by Георгий Касапиди on 15.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ActivePayment.h"
#import "N7AccountHistoryEntry.h"
#import "N7Message.h"

@interface N7SessionData : NSObject

@property (strong, nonatomic) NSNumber *balance;

@property (copy, nonatomic) NSArray<N7ActivePayment *> *activePayments;
@property (copy, nonatomic) NSArray<N7AccountHistoryEntry *> *accountHistory;
@property (copy, nonatomic) NSArray<N7Message *> *messages;

@end
