//
//  N7VehiclesCollectionViewCell.h
//  Parking
//
//  Created by Георгий Касапиди on 02.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@class N7Vehicle;

@interface N7VehiclesCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) N7Vehicle *vehicle;

@end
