//
//  N7ParkingArea.h
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7ParkingDaysCollection.h"

@interface N7ParkingArea : NSObject

@property (copy, nonatomic) NSString *code;

@property (assign, nonatomic) NSInteger freeSpots;
@property (assign, nonatomic) NSInteger totalSpots;

@property (strong, nonatomic) N7ParkingDaysCollection *days;

@end
