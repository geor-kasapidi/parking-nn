//
//  N7APIBalanceResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 22.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@interface N7APIBalanceResponse : N7APIResponse

@property (copy, nonatomic) NSString *balance;

@end
