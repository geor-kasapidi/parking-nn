//
//  N7NotificationsManager.m
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7NotificationsManager.h"

static NSString * const kNotificationUserInfoKey = @"kNotificationUserInfoKey";

@implementation N7NotificationsManager

- (BOOL)scheduleLocalNotificationWithKey:(NSString *)key fireDate:(NSDate *)fireDate body:(NSString *)body
{
    if ([[NSDate date] isLaterThan:fireDate]) {
        return NO;
    }
    
    UILocalNotification *notification = [UILocalNotification new];
    
    if (!notification) {
        return NO;
    }
    
    notification.fireDate = fireDate;
    notification.timeZone = [NSTimeZone systemTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.userInfo = @{kNotificationUserInfoKey: key};
    notification.alertBody = body;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    return YES;
}

- (void)cancelLocalNotificationsByKey:(NSString *)key
{
    NSArray<UILocalNotification *> *notifications = [UIApplication sharedApplication].scheduledLocalNotifications;
    
    for (UILocalNotification *notification in notifications) {
        NSString *userInfoKey = notification.userInfo[kNotificationUserInfoKey];
        
        if ([userInfoKey isEqualToString:key]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
}

- (void)cancelAllLocalNotifications
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

@end
