//
//  UIWindow+Root.m
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "UIWindow+Root.h"

@implementation UIWindow (Root)

- (void)changeRootViewControllerTo:(UIViewController *)viewController
               withTransitionStyle:(UIViewAnimationOptions)transitionStyle
{
    if ([self.rootViewController isKindOfClass:[viewController class]]) {
        return;
    }
    
    if (!self.rootViewController || transitionStyle == UIViewAnimationOptionTransitionNone) {
        self.rootViewController = viewController;
        
        return;
    }
    
    UIView *snapshot = [self.rootViewController.view snapshotViewAfterScreenUpdates:NO];
    
    [viewController.view addSubview:snapshot];
    
    self.rootViewController = viewController;
    
    [UIView transitionWithView:self
                      duration:.5
                       options:transitionStyle
                    animations:^{
                        [snapshot removeFromSuperview];
                    }
                    completion:nil];
}

@end
