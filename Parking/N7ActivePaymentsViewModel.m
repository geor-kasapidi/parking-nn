//
//  N7ActivePaymentsViewModel.m
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Core.h"

#import "N7ActivePaymentsViewModel.h"

@interface N7ActivePaymentsViewModel ()

@property (strong, nonatomic) NSArray<N7ActivePayment *> *activePayments;
@property (strong, nonatomic) N7ParkingAreasCollection *parkingAreas;

@property (strong, nonatomic) NSNumber *balance;

@property (strong, nonatomic) RACCommand *menuCommand;
@property (strong, nonatomic) RACCommand *stopCommand;
@property (strong, nonatomic) RACCommand *prolongCommand;

@end

@implementation N7ActivePaymentsViewModel

- (void)awakeFromNib
{
    RAC(self, balance) = RACObserve(CORE, data.account.session.balance);
    RAC(self, activePayments) = RACObserve(CORE, data.account.session.activePayments);
    RAC(self, parkingAreas) = RACObserve(CORE, data.account.parkingAreas);
}

#pragma mark -

- (RACCommand *)menuCommand
{
    if (!_menuCommand) {
        _menuCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            [CORE toggleMenu];
            
            return [RACSignal empty];
        }];
    }
    
    return _menuCommand;
}

- (RACCommand *)stopCommand
{
    if (!_stopCommand) {
        _stopCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(N7ActivePayment *activePayment) {
            if ([activePayment isKindOfClass:[N7ActivePayment class]]) {
                [CORE stopPayment:activePayment];
            }
            
            return [RACSignal empty];
        }];
    }
    
    return _stopCommand;
}

- (RACCommand *)prolongCommand
{
    if (!_prolongCommand) {
        _prolongCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(N7ActivePayment *activePayment) {
            if ([activePayment isKindOfClass:[N7ActivePayment class]]) {
                [CORE prolongPayment:activePayment];
            }
            
            return [RACSignal empty];
        }];
    }
    
    return _prolongCommand;
}

@end
