//
//  N7ActivePayment.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7ActivePayment.h"

@implementation N7ActivePayment

- (NSDate *)completedSoonNotificationFireDate
{
    return [self.endDate dateBySubtractingMinutes:3];
}

- (NSString *)completedSoonNotificationText
{
    NSString *dateString = [self.endDate formattedDateWithFormat:@"HH:mm"];
    
    return [NSString stringWithFormat:NSLocalizedString(@"completed_soon_notification_text", nil), dateString, self.vehicleNumber, self.parkingAreaCode];
}

@end
