//
//  N7AccountHistoryTableViewCell.h
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@class N7AccountHistoryEntry;

@interface N7AccountHistoryTableViewCell : UITableViewCell

@property (strong, nonatomic) N7AccountHistoryEntry *entry;

@end
