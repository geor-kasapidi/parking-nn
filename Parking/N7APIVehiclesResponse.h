//
//  N7APIVehiclesResponse.h
//  Parking
//
//  Created by Георгий Касапиди on 25.11.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIResponse.h"

@class N7APIVehicle;

@interface N7APIVehiclesResponse : N7APIResponse

@property (strong, nonatomic) NSArray<N7APIVehicle *> *vehicles;

@end
