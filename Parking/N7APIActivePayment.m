//
//  N7APIActivePayment.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIActivePayment.h"

@implementation N7APIActivePayment

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{SEL_NAME(vehicle): @"vehicle",
             SEL_NAME(zone): @"zone",
             SEL_NAME(startTime): @"time.start",
             SEL_NAME(currentTime): @"time.current",
             SEL_NAME(endTime): @"time.end"};
}

@end
