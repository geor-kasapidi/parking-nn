//
//  N7APIProlongParkingResponse.m
//  Parking
//
//  Created by Geor Kasapidi on 09.02.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIProlongParkingResponse.h"
#import "N7APIProlongParkingResult.h"

@implementation N7APIProlongParkingResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(prolongParkingResult): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)prolongParkingResultJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIProlongParkingResult class]];
}

- (id)resultObject
{
    return self.prolongParkingResult;
}

@end
