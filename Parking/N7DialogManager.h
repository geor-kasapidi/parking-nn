//
//  N7DialogManager.h
//  Parking
//
//  Created by Георгий Касапиди on 19.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

@class N7BaseNavigationManager;

NS_ASSUME_NONNULL_BEGIN

@interface N7DialogManager : NSObject

- (instancetype)initWithNavigationManager:(N7BaseNavigationManager *)navigationManager;

- (RACSignal *)alertWithTitle:(NSString *)title message:(NSString *)message;
- (RACSignal *)alertWithTitle:(NSString *)title
                      message:(NSString *)message
                   closeTitle:(NSString *)closeTitle;

- (RACSignal *)confirmWithTitle:(NSString *)title message:(NSString *)message;
- (RACSignal *)confirmWithTitle:(NSString *)title
                        message:(NSString *)message
                        okTitle:(NSString *)okTitle
                     closeTitle:(NSString *)closeTitle;

- (RACSignal *)promptWithTitle:(NSString *)title message:(NSString *)message;
- (RACSignal *)promptWithTitle:(NSString *)title
                       message:(NSString *)message
                          text:(NSString *)text;
- (RACSignal *)promptWithTitle:(NSString *)title
                       message:(NSString *)message
                          text:(NSString *)text
                       okTitle:(NSString *)okTitle
                    closeTitle:(NSString *)closeTitle;

- (RACSignal *)selectionListWithItems:(NSArray<NSString *> *)items
                                title:(NSString *)title
                           closeTitle:(NSString *)closeTitle;

@end

NS_ASSUME_NONNULL_END
