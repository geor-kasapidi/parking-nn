//
//  N7APIStopPaymentResponse.m
//  Parking
//
//  Created by Георгий Касапиди on 08.12.15.
//  Copyright © 2015 N7. All rights reserved.
//

#import "N7APIStopPaymentResponse.h"
#import "N7APIStopPaymentResult.h"

@implementation N7APIStopPaymentResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(stopPaymentResult): @"result"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

+ (NSValueTransformer *)stopPaymentResultJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[N7APIStopPaymentResult class]];
}

- (id)resultObject
{
    return self.stopPaymentResult;
}

@end
