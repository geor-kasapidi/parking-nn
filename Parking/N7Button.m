//
//  N7Button.m
//  Parking
//
//  Created by Георгий Касапиди on 04.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Button.h"

@implementation N7Button

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    
    self.alpha = enabled ? 1 : .6;
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    self.backgroundColor = highlighted ? [self.buttonColor darkenByPercentage:.1] : self.buttonColor;
}

#pragma mark -

- (void)setButtonColor:(UIColor *)buttonColor
{
    _buttonColor = buttonColor;
    
    self.backgroundColor = buttonColor;
}

#pragma mark -

- (UIColor *)textColor
{
    return [self titleColorForState:UIControlStateNormal];
}

- (void)setTextColor:(UIColor *)textColor
{
    [self setTitleColor:textColor forState:UIControlStateNormal];
}

#pragma mark -

- (CGFloat)cornerRadius
{
    return self.layer.cornerRadius;
}

- (void)setCornerRadius:(CGFloat)cornerRadius
{
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = cornerRadius > 0;
}

@end
