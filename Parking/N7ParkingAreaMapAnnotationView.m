//
//  N7ParkingAreaMapAnnotationView.m
//  Parking
//
//  Created by Георгий Касапиди on 02.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ParkingAreaMapAnnotationView.h"

@interface N7ParkingAreaMapAnnotationView ()

@property (strong, nonatomic) UILabel *textLabel;

@end

@implementation N7ParkingAreaMapAnnotationView

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        self.canShowCallout = NO;
        
        self.backgroundColor = [UIColor clearColor];
        
        self.rightCalloutAccessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"car"]];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:self.bounds];
        
        textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        textLabel.textColor = [UIColor whiteColor];
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.font = [UIFont boldSystemFontOfSize:22];
        
        [self addSubview:textLabel];
        
        self.textLabel = textLabel;
        
        self.count = 1;
        
        self.selected = NO;
    }
    
    return self;
}

- (UIImage *)createBackgroundImageWithSize:(CGFloat)size
{
    return [self createBackgroundImageWithSize:size backgroundColor:FlatSkyBlueDark borderColor:[UIColor whiteColor]];
}

- (UIImage *)createBackgroundImageWithSize:(CGFloat)size
                           backgroundColor:(UIColor *)backgroundColor
                               borderColor:(UIColor *)borderColor
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(size, size), NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [backgroundColor setFill];
    CGContextFillEllipseInRect(context, CGRectMake(2, 2, size - 4, size - 4));
    
    [borderColor setStroke];
    CGContextSetLineWidth(context, 2);
    CGContextStrokeEllipseInRect(context, CGRectMake(4, 4, size - 8, size - 8));
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark -

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if (self.count > 1) {
        return;
    }
    
    self.image = [self createBackgroundImageWithSize:selected ? 64 : 48];
}

- (void)setCount:(NSUInteger)count
{
    _count = count;
    
    self.textLabel.text = count > 1 ? [@(count) stringValue] : @"P";
}

@end
