//
//  N7APIResetPassword.h
//  Parking
//
//  Created by Георгий Касапиди on 14.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRequestModel.h"

@interface N7APIResetPassword : N7APIRequestModel

@property (copy, nonatomic) NSString *phone;

@end
