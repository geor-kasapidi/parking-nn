//
//  N7APIRegister.m
//  Parking
//
//  Created by Георгий Касапиди on 11.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7APIRegister.h"

@implementation N7APIRegister

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return [@{SEL_NAME(email): @"email",
              SEL_NAME(phone): @"phone",
              SEL_NAME(name): @"name",
              SEL_NAME(surname): @"surname",
              SEL_NAME(gender): @"gender"} mtl_dictionaryByAddingEntriesFromDictionary:[super JSONKeyPathsByPropertyKey]];
}

@end
