//
//  N7PriceCalculatorTests3.m
//  Parking
//
//  Created by Георгий Касапиди on 27.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@import XCTest;

#import <DateTools.h>

#import "NSNumber+Currency.h"

#import "N7ParkingDaysCollection.h"

@interface N7PriceCalculatorTests3 : XCTestCase

@property (strong, nonatomic) N7ParkingDaysCollection *days;

@end

@implementation N7PriceCalculatorTests3

- (void)setUp
{
    [super setUp];
    
    N7ParkingDay *workday = [N7ParkingDay parkingDayFromStartTimeString:@"08:00"
                                                          endTimeString:@"20:00"
                                                             rateString:@"60.00"
                                                        minAmountString:@"20.00"];
    
    N7ParkingDay *saturday = [N7ParkingDay parkingDayFromStartTimeString:@"08:00"
                                                           endTimeString:@"20:00"
                                                              rateString:@"60.00"
                                                         minAmountString:@"20.00"];
    
    N7ParkingDay *sunday = [N7ParkingDay parkingDayFromStartTimeString:@""
                                                         endTimeString:@""
                                                            rateString:@""
                                                       minAmountString:@""];
    
    N7ParkingDaysCollection *days = [N7ParkingDaysCollection collectionWithWorkday:workday
                                                                          saturday:saturday
                                                                            sunday:sunday];
    
    self.days = days;
}

- (void)test1
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:19
                                                                                                           minute:55
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:8
                                                                                                           minute:5
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 20, 0.001);
}

- (void)test2
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:19
                                                                                                           minute:55
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:8
                                                                                                           minute:20
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 25, 0.001);
}

@end
