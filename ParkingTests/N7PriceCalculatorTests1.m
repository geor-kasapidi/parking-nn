//
//  N7PriceCalculatorTests1.m
//  Parking
//
//  Created by Георгий Касапиди on 27.01.16.
//  Copyright © 2016 N7. All rights reserved.
//

@import XCTest;

#import <DateTools.h>

#import "NSNumber+Currency.h"

#import "N7ParkingDaysCollection.h"

@interface N7PriceCalculatorTests1 : XCTestCase

@property (strong, nonatomic) N7ParkingDaysCollection *days;

@end

@implementation N7PriceCalculatorTests1

- (void)setUp
{
    [super setUp];
    
    N7ParkingDay *workday = [N7ParkingDay parkingDayFromStartTimeString:@"08:00"
                                                          endTimeString:@"20:00"
                                                             rateString:@"50.00"
                                                        minAmountString:@"12.50"];
    
    N7ParkingDay *saturday = [N7ParkingDay parkingDayFromStartTimeString:@""
                                                           endTimeString:@""
                                                              rateString:@""
                                                         minAmountString:@""];
    
    N7ParkingDay *sunday = [N7ParkingDay parkingDayFromStartTimeString:@""
                                                         endTimeString:@""
                                                            rateString:@""
                                                       minAmountString:@""];
    
    N7ParkingDaysCollection *days = [N7ParkingDaysCollection collectionWithWorkday:workday
                                                                          saturday:saturday
                                                                            sunday:sunday];
    
    self.days = days;
}

- (void)test1
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:9
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:9
                                                                                                           minute:15
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 12.5, 0.001);
}

- (void)test2
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:7
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:8
                                                                                                           minute:0
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 0, 0.001);
}

- (void)test3
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:7
                                                                                                           minute:10
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:8
                                                                                                           minute:10
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 12.5, 0.001);
}

- (void)test4
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:7
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:9
                                                                                                           minute:0
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 50, 0.001);
}

- (void)test5
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:19
                                                                                                           minute:45
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:20
                                                                                                           minute:15
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 12.5, 0.001);
}

- (void)test6
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:20
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:21
                                                                                                           minute:30
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 0, 0.001);
}

- (void)test7
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:21
                                                                                                             hour:19
                                                                                                           minute:45
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:8
                                                                                                           minute:45
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 50, 0.001);
}

- (void)test8
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:21
                                                                                                             hour:19
                                                                                                           minute:59
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:8
                                                                                                           minute:14
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 12.5, 0.001);
}

- (void)test9
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:21
                                                                                                             hour:20
                                                                                                           minute:30
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:8
                                                                                                           minute:30
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 25, 0.001);
}

- (void)test10
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:21
                                                                                                             hour:20
                                                                                                           minute:10
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:8
                                                                                                           minute:10
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 12.5, 0.001);
}

- (void)test11
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:19
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:9
                                                                                                           minute:0
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 50, 0.001);
}

- (void)test12
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:19
                                                                                                           minute:45
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:8
                                                                                                           minute:45
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 12.5, 0.001);
}

- (void)test13
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:22
                                                                                                             hour:20
                                                                                                           minute:45
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:8
                                                                                                           minute:45
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 0, 0.001);
}

- (void)test14
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:10
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:11
                                                                                                           minute:0
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 0, 0.001);
}

- (void)test15
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:10
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:10
                                                                                                           minute:15
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 0, 0.001);
}

- (void)test16
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:23
                                                                                                             hour:10
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:24
                                                                                                             hour:9
                                                                                                           minute:45
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 0, 0.001);
}

- (void)test17
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:24
                                                                                                             hour:10
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:24
                                                                                                             hour:11
                                                                                                           minute:0
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 0, 0.001);
}

- (void)test18
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:24
                                                                                                             hour:10
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:25
                                                                                                             hour:8
                                                                                                           minute:15
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 12.5, 0.001);
}

- (void)test19
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:24
                                                                                                             hour:10
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:25
                                                                                                             hour:8
                                                                                                           minute:30
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 25, 0.001);
}

- (void)test20
{
    NSNumber *price = [self.days priceForParkingPeriod:[DTTimePeriod timePeriodWithStartDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:25
                                                                                                             hour:20
                                                                                                           minute:0
                                                                                                           second:0]
                                                                                     endDate:[NSDate dateWithYear:2016
                                                                                                            month:1
                                                                                                              day:25
                                                                                                             hour:21
                                                                                                           minute:0
                                                                                                           second:0]]];
    
    XCTAssertEqualWithAccuracy([price doubleValue], 0, 0.001);
}

@end
